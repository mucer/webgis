package de.fnt.webgis.session;

import de.fnt.webgis.context.ContextHolder;

public class Session
{
	private final String sessionId;

	private Authentication authentication = null;

	private ContextHolder sessionContext = null;

	public Session(String sessionId)
	{
		this.sessionId = sessionId;
	}

	/**
	 * Returns the current session id.
	 * 
	 * @return The id of the current session.
	 */
	public String getSessionId()
	{
		return this.sessionId;
	}

	/**
	 * Returns the current authentication for this session.
	 * 
	 * @return The current authentication or null.
	 */
	public Authentication getAuthentication()
	{
		return this.authentication;
	}

	/**
	 * Sets the authentication.
	 * 
	 * @param authentication Set this authentication
	 */
	public void setAuthentication(Authentication authentication)
	{
		if (this.authentication != authentication)
		{
			// remove old authentication from context
			if (this.sessionContext != null && this.authentication != null)
			{
				sessionContext.unregisterInstance(this.authentication);
			}

			this.authentication = authentication;

			// register new authentication
			if (this.sessionContext != null && this.authentication != null)
			{
				sessionContext.registerInstance(this.authentication);
			}
		}
	}

	public ContextHolder getSessionContext()
	{
		return this.sessionContext;
	}

	public void setSessionContext(ContextHolder sessionContext)
	{
		this.sessionContext = sessionContext;
	}

	protected void logout()
	{
	}

	@Override
	public String toString()
	{
		return "[Session: id '" + sessionId + "']";
	}
}
