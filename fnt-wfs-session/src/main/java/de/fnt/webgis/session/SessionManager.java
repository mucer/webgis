package de.fnt.webgis.session;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class SessionManager
{
	Map<String, Session> sessions = new ConcurrentHashMap<String, Session>();

	/**
	 * Gets the session for the given id.
	 * 
	 * @param sessionId Get the session for this id.
	 * @return The session or null.
	 */
	public Session getSession(String sessionId)
	{
		return this.sessions.get(sessionId);
	}

	public void logout(String sessionId)
	{
		Session session = getSession(sessionId);
		logout(session);
	}

	public void logout(Session session)
	{
		if (session != null)
		{
			session.logout();
			this.sessions.remove(session);
		}
	}

	/**
	 * Creates a new session.
	 * 
	 * @return The new session.
	 */
	public Session createSession()
	{
		String sessionId = generateSessionId();
		Session session = new Session(sessionId);
		this.sessions.put(sessionId, session);
		return session;
	}

	/**
	 * Generates a new session id with the length of 14 containing
	 * big letters and digits. Also ensures that the id is unique.
	 * 
	 * @return The new session id.
	 */
	protected synchronized String generateSessionId()
	{
		String sessionId;
		do
		{
			String pool = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
			int length = 14;

			char[] ary = new char[length];
			for (int i = 0; i < length; i++)
			{
				int random = (int) (Math.random() * length);
				// for the case Math.random() returns exact 1
				if (random == length)
				{
					random = length - 1;
				}
				ary[i] = pool.charAt(random);
			}

			sessionId = new String(ary);
		} while (sessions.containsKey(sessionId));

		return sessionId;
	}
}
