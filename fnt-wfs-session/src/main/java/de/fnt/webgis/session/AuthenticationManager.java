package de.fnt.webgis.session;

import de.fnt.webgis.annotation.Context;
import de.fnt.webgis.context.Scope;

@Context(enableCache = true, scope = Scope.APPLICATION)
public interface AuthenticationManager
{
	public Authentication restoreAuthentication(Session session);

	public Authentication createAnonymousAuthentication(Session session);

	public Authentication login(Session session, String username, String password);
}
