package de.fnt.webgis.session;

public interface Authentication
{
	/**
	 * Checks whether the session is valid, e.g. no
	 * timeout has occured.
	 * 
	 * @return Returns true if the session is valid.
	 */
	boolean isValid();

	/**
	 * Checks whether the user is logged in 
	 * anonymous.
	 * 
	 * @return Returns true if the authenication is anonymous
	 */
	boolean isAnonymous();

	/**
	 * The current user name
	 * 
	 * @return
	 */
	String getUserName();
}
