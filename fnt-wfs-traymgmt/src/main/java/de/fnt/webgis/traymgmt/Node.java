package de.fnt.webgis.traymgmt;

import com.vividsolutions.jts.geom.Point;

import de.fnt.webgis.annotation.Attribute;
import de.fnt.webgis.annotation.Feature;

@Feature("node")
public class Node
{
	private String id;

	private Point pos;

	public Node(String id, Point pos)
	{
		this.id = id;
		this.pos = pos;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	@Attribute(isUnique = true)
	public String getId()
	{
		return this.id;
	}

	public void setPos(Point pos)
	{
		this.pos = pos;
	}

	public Point getPos()
	{
		return this.pos;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (obj == this)
		{
			return true;
		}

		if (obj instanceof Node)
		{
			return ((Node) obj).getId().equals(this.id);
		}

		return false;
	}

	@Override
	public String toString()
	{
		return "[Node: id '" + id + "']";
	}
}
