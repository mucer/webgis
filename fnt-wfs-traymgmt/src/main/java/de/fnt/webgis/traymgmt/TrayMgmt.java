package de.fnt.webgis.traymgmt;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.vividsolutions.jts.geom.Geometry;

import de.fnt.webgis.annotation.Layer;
import de.fnt.webgis.di.DIUtil;
import de.fnt.webgis.session.Authentication;

@Layer("http://www.fnt.de/traymgmt")
public class TrayMgmt
{
	private final static Logger logger = Logger.getLogger(TrayMgmt.class);

	private static int nodeId = 1000;

	private static int trayId = 1000;

	public final Map<String, List<Node>> nodeMap = new HashMap<>();

	public final Map<String, List<Tray>> trayMap = new HashMap<>();

	public TrayMgmt()
	{
		//		GeometryBuilder gb = new GeometryBuilder();
		//		add(new Node(null, gb.point(10.102986328124768, 48.969637189373834)));
		//add(new Node(null, gb.point(1549472, 6403611)));
		//add(new Node(null, gb.point(1550001, 6404015)));

		//		add(new Tray(null, gb.lineString(146.468582, -41.241478, 146.574768, -41.251186, 146.640411,
		//			-41.255154, 146.766129, -41.332348, 146.794189, -41.34417, 146.822174, -41.362988,
		//			146.863434, -41.380234, 146.899521, -41.379452, 146.929504, -41.378227, 147.008041,
		//			-41.356079, 147.098343, -41.362919), nodes.get(0)));
		//		add(new Tray(null, gb.lineString(1549100, 6403700, 1549472, 6403611, 1550001, 6404015), nodes
		//
	}

	public List<Node> getNodes(Authentication auth)
	{
		String name = auth.getUserName().toUpperCase();
		List<Node> nodes = this.nodeMap.get(name);
		if (nodes == null)
		{
			nodes = new ArrayList<>();
			this.nodeMap.put(name, nodes);
		}
		return nodes;
	}

	public List<Tray> getTrays(Authentication auth)
	{
		String name = auth.getUserName().toUpperCase();
		List<Tray> trays = this.trayMap.get(name);
		if (trays == null)
		{
			trays = new ArrayList<>();
			this.trayMap.put(name, trays);
		}
		return trays;
	}

	public Node[] searchNodes(Authentication auth, Geometry box)
	{
		List<Node> result = new ArrayList<Node>();
		for (Node n : getNodes(auth))
		{
			if (box == null || n.getPos().isWithinDistance(box, 10))
			{
				result.add(n);
			}
		}
		Node[] ary = new Node[result.size()];
		result.toArray(ary);
		return ary;
	}

	public Node[] searchNodes(Authentication auth, String[] ids)
	{
		List<Node> result = new ArrayList<Node>();
		for (Node n : getNodes(auth))
		{
			if (ids == null || Arrays.binarySearch(ids, n.getId()) > -1)
			{
				result.add(n);
			}
		}
		Node[] ary = new Node[result.size()];
		result.toArray(ary);
		return ary;
	}

	public Tray[] searchTrays(Authentication auth, Geometry box)
	{
		List<Tray> result = new ArrayList<Tray>();
		for (Tray t : getTrays(auth))
		{
			if (box == null || t.getPath().isWithinDistance(box, 10))
			{
				result.add(t);
			}
		}
		Tray[] ary = new Tray[result.size()];
		result.toArray(ary);
		return ary;
	}

	public Tray getTray(Authentication auth, String id)
	{
		for (Tray t : getTrays(auth))
		{
			if (t.getId().equals(id))
			{
				return t;
			}
		}
		return null;
	}

	public Node getNode(Authentication auth, String id)
	{
		for (Node n : getNodes(auth))
		{
			if (n.getId().equals(id))
			{
				return n;
			}
		}
		return null;
	}

	public String add(Authentication auth, Node node)
	{
		String id = node.getId();
		if (id == null || id.length() == 0)
		{
			id = "N-" + (nodeId++);
			node.setId(id);
		}

		List<Node> nodes = getNodes(auth);
		if (nodes.contains(node))
		{
			logger.warn("The node '" + node + "' was already added!");
		}
		else
		{
			nodes.add(node);
			if (logger.isInfoEnabled())
			{
				logger.info("Added node '" + node + "'");
			}
		}

		return id;
	}

	public String add(Authentication auth, Tray tray)
	{
		String id = tray.getId();
		if (id == null || id.length() == 0)
		{
			id = "T-" + (trayId++);
			tray.setId(id);
		}

		List<Tray> trays = getTrays(auth);
		if (trays.contains(tray))
		{
			logger.warn("The tray '" + tray + "' was already added!");
		}
		else
		{
			trays.add(tray);
			if (logger.isInfoEnabled())
			{
				logger.info("Added tray '" + tray + "'");
			}
		}

		return id;
	}

	public void update(Authentication auth, Node node, Map<String, Object> attributes)
	{
		System.out.println("Update: " + node);
		DIUtil.apply(node, attributes);
	}

	public void update(Authentication auth, Tray tray)
	{
	}

	public void delete(Authentication auth, Node node)
	{
		getNodes(auth).remove(node);
	}

	public void delete(Authentication auth, Tray tray)
	{
		getTrays(auth).remove(tray);
	}
}
