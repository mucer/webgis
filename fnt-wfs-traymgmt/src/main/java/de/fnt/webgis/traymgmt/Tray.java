package de.fnt.webgis.traymgmt;

import com.vividsolutions.jts.geom.LineString;

import de.fnt.webgis.annotation.Attribute;
import de.fnt.webgis.annotation.Feature;

@Feature("tray")
public class Tray
{
	private String id;

	private LineString path;

	public Tray(String id, LineString path)
	{
		this.id = id;
		this.path = path;
	}

	@Attribute(isUnique = true)
	public String getId()
	{
		return this.id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public void setPath(LineString path)
	{
		this.path = path;
	}

	public LineString getPath()
	{
		return this.path;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (obj == this)
		{
			return true;
		}

		if (obj instanceof Tray)
		{
			return ((Tray) obj).getId().equals(this.id);
		}

		return false;
	}

	@Override
	public String toString()
	{
		return "[Tray: id '" + id + "']";
	}
}
