package de.fnt.webgis.servlet;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.namespace.QName;

import net.opengis.wfs.DeleteElementType;
import net.opengis.wfs.FeatureCollectionType;
import net.opengis.wfs.InsertElementType;
import net.opengis.wfs.InsertResultsType;
import net.opengis.wfs.InsertedFeatureType;
import net.opengis.wfs.PropertyType;
import net.opengis.wfs.QueryType;
import net.opengis.wfs.TransactionResponseType;
import net.opengis.wfs.TransactionSummaryType;
import net.opengis.wfs.TransactionType;
import net.opengis.wfs.UpdateElementType;
import net.opengis.wfs.WfsFactory;
import net.opengis.wfs.impl.WfsFactoryImpl;

import org.apache.log4j.Logger;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.xsd.XSDSchema;
import org.eclipse.xsd.util.XSDResourceImpl;
import org.geotools.data.collection.ListFeatureCollection;
import org.geotools.feature.NameImpl;
import org.geotools.filter.identity.FeatureIdImpl;
import org.geotools.xml.Encoder;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.filter.Filter;

import de.fnt.webgis.layer.FeatureLayerManager;
import de.fnt.webgis.ows.wfs.WfsConstants.HttpMethod;
import de.fnt.webgis.ows.wfs.WfsData;
import de.fnt.webgis.ows.wfs.WfsException;
import de.fnt.webgis.ows.wfs.WfsHttpRequestParser;
import de.fnt.webgis.ows.wfs.xml.GMLEncoder;
import de.fnt.webgis.util.ServletUtils;

/**
 * This servlet collects all requests and delegates them to the 
 * correct method.
 *
 */
public abstract class WfsServlet extends HttpServlet
{
	private static final Logger logger = Logger.getLogger(WfsServlet.class);

	private static final long serialVersionUID = 1L;

	protected FeatureLayerManager layerManager;

	@Override
	public void init() throws ServletException
	{
		super.init();
		long start = System.currentTimeMillis();
		this.layerManager = initLayerManager();
		System.out.println("Init: " + (System.currentTimeMillis() - start) + " ms");
	}

	protected abstract FeatureLayerManager initLayerManager();

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException
	{
		long serviceTime = System.currentTimeMillis();
		
		if (logger.isDebugEnabled())
		{
			logger.debug("new request: " + request.getPathInfo() + " (Method " + request.getMethod()
				+ ")");
		}

		if (HttpMethod.OPTIONS.toString().equals(request.getMethod()))
		{
			response.setHeader("Allow", HttpMethod.GET + ", " + HttpMethod.POST);
		}
		else
		{
			String pathInfo = ServletUtils.trimSlashes(request.getPathInfo());

			if ("wfs".equalsIgnoreCase(pathInfo))
			{
				URL baseURL = getBaseURL(request);
				WfsHttpRequestParser parser = new WfsHttpRequestParser(baseURL);
				parser.setFeatureLayerManager(this.layerManager);
				WfsData data = parser.parse(request);

				if (logger.isInfoEnabled())
				{
					logger.info("New WFS request of type '" + data.getRequestType() + "'");
				}

				try
				{
					switch (data.getRequestType()) {
					case DescribeFeatureType:
						handleDescribeFeatureType(baseURL, data, response);
						break;
					case GetFeature:
						handleGetFeature(data, response);
						break;
					case Transaction:
						handleTransaction(data, response);
						break;
					default:
						throw new Exception("Request '" + data.getRequestType() + " currently not supported!");
					}
				}
				catch (WfsException e)
				{
					throw e;
				}
				catch (Exception e)
				{
					throw new WfsException(data.getVersion(), WfsException.Code.OperationProcessingFailed, e);
				}
			}
			else if (pathInfo.startsWith("schemas"))
			{
				transferSchema(pathInfo, response);
			}
			else
			{
				throw new IllegalArgumentException("Not supported path '" + pathInfo + "' given!");
			}
		}

		System.out.println("Service: " + (System.currentTimeMillis() - serviceTime) + " ms");
	}

	protected URL getBaseURL(HttpServletRequest request) throws MalformedURLException
	{
		String scheme = request.getScheme();
		int port = request.getServerPort();

		// suppress default ports
		if ("http".equals(scheme) && port == 80 || "https".equals(scheme) && port == 443)
		{
			port = -1;
		}

		return new URL(scheme, request.getServerName(), port, request.getContextPath());
	}

	private void transferSchema(String pathInfo, HttpServletResponse response) throws IOException
	{

		File schema;
		try
		{
			URL url = getClass().getResource("/" + pathInfo);
			if (url == null)
				throw new FileNotFoundException(pathInfo);
			schema = new File(url.toURI());
		}
		catch (URISyntaxException e)
		{
			throw new IOException(e);
		}
		String mimeType = getServletContext().getMimeType(schema.getAbsolutePath());
		if (mimeType != null)
			response.setContentType(mimeType);
		long length = schema.length();
		if (length > 0 && length <= Integer.MAX_VALUE)
			response.setContentLength((int) length);

		// TODO last modified encoding

		FileReader r = null;
		OutputStreamWriter w = null;
		try
		{
			r = new FileReader(schema);
			w = new OutputStreamWriter(response.getOutputStream());
			response.setCharacterEncoding(r.getEncoding());
			char[] buffer = new char[8192];
			int len = -1;
			while ((len = r.read(buffer)) != -1)
			{
				w.write(buffer, 0, len);
			}
		}
		finally
		{
			if (r != null)
			{
				r.close();
			}
			if (w != null)
			{
				w.close();
			}
		}
	}

	private void handleDescribeFeatureType(URL baseURL, WfsData data, HttpServletResponse response)
		throws Exception
	{
		final String encoding = "UTF-8";
		URL schemaURL = new URL(baseURL.toExternalForm() + "/schemas");

		// TODO split
		XSDSchema[] xsd = this.layerManager.describeFeatureTypes(data.getVersion(), schemaURL);

		ByteArrayOutputStream out = new ByteArrayOutputStream();
		XSDResourceImpl.serialize(out, xsd[0].getElement(), encoding);
		out.flush();
		byte[] bytes = out.toByteArray();

		// TODO
		//response.setContentType(version.outputFormat);
		response.setCharacterEncoding(encoding);
		response.setContentLength(bytes.length);

		response.getOutputStream().write(bytes);
	}

	@SuppressWarnings("unchecked")
	private void handleTransaction(WfsData parameters, HttpServletResponse response)
		throws IOException
	{
		TransactionType data = (TransactionType) parameters.getRequestData();

		List<String> insertedIds = new ArrayList<String>();
		int totalUpdated = 0;
		int totalDeleted = 0;

		for (FeatureMap.Entry entry : data.getGroup())
		{
			if (entry.getValue() instanceof InsertElementType)
			{
				InsertElementType value = (InsertElementType) entry.getValue();
				for (Object obj : value.getFeature())
				{
					if (!(obj instanceof SimpleFeature))
					{
						throw new IllegalArgumentException("Unsupported feature: "
							+ obj.getClass().getCanonicalName());
					}

					SimpleFeature feature = (SimpleFeature) obj;
					String fid = layerManager.insertFeature(feature);
					insertedIds.add(fid);
				}
			}
			else if (entry.getValue() instanceof UpdateElementType)
			{
				UpdateElementType value = (UpdateElementType) entry.getValue();
				Map<String, Object> attributes = new HashMap<String, Object>();

				EList<PropertyType> list = value.getProperty();
				for (PropertyType p : list)
				{
					attributes.put(p.getName().getLocalPart(), p.getValue());
				}

				totalUpdated += layerManager.updateFeatures(new NameImpl(value.getTypeName()), value
					.getFilter(), attributes);
			}
			else if (entry.getValue() instanceof DeleteElementType)
			{
				DeleteElementType value = (DeleteElementType) entry.getValue();
				totalDeleted += layerManager.deleteFeatures(new NameImpl(value.getTypeName()), value
					.getFilter());
			}
			else
			{
				throw new IllegalArgumentException("The transaction type is not supported: "
					+ entry.getValue().getClass().getCanonicalName());
			}
		}

		// encode result
		WfsFactory factory = new WfsFactoryImpl();
		Encoder encoder = new Encoder(parameters.getVersion().getConfiguration());
		TransactionResponseType transactionResponse = factory.createTransactionResponseType();
		transactionResponse.setVersion(parameters.getVersion().getVersionStr());

		// summary
		TransactionSummaryType transactionSummery = factory.createTransactionSummaryType();
		transactionResponse.setTransactionSummary(transactionSummery);
		transactionSummery.setTotalInserted(BigInteger.valueOf(insertedIds.size()));
		transactionSummery.setTotalUpdated(BigInteger.valueOf(totalUpdated));
		transactionSummery.setTotalDeleted(BigInteger.valueOf(totalDeleted));

		// insert results
		if (insertedIds.size() > 0)
		{
			InsertResultsType insertResults = factory.createInsertResultsType();
			transactionResponse.setInsertResults(insertResults);

			for (String fid : insertedIds)
			{
				InsertedFeatureType feature = factory.createInsertedFeatureType();
				feature.getFeatureId().add(new FeatureIdImpl(fid));
				insertResults.getFeature().add(feature);
			}
		}

		encoder.setIndenting(true);
		encoder.encode(transactionResponse, org.geotools.wfs.WFS.TransactionResponse, response
			.getOutputStream());
	}

	@SuppressWarnings("unchecked")
	private void handleGetFeature(WfsData parameters, HttpServletResponse response)
		throws IOException
	{
		GMLEncoder encoder = new GMLEncoder(parameters.getVersion());
		encoder.setIndenting(true);

		if (parameters.getRequestData() instanceof net.opengis.wfs.GetFeatureType)
		{
			net.opengis.wfs.GetFeatureType request = (net.opengis.wfs.GetFeatureType) parameters
				.getRequestData();

			FeatureCollectionType collection = WfsFactory.eINSTANCE.createFeatureCollectionType();
			for (Object o : request.getQuery())
			{
				if (o instanceof QueryType)
				{
					QueryType query = (QueryType) o;
					QName typeName = (QName) query.getTypeName().get(0);
					//					URI srsName = query.getSrsName();
					Filter filter = query.getFilter();

					SimpleFeatureType type = layerManager.getFeatureType(new NameImpl(typeName));
					if (type == null)
					{
						throw new RuntimeException("Type name '" + typeName + "' is not supported!");
					}
					List<SimpleFeature> features = layerManager.searchFeatures(type.getName(), filter);
					if (!features.isEmpty())
					{
						String namespaceUri = type.getName().getNamespaceURI();
						encoder.addNamespace(generatePrefix(namespaceUri), namespaceUri);
						collection.getFeature().add(new ListFeatureCollection(type, features));
					}
				}
			}

			encoder.encode(response.getOutputStream(), collection);
		}
	}

	private String generatePrefix(String namespaceUri)
	{
		return namespaceUri.substring(namespaceUri.lastIndexOf("/") + 1);
	}
}
