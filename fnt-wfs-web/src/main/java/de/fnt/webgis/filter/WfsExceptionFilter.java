package de.fnt.webgis.filter;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.log4j.Logger;

import de.fnt.webgis.ows.wfs.WfsException;
import de.fnt.webgis.ows.wfs.xml.ExceptionEncoder;

public class WfsExceptionFilter implements Filter
{
	private static final Logger logger = Logger.getLogger(WfsExceptionFilter.class);

	public static final String CONTENT_TYPE = "text/xml";

	public static final Charset ENCODING = Charset.forName("UTF-8");

	public void init(FilterConfig filterConfig) throws ServletException
	{
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
		throws IOException, ServletException
	{
		try
		{
			chain.doFilter(request, response);
		}
		catch (WfsException e)
		{
			logger.error("Caught WfsException", e);

			ExceptionEncoder enc = new ExceptionEncoder(e);

			ByteArrayOutputStream out = new ByteArrayOutputStream();
			enc.setEncoding(ENCODING);
			enc.encode(out);
			out.flush();
			byte[] bytes = out.toByteArray();

			response.setContentType(CONTENT_TYPE);
			response.setCharacterEncoding(ENCODING.name());
			response.setContentLength(bytes.length);

			response.getOutputStream().write(bytes);
		}
	}

	public void destroy()
	{
	}
}
