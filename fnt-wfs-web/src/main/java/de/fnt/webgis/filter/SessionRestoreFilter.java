package de.fnt.webgis.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import de.fnt.webgis.context.ContextHolder;
import de.fnt.webgis.context.ContextManager;
import de.fnt.webgis.session.Session;
import de.fnt.webgis.session.SessionManager;

public class SessionRestoreFilter implements Filter
{
	private final static Logger logger = Logger.getLogger(SessionRestoreFilter.class);

	private SessionManager sessionManager = new SessionManager();

	public void init(FilterConfig filterConfig) throws ServletException
	{
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
		throws IOException, ServletException
	{
		HttpServletRequest req = (HttpServletRequest) request;

		Session session = null;

		HttpSession httpSession = req.getSession(false);
		if (httpSession != null)
		{
			String sessionId = (String) httpSession.getAttribute("sid");
			logger.debug("Loaded session id '" + sessionId + "' from http session");

			if (sessionId != null)
			{
				logger.debug("Restoring existing session with session id '" + sessionId + "'");
				session = sessionManager.getSession(sessionId);
				if (session == null)
				{
					// TODO multiserver
					throw new IllegalStateException("No session for session id '" + sessionId + "' found!");
				}
			}
		}

		// create new session
		if (session == null)
		{
			logger.debug("Creating new session");
			session = sessionManager.createSession();

			httpSession = req.getSession();
			httpSession.setAttribute("sid", session.getSessionId());
		}

		// get session context
		ContextManager manager = ContextManager.getInstance();
		ContextHolder context = session.getSessionContext();
		if (context == null)
		{
			context = manager.getSessionContext(true);
			context.registerInstance(session);
			session.setSessionContext(context);
		}
		else
		{
			manager.setSessionContext(context);
		}

		chain.doFilter(request, response);
	}

	public void destroy()
	{
	}

}
