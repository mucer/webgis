package de.fnt.webgis.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.log4j.Logger;

import de.fnt.webgis.context.ContextManager;

public class ContextInitialisationFilter implements Filter
{
	private final static Logger logger = Logger.getLogger(ContextInitialisationFilter.class);

	public void init(FilterConfig filterConfig) throws ServletException
	{

	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
		throws IOException, ServletException
	{
		ContextManager manager = ContextManager.getInstance();
		try
		{
			logger.debug("Creating new context");
			manager.getContext();

			chain.doFilter(request, response);
		}
		finally
		{
			logger.debug("Destroying current context");
			manager.destroyContext();
		}
	}

	public void destroy()
	{

	}
}
