package de.fnt.webgis.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import de.fnt.webgis.context.ContextHolder;
import de.fnt.webgis.context.ContextManager;
import de.fnt.webgis.session.Authentication;
import de.fnt.webgis.session.AuthenticationManager;
import de.fnt.webgis.session.Session;

public class AuthenticationFilter implements Filter
{
	private static final Logger logger = Logger.getLogger(AuthenticationFilter.class);

	protected class UsernamePassword
	{
		final String username;

		final String password;

		public UsernamePassword(String username, String password)
		{
			this.username = username;
			this.password = password;
		}
	}

	public void init(FilterConfig filterConfig) throws ServletException
	{
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
		throws IOException, ServletException
	{
		HttpServletRequest req = (HttpServletRequest) request;

		ContextHolder context = ContextManager.getInstance().getContext();
		Session session = context.get(Session.class);
		if (session == null)
		{
			throw new NullPointerException("No " + Session.class.getName() + " object in context found!");
		}

		Authentication authentication = session.getAuthentication();
		if (authentication != null)
		{
			if (!authentication.isValid())
			{
				logger.warn("Authentication is not valid");
				authentication = null;
			}
			else if (logger.isInfoEnabled())
			{
				logger.info("Used authentication '" + authentication + "' from session!");
			}
		}

		AuthenticationManager manager = getAuthenticationManager(context);
		if (authentication == null)
		{
			authentication = manager.restoreAuthentication(session);

			if (authentication != null && logger.isInfoEnabled())
			{
				logger.info("Used authentication '" + authentication + "' from session!");
			}
		}

		UsernamePassword data = getUsernamePassword(req);
		if (authentication == null || data != null)
		{
			authentication = data == null || data.username.length() == 0 ? manager
				.createAnonymousAuthentication(session)
				: manager.login(session, data.username, data.password);

			if (authentication != null && logger.isInfoEnabled())
			{
				logger.info("Logged in authentication '" + authentication + "'");
			}
		}

		if (authentication == null)
		{
			throw new IllegalStateException("Login failed!");
		}

		session.setAuthentication(authentication);

		chain.doFilter(request, response);
	}

	private AuthenticationManager getAuthenticationManager(ContextHolder context)
	{
		AuthenticationManager manager = context.get(AuthenticationManager.class);
		if (manager == null)
		{
			throw new NullPointerException("No " + AuthenticationManager.class + " in context found!");
		}
		return manager;
	}

	protected UsernamePassword getUsernamePassword(HttpServletRequest request)
	{
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		UsernamePassword data = null;
		if (username != null)
		{
			data = new UsernamePassword(username, password);
		}

		return data;
	}

	public void destroy()
	{
	}

}
