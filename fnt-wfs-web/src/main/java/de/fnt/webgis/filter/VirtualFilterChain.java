package de.fnt.webgis.filter;

import java.io.IOException;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class VirtualFilterChain implements FilterChain
{
	protected final List<Filter> filters;

	protected final FilterChain parentChain;

	private int currentFilter = 0;

	public VirtualFilterChain(List<Filter> filters, FilterChain parentChain)
	{
		this.filters = filters;
		this.parentChain = parentChain;
	}

	public void doFilter(ServletRequest request, ServletResponse response) throws IOException,
		ServletException
	{
		if (this.currentFilter < this.filters.size())
		{
			this.filters.get(this.currentFilter++).doFilter(request, response, this);
		}
		else
		{
			parentChain.doFilter(request, response);
		}
	}

}
