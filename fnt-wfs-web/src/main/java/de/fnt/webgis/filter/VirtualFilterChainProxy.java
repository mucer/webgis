package de.fnt.webgis.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 * This proxy links between a "normal" {@link Filter} which was
 * defined in the server config and a virtual {@link Filter} which
 * is defined in this proxy. The execution of the virtual chain
 * is done by a {@link VirtualFilterChain} object.
 */
public class VirtualFilterChainProxy implements Filter
{
	protected List<Filter> filters = new ArrayList<Filter>();

	public void init(FilterConfig filterConfig) throws ServletException
	{
		// TODO dynamic
		filters.add(new ContextInitialisationFilter());
		filters.add(new SessionRestoreFilter());
		filters.add(new AuthenticationFilter());

		for (Filter filter : filters)
		{
			// TODO own filter config?
			filter.init(filterConfig);
		}
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
		throws IOException, ServletException
	{
		VirtualFilterChain vChain = new VirtualFilterChain(filters, chain);
		vChain.doFilter(request, response);
	}

	public void destroy()
	{
		for (Filter filter : filters)
		{
			filter.destroy();
		}
	}

}
