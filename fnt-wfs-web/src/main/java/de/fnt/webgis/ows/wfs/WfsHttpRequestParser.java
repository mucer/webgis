package de.fnt.webgis.ows.wfs;

import java.io.InputStream;
import java.net.URL;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import de.fnt.webgis.ows.wfs.WfsConstants.HttpMethod;

public class WfsHttpRequestParser extends WfsDocumentParser
{

	public WfsHttpRequestParser(URL baseURL)
	{
		super(baseURL);
	}

	public WfsData parse(HttpServletRequest servletRequest)
	{
		String methodStr = servletRequest.getMethod();
		HttpMethod method = HttpMethod.valueOf(methodStr);
		WfsData data;

		switch (method) {
		case POST:
			try
			{
				// TODO When using the HTTP POST method, the content type for XML encoded WFS requests must be set to text/xml.
				InputStream in = servletRequest.getInputStream();
				data = parse(in);
				in.close();
			}
			catch (Exception e)
			{
				throw new WfsException(null, WfsException.Code.OperationParsingFailed, "CommonParameters",
					e);
			}
			break;

		case GET:
			// TODO The combination of XML encoded requests and the HTTP GET method is not supported.
			@SuppressWarnings("unchecked")
			Enumeration<String> e = servletRequest.getParameterNames();
			Map<String, Object> parameters = new HashMap<String, Object>();
			while (e.hasMoreElements())
			{
				String name = e.nextElement();
				parameters.put(name, servletRequest.getParameter(name));
			}

			data = parse(parameters);
			break;
		default:
			throw new WfsException(null, WfsException.Code.OperationParsingFailed, "ParameterParser",
				"Unsupported http method '" + method + "'");
		}

		return data;
	}
}
