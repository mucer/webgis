package de.fnt.webgis.util;

public class ServletUtils
{
	/**
	 * Removes splashes at the beginning and at the end of the given string.
	 * 
	 * @param str Remove the slashes from this string
	 * @return The corrected string
	 * 
	 * @modify dl 10-SEP-12
	 */
	public static String trimSlashes(String str)
	{
		if (str != null)
		{
			str = str.trim();
			if (str.startsWith("/"))
			{
				str = str.substring(1);
			}
			if (str.endsWith("/"))
			{
				str = str.substring(0, str.length() - 1);
			}
		}
		return str;
	}
}
