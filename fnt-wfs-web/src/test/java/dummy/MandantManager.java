package dummy;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import de.fnt.webgis.session.Authentication;
import de.fnt.webgis.session.AuthenticationManager;
import de.fnt.webgis.session.Session;
import dummy.Mandant.Status;

public class MandantManager implements AuthenticationManager
{
	private final static Logger logger = Logger.getLogger(MandantManager.class);

	Map<String, Mandant> mandants = new HashMap<String, Mandant>();

	public Authentication createAnonymousAuthentication(Session session)
	{
		logger.info("New anonymous mandant");
		Mandant mandant = new Mandant("anonym");
		this.mandants.put(session.getSessionId(), mandant);
		return mandant;
	}

	public Authentication restoreAuthentication(Session session)
	{
		Mandant mandant = this.mandants.get(session.getSessionId());
		if (mandant != null)
		{
			logger.info("Restored mandant from session '" + session + "'");
			mandant.setStatus(Status.RESTORED);
		}
		return mandant;
	}

	public Authentication login(Session session, String username, String password)
	{
		logger.info("Created new mandant with username '" + username + "' and password '" + password
			+ "'");
		Mandant mandant = new Mandant(username);
		this.mandants.put(session.getSessionId(), mandant);
		return mandant;
	}
}
