package dummy;

import de.fnt.webgis.session.Authentication;

public class Mandant implements Authentication
{
	public static enum Status
	{
		LOGGED_IN, ANONYMOUS, INVALID, RESTORED
	};

	private final String userName;

	private Status status = null;

	public Mandant(String userName)
	{
		this.userName = userName;
	}

	public boolean isValid()
	{
		return true;
	}

	public String getUserName()
	{
		return userName;
	}

	public boolean isAnonymous()
	{
		return "anonym".equals(this.userName);
	}

	@Override
	public String toString()
	{
		return "[Mandant: user '" + this.userName + "']";
	}

	public void setStatus(Status status)
	{
		this.status = status;
	}

	public Status getStatus()
	{
		Status status;
		if (this.status != null)
		{
			status = this.status;
		}
		else if (!isValid())
		{
			status = Status.INVALID;
		}
		else if (isAnonymous())
		{
			status = Status.ANONYMOUS;
		}
		else
		{
			status = Status.LOGGED_IN;
		}
		return status;
	}
}
