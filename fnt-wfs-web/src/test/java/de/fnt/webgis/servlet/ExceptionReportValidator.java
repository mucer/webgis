package de.fnt.webgis.servlet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayInputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ExceptionReportValidator
{
	public static String validate(String string) throws Exception
	{
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setIgnoringElementContentWhitespace(true);
		factory.setIgnoringComments(true);
		DocumentBuilder builder = factory.newDocumentBuilder();
		ByteArrayInputStream in = new ByteArrayInputStream(string.getBytes());
		Document doc = builder.parse(in);
		in.close();

		// check root node
		Element root = doc.getDocumentElement();
		assertEquals("ExceptionReport", root.getNodeName());
		assertTrue("Attribute 'version' required for node: " + root.getNodeName(), root
			.hasAttribute("version"));

		// check exception node
		NodeList childs = root.getElementsByTagName("Exception");
		assertEquals("One child expected: " + root.getNodeName(), 1, childs.getLength());
		Element exceptionElem = (Element) childs.item(0);
		assertTrue("Attribute 'code' required for node: " + exceptionElem.getNodeName(), exceptionElem
			.hasAttribute("code"));

		// check exception text node
		childs = exceptionElem.getElementsByTagName("ExceptionText");
		assertEquals("One child expected for node: " + exceptionElem.getNodeName(), 1, childs
			.getLength());
		Element exceptionTextElem = (Element) childs.item(0);

		// check node value
		childs = exceptionTextElem.getChildNodes();
		assertEquals("One child expected for node: " + exceptionTextElem.getNodeName(), 1, childs
			.getLength());
		Node textNode = childs.item(0);
		String message = textNode.getNodeValue();
		assertNotNull("Exception message expected", message);

		return message;
	}
}
