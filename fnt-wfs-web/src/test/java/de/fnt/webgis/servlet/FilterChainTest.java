package de.fnt.webgis.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import junit.framework.Assert;

import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mortbay.jetty.Handler;
import org.mortbay.jetty.LocalConnector;
import org.mortbay.jetty.Server;
import org.mortbay.jetty.servlet.Context;
import org.mortbay.jetty.servlet.SessionHandler;
import org.mortbay.jetty.testing.HttpTester;

import de.fnt.webgis.context.ContextHolder;
import de.fnt.webgis.context.ContextManager;
import de.fnt.webgis.context.Scope;
import de.fnt.webgis.filter.VirtualFilterChainProxy;
import de.fnt.webgis.session.Session;
import dummy.Mandant;
import dummy.Mandant.Status;

public class FilterChainTest
{
	private static Server server;

	private static LocalConnector connector;

	private static final List<Cookie> cookies = new ArrayList<Cookie>();

	/**
	 */
	@BeforeClass
	public static void startServletContainer() throws Exception
	{
		// start logger
		Logger.getRootLogger().addAppender(
			new ConsoleAppender(new PatternLayout("%5p [%t] (%F:%L) - %m%n")));

		// populate context
		ContextHolder appContext = ContextManager.getInstance().getApplicationContext();
		appContext.registerPackage("dummy");
		appContext.registerClass(ApplicationObject.class);
		appContext.registerClass(SessionObject.class);
		appContext.registerClass(RequestObject.class);

		// add servlet to context
		Context context = new Context();
		context.setContextPath("/context");
		context.setResourceBase("target/webgis");
		context.addServlet(TestServlet.class, "/*");
		context.addFilter(VirtualFilterChainProxy.class, "/wfs/*", Handler.DEFAULT);
		context.setSessionHandler(new SessionHandler());

		// add context to server
		server = new Server();
		connector = new LocalConnector();
		server.addConnector(connector);
		server.addHandler(context);

		server.start();
	}

	/**
		* Stops the Jetty container.
		*/
	@AfterClass
	public static void stopServletContainer() throws Exception
	{
		server.stop();
	}

	private HttpTester httpTest(String uri) throws Exception
	{
		HttpTester request = new HttpTester();
		request.setMethod("GET");
		request.setHeader("Host", "tester");
		request.setURI("/context/" + uri);

		StringBuffer cstr = new StringBuffer();
		for (Cookie c : cookies)
		{
			if (cstr.length() > 0)
				cstr.append("; ");
			cstr.append(c.getName()).append("=").append(c.getValue());
		}
		request.setHeader("Cookie", cstr.toString());

		//request.setContent(content);

		HttpTester response = new HttpTester();
		connector.reopen();
		response.parse(connector.getResponses(request.generate()));

		// handle cookies
		String setCookie = response.getHeader("Set-Cookie");
		if (setCookie != null)
		{
			String[] options = setCookie.split(";");
			String[] data = options[0].split("=");
			Cookie c = new Cookie(data[0], data[1]);
			cookies.add(c);
		}

		Assert.assertNull(response.getMethod());
		//		assertTrue("Content type 'text/xml' expected!", response.getHeader("Content-Type") != null
		//			&& response.getHeader("Content-Type").startsWith("text/xml"));
		Assert.assertEquals(200, response.getStatus());

		response.setContent(response.getContent().trim());

		return response;
	}

	// --------------------------------------------------------------------
	// --------------------------------------------------------------------
	// --------------------------------------------------------------------

	public static class TestServlet extends HttpServlet
	{
		private static final long serialVersionUID = 1L;

		@Override
		protected void service(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException
		{
			//Connection conn = (Connection) ContextHolder.getContextObject(Connection.class);
			ContextHolder context = ContextManager.getInstance().getContext();
			Assert.assertNotNull("No context found!", context);
			Session session = context.get(Session.class);
			// the first four request are wfs requests and should contain a session
			if (FilterChainTest.requests < 5)
			{
				Assert.assertNotNull("No session found!", session);

				// check session wide context
				int appNum = ++context.get(ApplicationObject.class).num;
				int sesNum = ++context.get(SessionObject.class).num;
				int reqNum = ++context.get(RequestObject.class).num;

				// check application wide context
				Assert.assertEquals(FilterChainTest.requests, appNum);
				// check application wide context
				Assert.assertEquals(FilterChainTest.requests == 4 ? 1 : FilterChainTest.requests, sesNum);
				// check application wide context
				Assert.assertEquals(1, reqNum);

				// check mandant
				Mandant mandant = context.get(Mandant.class);
				Assert.assertNotNull("No mandant object in context found!", mandant);

				if (FilterChainTest.requests == 3)
				{
					session.setAuthentication(null);
					Assert.assertNull("Mandant should be null now", context.get(Mandant.class));
				}
				resp.getOutputStream().println(mandant.getStatus().toString());
			}
			else
			{
				Assert.assertNull("No session should be found!", session);
				resp.getOutputStream().println("no session");
			}
		}
	}

	static int requests = 0;

	@de.fnt.webgis.annotation.Context(instanceScope = Scope.APPLICATION)
	public static class ApplicationObject
	{
		public int num = 0;
	}

	@de.fnt.webgis.annotation.Context(instanceScope = Scope.SESSION)
	public static class SessionObject
	{
		public int num = 0;
	}

	@de.fnt.webgis.annotation.Context(instanceScope = Scope.REQUEST)
	public static class RequestObject
	{
		public int num = 0;
	}

	@Test
	public void testAuthenication() throws Exception
	{
		System.out.println("\nFirst: Should create a new anonymous mandant object");
		FilterChainTest.requests = 1;
		HttpTester response = httpTest("wfs/first");
		Assert.assertEquals(Status.ANONYMOUS, Status.valueOf(response.getContent()));

		// Outside the filter chain, no session should be available
		ContextHolder context = ContextManager.getInstance().getContext();
		Assert.assertEquals(1, context.get(ApplicationObject.class).num);
		Assert.assertEquals(0, context.get(SessionObject.class).num);
		Assert.assertEquals(0, context.get(RequestObject.class).num);
		Assert.assertNull(context.get(Session.class));
		Assert.assertNull(context.get(Mandant.class));

		System.out.println("\nSecond: Normal request");
		FilterChainTest.requests = 2;
		response = httpTest("wfs/second");
		Assert.assertEquals(Status.ANONYMOUS, Status.valueOf(response.getContent()));

		System.out.println("\nThird: Restore authenication");
		FilterChainTest.requests = 3;
		response = httpTest("wfs/third");
		Assert.assertEquals(Status.ANONYMOUS, Status.valueOf(response.getContent()));

		System.out.println("\nFourth: Delete cookies -> new anonymous mandant object");
		FilterChainTest.requests = 4;
		cookies.clear();
		response = httpTest("wfs/fourth");
		Assert.assertEquals(Status.ANONYMOUS, Status.valueOf(response.getContent()));

		System.out.println("\nFifth: No context should be initialized");
		FilterChainTest.requests = 5;
		response = httpTest("fifth");
		Assert.assertEquals("no session", response.getContent());
	}
}
