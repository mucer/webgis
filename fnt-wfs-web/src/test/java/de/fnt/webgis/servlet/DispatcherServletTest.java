package de.fnt.webgis.servlet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.geotools.GMLExtended.Version;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mortbay.jetty.Handler;
import org.mortbay.jetty.LocalConnector;
import org.mortbay.jetty.Server;
import org.mortbay.jetty.servlet.Context;
import org.mortbay.jetty.servlet.ServletHolder;
import org.mortbay.jetty.servlet.SessionHandler;
import org.mortbay.jetty.testing.HttpTester;

import de.fnt.webgis.context.ContextManager;
import de.fnt.webgis.filter.WfsExceptionFilter;
import de.fnt.webgis.filter.VirtualFilterChainProxy;
import de.fnt.webgis.ows.wfs.WfsConstants.Request;

public class DispatcherServletTest
{
	private static Server server;

	private static LocalConnector connector;

	/**
	 * This kicks off an instance of the Jetty
	 * servlet container so that we can hit it.
	 * We register an echo service that simply
	 * returns the parameters passed to it.
	 */
	@BeforeClass
	public static void startServletContainer() throws Exception
	{
		// start logger
		Logger.getRootLogger().addAppender(
			new ConsoleAppender(new PatternLayout("%5p [%t] (%F:%L) - %m%n")));

		ContextManager.getInstance().getApplicationContext().registerPackage("dummy");

		// create servlet
		ServletHolder servlet = new ServletHolder(DynamicWfsServlet.class);
		servlet.setName("dispatcher");
		servlet.setInitOrder(1);

		// add servlet to context
		Context context = new Context();
		context.setContextPath("/context");
		context.setResourceBase("target/webgis");
		context.addServlet(servlet, "/*");
		context.addFilter(WfsExceptionFilter.class, "/*", Handler.DEFAULT);
		context.addFilter(VirtualFilterChainProxy.class, "/*", Handler.DEFAULT);
		context.setSessionHandler(new SessionHandler());

		// add context to server
		server = new Server();
		connector = new LocalConnector();
		server.addConnector(connector);
		server.addHandler(context);

		server.start();
	}

	/**
		* Stops the Jetty container.
		*/
	@AfterClass
	public static void stopServletContainer() throws Exception
	{
		server.stop();
	}

	private HttpTester httpTest(String uri) throws Exception
	{
		return httpTest(uri, "GET", null);
	}

	private HttpTester httpTest(String uri, String method, String content) throws Exception
	{
		HttpTester request = new HttpTester();
		request.setMethod(method);
		request.setHeader("Host", "tester");
		request.setURI("/context/" + uri);
		request.setContent(content);

		HttpTester response = new HttpTester();
		connector.reopen();
		response.parse(connector.getResponses(request.generate()));
		assertTrue(response.getMethod() == null);
		//		assertTrue("Content type 'text/xml' expected!", response.getHeader("Content-Type") != null
		//			&& response.getHeader("Content-Type").startsWith("text/xml"));
		assertEquals(200, response.getStatus());

		return response;
	}

	/**
	 * @throws Exception 
	 * @throws  
	 * 
	 */
	@Test
	public void testExceptionReport() throws Exception
	{
		HttpTester response = httpTest("wfs?version=1.1.1");
		String message = ExceptionReportValidator.validate(response.getContent());
		System.out.println("Returned well formed message: " + message);

		response = httpTest("wfs?version=1.0.0&request=Bla");
		message = ExceptionReportValidator.validate(response.getContent());
		System.out.println("Returned well formed message: " + message);
	}

	@Test
	public void testGetCapabilities() throws Exception
	{
		HttpTester response = httpTest("wfs?version=" + Version.WFS1_0.getVersionStr() + "&request="
			+ Request.GetCapabilities);
		System.out.println(response.getContent());
	}

	@Test
	public void testDescribeFeatureType1_0() throws Exception
	{
		HttpTester response = httpTest("wfs?version=" + Version.WFS1_0.getVersionStr() + "&request="
			+ Request.DescribeFeatureType);
		SchemaValidator.validate(response.getContent(), Version.WFS1_0.getNamespace());
	}

	@Test
	public void testDescribeFeatureType1_1() throws Exception
	{
		HttpTester response = httpTest("wfs?version=" + Version.WFS1_1.getVersionStr() + "&request="
			+ Request.DescribeFeatureType);
		SchemaValidator.validate(response.getContent(), Version.WFS1_1.getNamespace());
		System.out.println(response.getContent());
	}

	@Test
	public void testDescribeFeatureType2_0() throws Exception
	{
		HttpTester response = httpTest("wfs?version=" + Version.WFS2_0.getVersionStr() + "&request="
			+ Request.DescribeFeatureType);
		SchemaValidator.validate(response.getContent(), Version.WFS2_0.getNamespace());
	}

	@Test
	public void testGetFeature() throws Exception
	{
		HttpTester response = httpTest("wfs?version=" + Version.WFS1_1.getVersionStr() + "&request="
			+ Request.GetFeature + "&typeName=node,tray");
		System.out.println(response.getContent());
	}

	@Test
	public void testTransactionCreate() throws Exception
	{
		HttpTester response = httpTest("wfs", "POST", readFile("transaction_request.xml"));
		System.out.println(response.getContent());
	}

	@Test
	public void testTransactionUpdate() throws Exception
	{
		HttpTester response = httpTest("wfs", "POST", readFile("update_request.xml"));
		System.out.println(response.getContent());
	}

	private String readFile(String fileName)
	{
		File resources = new File("src/test/resources");
		File file = new File(resources, fileName);
		StringBuffer buf = new StringBuffer();
		try
		{
			BufferedReader r = new BufferedReader(new InputStreamReader(new FileInputStream(file),
				"UTF-8"));
			String line;
			while ((line = r.readLine()) != null)
			{
				buf.append(line);
			}
			r.close();
		}
		catch (Exception e)
		{
			throw new RuntimeException("Could not read file '" + fileName + "'", e);
		}
		return buf.toString();
	}
}
