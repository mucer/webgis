package de.fnt.webgis.servlet;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;

import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.mortbay.jetty.Handler;
import org.mortbay.jetty.LocalConnector;
import org.mortbay.jetty.Server;
import org.mortbay.jetty.servlet.Context;
import org.mortbay.jetty.servlet.ServletHolder;
import org.mortbay.jetty.servlet.SessionHandler;
import org.mortbay.jetty.testing.HttpTester;

import de.fnt.webgis.context.ContextManager;
import de.fnt.webgis.filter.VirtualFilterChainProxy;
import de.fnt.webgis.filter.WfsExceptionFilter;

public class PerformanceTest
{
	private static String generateRequestString()
	{
		File file = new File("src/test/resources/transaction_request.xml");
		try
		{
			StringBuffer content = new StringBuffer();
			BufferedReader r = new BufferedReader(new InputStreamReader(new FileInputStream(file),
				"UTF-8"));
			String line;
			while ((line = r.readLine()) != null)
			{
				content.append(line);
			}
			r.close();

			HttpTester request = new HttpTester();
			request.setMethod("POST");
			request.setHeader("Host", "testexr");
			request.setURI("/wfs?username=test");
			request.setContent(content.toString());

			return request.generate();
		}
		catch (Exception e)
		{
			throw new RuntimeException("Could not read file '" + file.getAbsolutePath() + "'", e);
		}

	}

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception
	{
		boolean dynamic = false;

		// start logger
		//Logger.getRootLogger().addAppender(
		//	new ConsoleAppender(new PatternLayout("%5p [%t] (%F:%L) - %m%n")));
		String requestString = generateRequestString();

		// -------------- start init --------------
		long init = System.currentTimeMillis();

		// create servlet
		ServletHolder servlet = new ServletHolder(dynamic ? DynamicWfsServlet.class
			: StaticWfsServlet.class);
		servlet.setInitOrder(1);

		// add servlet to context
		Context context = new Context();
		context.setContextPath("/");
		context.setResourceBase("target/webgis");
		context.addServlet(servlet, "/*");
		context.addFilter(WfsExceptionFilter.class, "/*", Handler.DEFAULT);

		if (dynamic)
		{
			context.addFilter(VirtualFilterChainProxy.class, "/*", Handler.DEFAULT);
			context.setSessionHandler(new SessionHandler());
		}

		// add context to server
		Server server = new Server();
		LocalConnector connector = new LocalConnector();
		server.addConnector(connector);
		server.addHandler(context);

		// -------------- start server --------------
		if (dynamic)
		{
			ContextManager.getInstance().getApplicationContext().registerPackage("dummy");
		}
		server.start();
		init = System.currentTimeMillis() - init;

		// -------------- insert feature --------------
		int max = 11;
		long[] inserts = new long[max];
		long sum = 0;
		for (int i = 0; i < max; i++)
		{
			long insert = System.currentTimeMillis();
			connector.reopen();
			connector.getResponses(requestString);

			inserts[i] = System.currentTimeMillis() - insert;
			if (i > 0)
				sum += inserts[i];
		}

		server.stop();

		System.out.println("Init:    " + init);
		System.out.println("Inserts: " + Arrays.toString(inserts));
		System.out.println("         sum " + sum + ", avg " + (sum / (max - 1)));
	}
}
