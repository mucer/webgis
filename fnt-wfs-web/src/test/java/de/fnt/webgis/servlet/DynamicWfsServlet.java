package de.fnt.webgis.servlet;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.geotools.geometry.jts.GeometryBuilder;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.LineString;

import de.fnt.webgis.context.ContextManager;
import de.fnt.webgis.di.layer.DynamicFeatureLayerManagerBuilder;
import de.fnt.webgis.layer.FeatureLayerManager;
import de.fnt.webgis.session.Authentication;
import de.fnt.webgis.traymgmt.Node;
import de.fnt.webgis.traymgmt.Tray;
import de.fnt.webgis.traymgmt.TrayMgmt;
import de.fnt.webgis.util.ServletUtils;

public class DynamicWfsServlet extends WfsServlet
{
	private static final long serialVersionUID = 1L;

	private TrayMgmt dataLayer = null;

	@Override
	protected FeatureLayerManager initLayerManager()
	{
		ContextManager.getInstance().getApplicationContext().registerPackage("dummy");
		this.dataLayer = new TrayMgmt();
		DynamicFeatureLayerManagerBuilder builder = new DynamicFeatureLayerManagerBuilder();
		builder.addLayerInstance(this.dataLayer);
		return builder.build();
	}

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException
	{
		String pathInfo = ServletUtils.trimSlashes(request.getPathInfo());
		if (pathInfo.startsWith("data"))
		{
			Authentication auth = ContextManager.get(Authentication.class);
			String delete = request.getParameter("delete");
			if (delete != null)
			{
				Node node = this.dataLayer.getNode(auth, delete);
				if (node != null)
				{
					this.dataLayer.delete(auth, node);
				}
				Tray tray = this.dataLayer.getTray(auth, delete);
				if (tray != null)
				{
					this.dataLayer.delete(auth, tray);
				}
				response.sendRedirect("/webgis/data");
			}
			try
			{
				double x = Double.parseDouble(request.getParameter("x"));
				double y = Double.parseDouble(request.getParameter("y"));

				GeometryBuilder gb = new GeometryBuilder();
				Node n = new Node(null, gb.point(x, y));
				this.dataLayer.add(ContextManager.get(Authentication.class), n);
				response.sendRedirect("/webgis/data");
			}
			catch (Exception e)
			{

			}

			BufferedWriter w = new BufferedWriter(new OutputStreamWriter(response.getOutputStream()));
			w.write("<html><head><style>BODY {font-family: Verdana } TABLE { border: 1px solid grey; border-collapse: collapse } TD { border: 1px dotted grey }</style></head><body>");

			w.write("Aktueller Benutzer: <input id='user' type='text' value='" + auth.getUserName()
				+ "'/><button onclick=\"javascript:window.location.href='?username='+document.getElementById('user').value;\">Set</button>");
			w.write("<h2>Knoten</h2>");
			w.write("<table><tr><th>ID</th><th>X</th><th>Y</th><th></th></tr>");
			for (Node n : this.dataLayer.getNodes(auth))
			{
				w.write("<tr><td>" + n.getId() + "</td><td>" + n.getPos().getX() + "&deg;</td><td>"
					+ n.getPos().getY() + "&deg;</td><td><a href='?delete=" + n.getId() + "'>X</a></td></tr>");
			}
			w.write("</table>");
			w.write("<form action='data'>X <input type='text' name='x'/><br>Y <input type='text' name='y'/><br><input type='submit' value='Add'/></form><br>");

			w.write("<h2>Trassen</h2>");

			w.write("<table><tr><th>ID</th><th colspan=2>Verlauf</th><th></th></tr>");
			for (Tray t : this.dataLayer.getTrays(auth))
			{
				LineString p = t.getPath();
				Coordinate[] cs = p.getCoordinates();

				w.write("<tr><td rowspan=" + cs.length + ">" + t.getId() + "</td><td>" + cs[0].x
					+ "&deg;</td><td>" + cs[0].y + "&deg;</td><td rowspan=" + cs.length
					+ "><a href='?delete=" + t.getId() + "'/>X</a></td></tr>");
				for (int i = 1; i < cs.length; i++)
				{
					w.write("<tr><td>" + cs[i].x + "&deg;</td><td>" + cs[i].y + "&deg;</td></tr>");
				}
			}
			w.write("</table>");

			w.write("</body><html>");
			w.close();
		}
		else
		{
			super.service(request, response);
		}
	}
}
