package de.fnt.webgis.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import de.fnt.webgis.layer.FeatureLayerManager;
import de.fnt.webgis.session.Authentication;
import de.fnt.webgis.stat.layer.StaticLayerManager;
import dummy.Mandant;

public class StaticWfsServlet extends WfsServlet
{
	private static final long serialVersionUID = 1L;

	private ThreadLocal<Authentication> auths = new ThreadLocal<Authentication>();

	@Override
	protected FeatureLayerManager initLayerManager()
	{
		return new StaticLayerManager(this);
	}

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException
	{
		auths.set(new Mandant("dummy"));
		super.service(request, response);
	}

	public Authentication getAuthentication()
	{
		return auths.get();
	}

	public void setAuthentication(Authentication auth)
	{
		auths.set(auth);
	}
}
