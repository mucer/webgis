package de.fnt.webgis.servlet;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayInputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class SchemaValidator
{
	public static void validate(String xml, String gmlNamespace) throws Exception
	{
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setIgnoringElementContentWhitespace(true);
		factory.setIgnoringComments(true);
		DocumentBuilder builder = factory.newDocumentBuilder();
		ByteArrayInputStream in = new ByteArrayInputStream(xml.getBytes());
		Document doc = builder.parse(in);
		in.close();

		// check root node
		Element root = doc.getDocumentElement();
		assertEquals("xsd:schema", root.getNodeName());

		String value = root.getAttribute("xmlns:gml");
		assertEquals(gmlNamespace, value);
	}
}
