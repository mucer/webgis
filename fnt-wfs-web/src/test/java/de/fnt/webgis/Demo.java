package de.fnt.webgis;

import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.mortbay.jetty.Handler;
import org.mortbay.jetty.Server;
import org.mortbay.jetty.handler.ResourceHandler;
import org.mortbay.jetty.nio.SelectChannelConnector;
import org.mortbay.jetty.servlet.Context;
import org.mortbay.jetty.servlet.ServletHolder;
import org.mortbay.jetty.servlet.SessionHandler;

import de.fnt.webgis.filter.VirtualFilterChainProxy;
import de.fnt.webgis.filter.WfsExceptionFilter;
import de.fnt.webgis.servlet.DynamicWfsServlet;

/**
 * Starts a web service on port 8081 using Jetty.
 */
public class Demo
{
	public static Class<?> servletClass = DynamicWfsServlet.class;

	public static boolean addFilterChain = true;

	public static void main(String[] args)
	{
		try
		{
			// start logger
			Logger.getRootLogger().addAppender(
				new ConsoleAppender(new PatternLayout("%5p [%t] (%F:%L) - %m%n")));
			Logger.getRootLogger().setLevel(Level.DEBUG);

			// create servlet
			ServletHolder servlet = new ServletHolder(servletClass);
			servlet.setName("dispatcher");
			servlet.setInitOrder(1);

			// add servlet to context
			Context context = new Context();
			context.setSessionHandler(new SessionHandler());
			context.setContextPath("/webgis");
			context.setResourceBase("src/main/webapp");
			context.addServlet(servlet, "/*");
			context.addFilter(WfsExceptionFilter.class, "/wfs/*", Handler.DEFAULT);
			if (addFilterChain)
			{
				context.addFilter(VirtualFilterChainProxy.class, "/*", Handler.DEFAULT);
			}
			
			// resource handler
			ResourceHandler resources = new ResourceHandler();
			resources.setResourceBase("src/main/webapp");
			resources.setWelcomeFiles(new String[] { "layers.html" });

			// add context to server
			SelectChannelConnector connector = new SelectChannelConnector();
			connector.setPort(8081);

			Server server = new Server();
			server.addConnector(connector);
			server.addHandler(context);
			server.addHandler(resources);

			server.start();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}
