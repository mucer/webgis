package de.fnt.webgis.stat.layer;

import de.fnt.webgis.layer.FeatureLayerManager;
import de.fnt.webgis.servlet.StaticWfsServlet;

public class StaticLayerManager extends FeatureLayerManager
{
	public StaticLayerManager(StaticWfsServlet servlet)
	{
		addLayer(new StaticTrayMgmtLayer(servlet));
	}
}
