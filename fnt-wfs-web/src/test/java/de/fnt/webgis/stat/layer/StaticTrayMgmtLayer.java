package de.fnt.webgis.stat.layer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.geotools.feature.NameImpl;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.feature.simple.SimpleFeatureTypeBuilder;
import org.geotools.filter.FidFilter;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.filter.Filter;
import org.opengis.filter.spatial.BBOX;
import org.opengis.geometry.BoundingBox;

import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.Point;

import de.fnt.webgis.layer.FeatureLayer;
import de.fnt.webgis.servlet.StaticWfsServlet;
import de.fnt.webgis.session.Authentication;
import de.fnt.webgis.traymgmt.Node;
import de.fnt.webgis.traymgmt.Tray;
import de.fnt.webgis.traymgmt.TrayMgmt;

public class StaticTrayMgmtLayer extends FeatureLayer
{
	public static final String NAMESPACE_URI = "http://www.fnt.de/traymgmt";

	public final SimpleFeatureType nodeType;

	public final SimpleFeatureType trayType;

	final StaticWfsServlet servlet;

	final TrayMgmt dataLayer = new TrayMgmt();

	public StaticTrayMgmtLayer(StaticWfsServlet servlet)
	{
		super(NAMESPACE_URI);

		this.servlet = servlet;

		// build node type
		SimpleFeatureTypeBuilder builder = new SimpleFeatureTypeBuilder();
		builder.setName(new NameImpl(NAMESPACE_URI, "node"));
		builder.add("id", String.class);
		builder.add("the_geom", Point.class);
		this.nodeType = builder.buildFeatureType();
		addFeatureType(this.nodeType);

		// build tray type
		builder = new SimpleFeatureTypeBuilder();
		builder.setName(new NameImpl(NAMESPACE_URI, "tray"));
		builder.add("id", String.class);
		builder.add("the_geom", LineString.class);
		this.trayType = builder.buildFeatureType();
		addFeatureType(this.trayType);
	}

	@Override
	public List<SimpleFeature> searchFeatures(String typeName, Filter filter)
	{
		List<SimpleFeature> features = new ArrayList<SimpleFeature>();

		if (this.nodeType.getTypeName().equals(typeName))
		{
			Node[] nodes = searchNodes(filter);
			SimpleFeatureBuilder b = new SimpleFeatureBuilder(this.nodeType);
			for (Node node : nodes)
			{
				features.add(b.buildFeature(node.getId(), new Object[] { node.getId(), node.getPos() }));
			}
		}
		else if (this.trayType.getTypeName().equals(typeName))
		{
			Tray[] trays = searchTrays(filter);
			SimpleFeatureBuilder b = new SimpleFeatureBuilder(this.trayType);
			for (Tray tray : trays)
			{
				features.add(b.buildFeature(tray.getId(), new Object[] { tray.getId(), tray.getPath() }));
			}
		}
		else
		{
			throw new IllegalArgumentException("Unsupported type name '" + typeName + "' given!");
		}

		return features;
	}

	/**
	 * Uses the given filter to search for nodes
	 *
	 * @param filter Filter with Geometry or Id restriction
	 * @return The found nodes
	 */
	private Node[] searchNodes(Filter filter)
	{
		Node[] nodes = null;

		Authentication auth = servlet.getAuthentication();
		Geometry g = extractGeometry(filter);
		String[] ids = extractIds(filter);
		if (ids != null)
		{
			nodes = this.dataLayer.searchNodes(auth, ids);
		}
		else
		{
			nodes = this.dataLayer.searchNodes(auth, g);
		}
		return nodes;
	}

	/**
	 * Uses the given filter to search for trays
	 *
	 * @param filter Filter with Geometry restriction
	 * @return The found trays
	 */
	private Tray[] searchTrays(Filter filter)
	{
		Authentication auth = servlet.getAuthentication();
		Geometry g = extractGeometry(filter);
		Tray[] trays = this.dataLayer.searchTrays(auth, g);
		return trays;
	}

	/**
	 * Converts the given features to internal objects and stores them
	 * in the dataLayer.
	 *
	 * @param features Insert this features.
	 * @return The generated ids.
	 */
	@Override
	public String[] insertFeatures(SimpleFeature... features)
	{
		String ids[] = new String[features.length];
		Authentication auth = servlet.getAuthentication();
		for (int i = 0; i < features.length; i++)
		{
			SimpleFeature feature = features[i];
			if (feature.getType().getTypeName().equals(this.nodeType.getTypeName()))
			{
				String id = (String) feature.getAttribute("id");
				Point pos = (Point) feature.getAttribute("the_geom");
				ids[i] = dataLayer.add(this.servlet.getAuthentication(), new Node(id, pos));
			}
			else if (feature.getType().getTypeName().equals(this.trayType.getTypeName()))
			{
				String id = (String) feature.getAttribute("id");
				LineString path = (LineString) feature.getAttribute("the_geom");
				ids[i] = dataLayer.add(this.servlet.getAuthentication(), new Tray(id, path));
			}
			else
			{
				throw new IllegalArgumentException("Unsupported type name '" + feature.getType()
					.getTypeName() + "' given!");
			}
		}
		return ids;
	}

	/**
	 * Search features for the given restrictions and applies the new properties.
	 *
	 * @param typeName Update this type name
	 * @param filter Use this search restriction
	 * @param properties Apply this properties
	 * @return The number of updated features
	 */
	@Override
	public int updateFeatures(String typeName, Filter filter, Map<String, Object> properties)
	{
		int updated = 0;
		if (this.nodeType.getTypeName().equals(typeName))
		{
			Node[] nodes = searchNodes(filter);
			for (Node node : nodes)
			{
				if (properties.containsKey("the_geom"))
				{
					node.setPos((Point) properties.get("the_geom"));
				}
				updated++;
			}
		}
		else if (this.trayType.getTypeName().equals(typeName))
		{
			Tray[] trays = searchTrays(filter);
			for (Tray tray : trays)
			{
				if (properties.containsKey("the_geom"))
				{
					tray.setPath((LineString) properties.get("the_geom"));
				}
				updated++;
			}
		}
		else
		{
			throw new IllegalArgumentException("Unsupported type name '" + typeName + "' given!");
		}

		return updated;
	}

	/**
	 * Search features with the given restrictions and deletes them.
	 *
	 * @param typeName Use this type name.
	 * @param filter Use this restrictions.
	 * @return The number of deleted features.
	 */
	@Override
	public int deleteFeatures(String typeName, Filter filter)
	{
		Authentication auth = servlet.getAuthentication();
		int deleted = 0;
		if (this.nodeType.getTypeName().equals(typeName))
		{
			Node[] nodes = searchNodes(filter);
			for (Node node : nodes)
			{
				this.dataLayer.delete(auth, node);
				deleted++;
			}
		}
		else if (this.trayType.getTypeName().equals(typeName))
		{
			Tray[] trays = searchTrays(filter);
			for (Tray tray : trays)
			{
				this.dataLayer.delete(auth, tray);
				deleted++;
			}
		}
		else
		{
			throw new IllegalArgumentException("Unsupported type name '" + typeName + "' given!");
		}

		return deleted;
	}

	protected String[] extractIds(Filter filter)
	{
		String[] ids = null;
		;
		if (filter != null && filter instanceof FidFilter)
		{
			FidFilter fid = (FidFilter) filter;
			Set<Object> set = fid.getIDs();
			ids = new String[set.size()];
			int i = 0;
			for (Object id : set)
			{
				ids[i++] = id.toString();
			}
		}
		return ids;
	}

	protected Geometry extractGeometry(Filter filter)
	{
		Geometry g = null;
		if (filter != null && filter instanceof BBOX)
		{
			BBOX bbox = (BBOX) filter;
			BoundingBox bounds = bbox.getBounds();
			// also add geometry object
			if (bounds instanceof Envelope)
			{
				GeometryFactory f = new GeometryFactory();
				g = f.toGeometry((Envelope) bounds);
			}
		}
		return g;
	}
}
