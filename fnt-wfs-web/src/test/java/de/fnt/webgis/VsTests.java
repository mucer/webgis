package de.fnt.webgis;

import java.util.List;

import junit.framework.Assert;

import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.filter.Filter;
import org.geotools.filter.FilterFactory;
import org.geotools.filter.FilterFactoryImpl;
import org.geotools.geometry.jts.GeometryBuilder;
import org.opengis.feature.simple.SimpleFeature;

import com.vividsolutions.jts.geom.Point;

import de.fnt.webgis.servlet.StaticWfsServlet;
import de.fnt.webgis.stat.layer.StaticTrayMgmtLayer;
import de.fnt.webgis.traymgmt.Node;
import de.fnt.webgis.traymgmt.TrayMgmt;
import dummy.Mandant;

/**
 * Shows different test styles (with and without the framework)
 * @author daniel
 *
 */
public class VsTests
{
	@SuppressWarnings("deprecation")
	public static void main(String[] args)
	{
		String id = "ID";
		Point point = new GeometryBuilder().point(1.0, 2.0);
		Mandant mandant = new Mandant("test");

		// Test with framework
		TrayMgmt layer = new TrayMgmt();
		Node node = new Node(id, point);
		layer.add(mandant, node);
		Node result = layer.getNode(mandant, node.getId());
		Assert.assertEquals(node, result);

		// Test without framework
		StaticWfsServlet servlet = new StaticWfsServlet();
		servlet.setAuthentication(mandant);
		StaticTrayMgmtLayer layer2 = new StaticTrayMgmtLayer(servlet);
		SimpleFeatureBuilder sfb = new SimpleFeatureBuilder(layer2.nodeType);
		SimpleFeature feature = sfb.buildFeature(id, new Object[] { id, point });
		layer2.insertFeatures(feature);
		FilterFactory factory = new FilterFactoryImpl();
		Filter filter = factory.createFidFilter(id);
		List<SimpleFeature> results = layer2.searchFeatures(layer2.nodeType.getTypeName(), filter);
		Assert.assertEquals(1, results.size());
		SimpleFeature result2 = results.get(0);
		Assert.assertEquals(id, result2.getAttribute("id"));
		Assert.assertEquals(point, result2.getAttribute("the_geom"));
	}
}
