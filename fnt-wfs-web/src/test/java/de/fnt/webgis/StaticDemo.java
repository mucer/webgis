package de.fnt.webgis;

import de.fnt.webgis.servlet.StaticWfsServlet;

/**
 * Overwrites some settings of the {@link Demo} programm and
 * starts a static version of the demo.
 */
public class StaticDemo
{
	public static void main(String[] args)
	{
		Demo.servletClass = StaticWfsServlet.class;
		Demo.addFilterChain = false;
		Demo.main(args);
	}

}
