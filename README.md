# WebGIS Prototype

WebGIS Prototype where the Server only offers a WFS interface to interact with the data.

## Start

Import the projects as Maven projects into eclipse. Lauch the "Demo.java" in the project "fnt-wfs-web"

Open http://localhost:8081/webgis/data to control the login and see the raw data.

Open http://localhost:8081 to see the WebGIS