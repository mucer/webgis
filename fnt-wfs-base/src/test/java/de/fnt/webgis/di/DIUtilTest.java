package de.fnt.webgis.di;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.geotools.geometry.jts.GeometryBuilder;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.Point;

import dummy.custom.first.FirstFeature2Custom;
import dummy.first.FirstFeature1;

public class DIUtilTest
{
	@BeforeClass
	public static void before() throws Exception
	{
		// start logger
		Logger.getRootLogger().addAppender(
			new ConsoleAppender(new PatternLayout("%5p [%t] (%F:%L) - %m%n")));
	}

	@Test
	public void testApply()
	{
		GeometryBuilder b = new GeometryBuilder();
		Point point = b.point(1, 1);
		LineString line = b.lineString(1, 1, 2, 2);

		FirstFeature1 f1 = new FirstFeature1("oldid", null);
		FirstFeature2Custom f2 = new FirstFeature2Custom("oldid", line, "oldcustom");

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("id", "newid");
		map.put("customAtt", "newcustom");
		map.put("Point", point);
		map.put("line", line);

		DIUtil.apply(f1, map);
		Assert.assertEquals("newid", f1.getId());
		Assert.assertEquals(point, f1.getPoint());

		DIUtil.apply(f2, map);
		Assert.assertEquals("newid", f2.getId());
		Assert.assertEquals(line, f2.getLine());
		Assert.assertEquals("newcustom", f2.getCustomAtt());
	}
}
