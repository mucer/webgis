package de.fnt.webgis.di.feature;

import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.geotools.geometry.jts.GeometryBuilder;
import org.junit.BeforeClass;
import org.junit.Test;

import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.util.Assert;

import dummy.first.FirstFeature1;
import dummy.first.FirstFeature2;

public class AnnotationFeatureTest
{
	@BeforeClass
	public static void appendLogger() throws Exception
	{
		// start logger
		Logger.getRootLogger().addAppender(
			new ConsoleAppender(new PatternLayout("%5p [%t] (%F:%L) - %m%n")));
	}

	@Test
	public void test()
	{
		GeometryBuilder geo = new GeometryBuilder();
		Point point = geo.point(1, 2);
		LineString line = geo.lineString(1, 2, 3, 4, 5, 6);
		FirstFeature1 first1 = new FirstFeature1("id1", point);
		FirstFeature2 first2 = new FirstFeature2("id2");
		first2.setLine(line);

		DynamicFeatureType first1Type = new DynamicFeatureTypeBuilder("http://www.fnt.de/test",
			FirstFeature1.class).buildFeatureType();
		DynamicFeatureType first2Type = new DynamicFeatureTypeBuilder("http://www.fnt.de/test",
			FirstFeature2.class).buildFeatureType();
		DynamicFeature first1Feature = new DynamicFeatureBuilder(first1Type).parseObject(first1);
		DynamicFeature first2Feature = new DynamicFeatureBuilder(first2Type).parseObject(first2);

		// check feature
		Assert.equals("id1", first1Feature.getAttribute("id"));
		Assert.equals(point, first1Feature.getAttribute("the_geom"));
		Assert.equals("id2", first2Feature.getAttribute("id"));
		Assert.equals(line, first2Feature.getAttribute("the_geom"));

		// Reassign
		first1 = (FirstFeature1) first1Feature.newFeatureClassInstance();
		Assert.equals("id1", first1.getId());
		Assert.equals(point, first1.getPoint());

		first2 = (FirstFeature2) first2Feature.newFeatureClassInstance();
		Assert.equals("id2", first2.getId());
		Assert.equals(line, first2.getLine());
	}
}
