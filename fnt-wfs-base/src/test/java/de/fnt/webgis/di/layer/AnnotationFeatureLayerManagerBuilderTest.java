package de.fnt.webgis.di.layer;

import java.util.Collection;

import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.geotools.feature.NameImpl;
import org.geotools.geometry.jts.GeometryBuilder;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import de.fnt.webgis.context.ContextManager;
import de.fnt.webgis.di.feature.DynamicFeature;
import de.fnt.webgis.di.feature.DynamicFeatureBuilder;
import de.fnt.webgis.di.feature.DynamicFeatureType;
import de.fnt.webgis.layer.FeatureLayerManager;
import dummy.first.FirstFeature1;
import dummy.first.FirstFeature2;
import dummy.first.TestParam;

public class AnnotationFeatureLayerManagerBuilderTest
{
	@BeforeClass
	public static void before() throws Exception
	{
		// start logger
		Logger.getRootLogger().addAppender(
			new ConsoleAppender(new PatternLayout("%5p [%t] (%F:%L) - %m%n")));
		Logger.getRootLogger().setLevel(Level.INFO);
	}

	@Test
	public void buildManager() throws Exception
	{
		DynamicFeatureLayerManagerBuilder managerBuilder = new DynamicFeatureLayerManagerBuilder();
		managerBuilder.parsePackage("dummy.first");
		managerBuilder.parsePackage("dummy.second");

		// check layer builder count from manager builder
		Collection<DynamicFeatureLayerBuilder> layerBuilders = managerBuilder.getFeatureLayerBuilders();
		Assert.assertEquals("Invalid number of layer builders", 2, layerBuilders.size());

		// get layer builders from manager builder
		DynamicFeatureLayerBuilder firstLayerBuilder = null;
		DynamicFeatureLayerBuilder secondLayerBuilder = null;
		for (DynamicFeatureLayerBuilder layerBuilder : layerBuilders)
		{
			if (layerBuilder.getNamespaceURI().equals("http://www.fnt.de/first"))
			{
				firstLayerBuilder = layerBuilder;
			}
			else if (layerBuilder.getNamespaceURI().equals("http://www.fnt.de/second"))
			{
				secondLayerBuilder = layerBuilder;
			}
		}

		// check layer builders
		Assert.assertNotNull("First layer not found", firstLayerBuilder);
		Assert.assertNotNull("Second layer not found", secondLayerBuilder);

		// build manager
		FeatureLayerManager manager = managerBuilder.build();

		// check feature types
		DynamicFeatureType first1Type = (DynamicFeatureType) manager.getFeatureType(new NameImpl(
			"http://www.fnt.de/first", "first1"));
		Assert.assertNotNull("Feature 'first1' not found", first1Type);
		Assert.assertEquals(FirstFeature1.class, first1Type.getFeatureClass());
		DynamicFeatureType first2Type = (DynamicFeatureType) manager.getFeatureType(new NameImpl(
			"http://www.fnt.de/first", "first2"));
		Assert.assertNotNull("Feature 'first2' not found", first2Type);
		Assert.assertEquals(FirstFeature2.class, first2Type.getFeatureClass());

		// build features
		GeometryBuilder gb = new GeometryBuilder();
		DynamicFeatureBuilder first1Builder = new DynamicFeatureBuilder(first1Type);
		DynamicFeature first1Feature = first1Builder.parseObject(new FirstFeature1("1", gb
			.point(20, 20)));

		TestParam param = new TestParam();
		ContextManager.getInstance().getApplicationContext().registerInstance("param", param);

		// insert
		String result = manager.insertFeature(first1Feature);
		Assert.assertEquals("insert", result);
		Assert.assertEquals("insert", param.result);

		// search
		manager.searchFeatures(first2Type.getName(), null);
		Assert.assertEquals("standard", param.result);

		// parse custom
		managerBuilder.parsePackage("dummy.custom");
		manager = managerBuilder.build();

		// search
		manager.searchFeatures(first2Type.getName(), null);
		Assert.assertEquals("custom", param.result);

		// reparse standard
		managerBuilder.parsePackage("de.fnt.module.first");
		manager = managerBuilder.build();

		// search
		manager.searchFeatures(first2Type.getName(), null);
		Assert.assertEquals("custom", param.result);

		//		FeatureLayerManager manager = builder.build();
		//		List<FeatureLayer> layers = manager.getLayers();
		//		for (FeatureLayer layer : layers)
		//		{
		//			//String namespaceURI = layer.getNamespaceURI();
		//
		//			System.out.println(layer);
		//			for (SimpleFeatureType featureType : layer.getFeatureTypes())
		//			{
		//				System.out.println("  " + featureType);
		//			}
		//
		//			//GMLEncoder encoder = new GMLEncoder(GML.Version.WFS1_1);
		//			//encoder.setFeatureLayerManager(manager);
		//
		//			//encoder.setBaseURL(new URL("http://localhost:8080/fnt-wfs/schemas"));
		//			//encoder.setNamespace(manager.getPrefix(namespaceURI), namespaceURI);
		//			//encoder.encode(System.out, "UTF-8", types.toArray(new SimpleFeatureType[types.size()]));
		//		}
		//
		//		XSDSchema[] schemas = manager.describeFeatureTypes(GML.Version.WFS1_1,
		//			AnnotationFeatureLayerManagerBuilderTest.class.getResource("."));
		//
		//		System.out.println("Generated " + schemas.length + " schema(s)");
		//
		//		for (XSDSchema schema : schemas)
		//		{
		//			XSDResourceImpl.serialize(System.out, schema.getElement(), "UTF-8");
		//		}
		//
		//		FilterFactory ff = CommonFactoryFinder.getFilterFactory();
		//
		//		SimpleFeatureType node = manager.getFeatureType(new NameImpl("http://www.fnt.de/traymgmt",
		//			"node"));
		//		List<SimpleFeature> nodes = manager.searchFeatures(node, ff.bbox("the_geom", 0, 0, 100, 100,
		//			"EPSG:4326", MatchAction.ANY));
		//
		//		SimpleFeatureType tray = manager.getFeatureType(new NameImpl("http://www.fnt.de/traymgmt",
		//			"tray"));
		//		List<SimpleFeature> trays = manager.searchFeatures(tray, null);
		//
		//		FeatureCollectionType collection = WfsFactory.eINSTANCE.createFeatureCollectionType();
		//		collection.getFeature().add(new ListFeatureCollection(node, nodes));
		//		collection.getFeature().add(new ListFeatureCollection(tray, trays));
		//
		//		GMLEncoder encoder = new GMLEncoder(GML.Version.WFS1_1);
		//		encoder.setBaseURL(new URL("http://localhost:8080/fnt-wfs/schemas"));
		//		encoder.setIndenting(true);
		//		encoder.encode(System.out, collection);
	}
}
