package de.fnt.webgis.di.feature;

import java.util.List;

import junit.framework.Assert;

import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.junit.BeforeClass;
import org.junit.Test;
import org.opengis.feature.type.AttributeDescriptor;

import de.fnt.webgis.di.attribute.DynamicAttributeDescriptor;
import dummy.custom.first.FirstFeature2Custom;
import dummy.first.FirstFeature1;
import dummy.first.FirstFeature2;

public class AnnotationFeatureTypeBuilderTest
{
	@BeforeClass
	public static void appendLogger() throws Exception
	{
		// start logger
		Logger.getRootLogger().addAppender(
			new ConsoleAppender(new PatternLayout("%5p [%t] (%F:%L) - %m%n")));
	}

	@Test
	public void buildFeatureType()
	{
		DynamicFeatureTypeBuilder builder = new DynamicFeatureTypeBuilder("http://www.fnt.de/test",
			FirstFeature1.class);
		DynamicFeatureType type = builder.buildFeatureType();
		// expect id, line
		List<AttributeDescriptor> attributes = type.getAttributeDescriptors();
		Assert.assertEquals("Invalid number of attributes", 2, attributes.size());
		Assert.assertEquals("Invalid binding class", FirstFeature1.class, type.getFeatureClass());
		DynamicAttributeDescriptor attrib = (DynamicAttributeDescriptor) type.getDescriptor("id");
		Assert.assertNotNull(attrib);
		Assert.assertTrue(attrib.getType().isIdentified());

		builder = new DynamicFeatureTypeBuilder("http://www.fnt.de/test", FirstFeature2.class);
		type = builder.buildFeatureType();
		attributes = type.getAttributeDescriptors();
		Assert.assertEquals("Invalid number of attributes", 2, attributes.size());
		Assert.assertEquals("Invalid binding class", FirstFeature2.class, type.getFeatureClass());
		attrib = (DynamicAttributeDescriptor) type.getDescriptor("id");
		Assert.assertNotNull(attrib);
		Assert.assertTrue(attrib.getType().isIdentified());

		builder = new DynamicFeatureTypeBuilder("http://www.fnt.de/test", FirstFeature2Custom.class);
		type = builder.buildFeatureType();
		attributes = type.getAttributeDescriptors();
		Assert.assertEquals("Invalid number of attributes", 3, attributes.size());
		Assert.assertEquals("Invalid binding class", FirstFeature2Custom.class, type.getFeatureClass());
	}
}
