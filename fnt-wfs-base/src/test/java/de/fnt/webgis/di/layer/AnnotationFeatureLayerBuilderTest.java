package de.fnt.webgis.di.layer;

import java.util.List;

import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import de.fnt.webgis.annotation.LayerAction;
import de.fnt.webgis.di.layer.DynamicFeatureLayerAction.Update;
import de.fnt.webgis.di.layer.DynamicFeatureLayerAction.UpdateType;
import dummy.first.FirstLayer;

public class AnnotationFeatureLayerBuilderTest
{
	@BeforeClass
	public static void before() throws Exception
	{
		// start logger
		Logger.getRootLogger().addAppender(
			new ConsoleAppender(new PatternLayout("%5p [%t] (%F:%L) - %m%n")));
	}

	@Test
	public void buildFeatureLayer()
	{
		DynamicFeatureLayerBuilder builder = new DynamicFeatureLayerBuilder();
		builder.addLayer(FirstLayer.class);
		DynamicFeatureLayer layer = builder.build(null);

		List<DynamicFeatureLayerAction> actions = layer.getLayerActions(LayerAction.ActionType.UPDATE,
			null);
		Assert.assertEquals(3, actions.size());
		boolean filter = false;
		boolean multiFeature = false;
		boolean singleFeature = false;
		for (DynamicFeatureLayerAction action : actions)
		{
			Update u = (Update) action;
			if (u.getUpdateType() == UpdateType.FILTER)
			{
				Assert.assertNull(u.getFeatureClass());
				filter = true;
			}
			else if (u.getUpdateType() == UpdateType.MULTI_FEATURE)
			{
				//				Assert.assertEquals(FirstFeature.class, u.getFeatureClass());
				multiFeature = true;
			}
			else if (u.getUpdateType() == UpdateType.SINGLE_FEATURE)
			{
				//				Assert.assertEquals(FirstFeature.class, u.getFeatureClass());
				singleFeature = true;
			}
		}

		Assert.assertTrue("No FILTER update type", filter);
		Assert.assertTrue("No MULTI_FEATURE update type", multiFeature);
		Assert.assertTrue("No SINGLE_FEATURE update type", singleFeature);

		//AnnotationFeatureType type = (AnnotationFeatureType) layer.getFeatureTypes().get(0);
		//		type.newBindingInstance(feature)
		//
		//		layer.searchFeatures(type, filter);
		//		layer.updateFeatures(type, filter, properties);
		//		layer.deleteFeatures(type, filter);
	}
}
