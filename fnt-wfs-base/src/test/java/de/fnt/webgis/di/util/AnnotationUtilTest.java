package de.fnt.webgis.di.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;

import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import de.fnt.webgis.annotation.AnnotationUtil;
import de.fnt.webgis.annotation.Layer;

public class AnnotationUtilTest
{
	@Target({ ElementType.FIELD, ElementType.METHOD, ElementType.TYPE })
	@Retention(RetentionPolicy.RUNTIME)
	static @interface TestAnnotation
	{
		int value();
	}

	@TestAnnotation(1)
	static class superClass
	{
		@TestAnnotation(2)
		public int field = 2;

		@TestAnnotation(6)
		protected int privateField = 6;

		@TestAnnotation(3)
		public int method()
		{
			return 3;
		}
	}

	@TestAnnotation(4)
	static interface superInterface
	{
		@TestAnnotation(5)
		int method();
	}

	static class subClass1 extends superClass
	{

	}

	static class subClass2 extends superClass
	{
		@Override
		public int method()
		{
			return 4;
		}
	}

	static class subClassInterface implements superInterface
	{
		public int method()
		{
			return 0;
		}
	}

	@BeforeClass
	public static void appendLogger() throws Exception
	{
		// start logger
		Logger.getRootLogger().addAppender(
			new ConsoleAppender(new PatternLayout("%5p [%t] (%F:%L) - %m%n")));
	}

	@Test
	public void testSearchClasses() throws Exception
	{
		// all classes, incl. sub directories
		List<Class<?>> classes = AnnotationUtil.getClasses("dummy");
		assertEquals(10, classes.size());

		// classes for annotation layer
		classes = AnnotationUtil.getClasses("dummy", Layer.class);
		assertEquals(3, classes.size());

		// with out sub directories
		classes = AnnotationUtil.getClasses("dummy", Layer.class, false);
		assertEquals(0, classes.size());

		// in library
		classes = AnnotationUtil.getClasses("org.junit");
		int deep = classes.size();
		Assert.assertTrue("No classes in package 'org.junit' found!", deep > 0);
		classes = AnnotationUtil.getClasses("org.junit", false);
		int single = classes.size();
		Assert.assertTrue("No classes direct in package 'org.junit' found!", single > 0);
		Assert.assertTrue("Expecting lesser classes without sub packages", single < deep);

	}

	@Test
	public void testInheritance() throws Exception
	{
		TestAnnotation anno;

		// Test class 
		anno = AnnotationUtil.getAnnotation(superClass.class, TestAnnotation.class);
		assertNotNull("Annotation not found (class)", anno);
		assertEquals("Invalid number in annotation (class)", 1, anno.value());

		// Test extended class
		anno = AnnotationUtil.getAnnotation(subClass1.class, TestAnnotation.class);
		assertNotNull("Annotation not found (extended class)", anno);
		assertEquals("Invalid number in annotation (extended class)", 1, anno.value());

		// Test interface
		anno = AnnotationUtil.getAnnotation(superInterface.class, TestAnnotation.class);
		assertNotNull("Annotation not found (interface)", anno);
		assertEquals("Invalid number in annotation (interface)", 4, anno.value());

		// Test implemented class
		anno = AnnotationUtil.getAnnotation(subClassInterface.class, TestAnnotation.class);
		assertNotNull("Annotation not found (implemented class)", anno);
		assertEquals("Invalid number in annotation (implemented class)", 4, anno.value());

		// Test field
		List<Field> fields = AnnotationUtil.getFields(superClass.class, TestAnnotation.class, false);
		assertEquals("Invalid number of fields found", 1, fields.size());
		anno = AnnotationUtil.getAnnotation(fields.get(0), TestAnnotation.class);
		assertNotNull("Annotation not found (field in class)", anno);
		assertEquals("Invalid number in annotation (field in class)", 2, anno.value());

		// Test field
		fields = AnnotationUtil.getFields(superClass.class, TestAnnotation.class, true);
		assertEquals("Invalid number of fields found", 2, fields.size());
		anno = AnnotationUtil.getAnnotation(fields.get(0), TestAnnotation.class);
		assertNotNull("Annotation not found (field in class)", anno);
		assertEquals("Invalid number in annotation (field in class)", 2, anno.value());

		// Test field in extended class
		fields = AnnotationUtil.getFields(subClass1.class, TestAnnotation.class, true);
		assertEquals("Invalid number of fields found", 2, fields.size());
		anno = AnnotationUtil.getAnnotation(fields.get(0), TestAnnotation.class);
		assertNotNull("Annotation not found (field in extended class)", anno);
		assertEquals("Invalid number in annotation (field in extended class)", 2, anno.value());

		// Test methods
		Method[] methods = AnnotationUtil.getMethods(superClass.class, TestAnnotation.class);
		assertEquals("Invalid number of methods found", 1, methods.length);
		anno = AnnotationUtil.getAnnotation(methods[0], TestAnnotation.class);
		assertNotNull("Annotation not found (method in class)", anno);
		assertEquals("Invalid number in annotation (method in class)", 3, anno.value());

		// Test methods
		methods = AnnotationUtil.getMethods(subClass1.class, TestAnnotation.class);
		assertEquals("Invalid number of methods found (method in extended class)", 1, methods.length);
		anno = AnnotationUtil.getAnnotation(methods[0], TestAnnotation.class);
		assertNotNull("Annotation not found (method in extended class)", anno);
		assertEquals("Invalid number in annotation (method in extended class)", 3, anno.value());

		// Test methods
		methods = AnnotationUtil.getMethods(subClass2.class, TestAnnotation.class);
		assertEquals("Invalid number of methods found (overwritten method in extended class)", 0,
			methods.length);
	}
}
