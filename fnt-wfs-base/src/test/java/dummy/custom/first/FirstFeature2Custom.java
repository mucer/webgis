package dummy.custom.first;

import com.vividsolutions.jts.geom.LineString;

import de.fnt.webgis.annotation.Attribute;
import dummy.first.FirstFeature2;

public class FirstFeature2Custom extends FirstFeature2
{
	@Attribute(nillable = true)
	public String customAtt;

	public FirstFeature2Custom(String id, LineString line, String customAtt)
	{
		super(id);
		setLine(line);
	}

	public String getCustomAtt()
	{
		return customAtt;
	}
}
