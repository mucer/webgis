package dummy.custom.first;

import org.opengis.geometry.BoundingBox;

import dummy.first.FirstLayer;
import dummy.first.TestParam;

public class FirstLayerCustom extends FirstLayer
{
	@Override
	public FirstFeature2Custom[] search2(BoundingBox box, TestParam param)
	{
		param.result = "custom";
		return null;
	}

	public FirstFeature3[] search3(BoundingBox box)
	{
		return null;
	}
}
