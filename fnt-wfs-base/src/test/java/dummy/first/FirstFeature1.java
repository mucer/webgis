package dummy.first;

import com.vividsolutions.jts.geom.Point;

import de.fnt.webgis.annotation.Attribute;
import de.fnt.webgis.annotation.Feature;

@Feature("first1")
public class FirstFeature1 implements FirstFeature
{
	String id;

	Point point;

	public FirstFeature1(String id, Point point)
	{
		this.id = id;
		this.point = point;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	@Attribute(isUnique = true)
	public String getId()
	{
		return id;
	}

	public void setPoint(Point point)
	{
		this.point = point;
	}

	public Point getPoint()
	{
		return point;
	}
}
