package dummy.first;

import com.vividsolutions.jts.geom.LineString;

import de.fnt.webgis.annotation.Attribute;
import de.fnt.webgis.annotation.Feature;

@Feature("first2")
public class FirstFeature2 implements FirstFeature
{
	@Attribute(isUnique = true)
	String id;

	LineString line = null;

	public FirstFeature2(String id)
	{
		this.id = id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getId()
	{
		return id;
	}

	public void setLine(LineString line)
	{
		this.line = line;
	}

	public LineString getLine()
	{
		return line;
	}
}
