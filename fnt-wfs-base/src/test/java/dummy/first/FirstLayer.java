package dummy.first;

import static de.fnt.webgis.annotation.LayerAction.ActionType.DELETE;
import static de.fnt.webgis.annotation.LayerAction.ActionType.INSERT;
import static de.fnt.webgis.annotation.LayerAction.ActionType.SEARCH;

import java.sql.Connection;
import java.util.Map;

import org.opengis.filter.Filter;
import org.opengis.geometry.BoundingBox;

import de.fnt.webgis.annotation.Layer;
import de.fnt.webgis.annotation.LayerAction;

@Layer("http://www.fnt.de/first")
public class FirstLayer
{
	public FirstFeature1[] search1(BoundingBox box)
	{
		return null;
	}

	public FirstFeature2[] search2(BoundingBox box, TestParam param)
	{
		param.result = "standard";
		return null;
	}

	@LayerAction(SEARCH)
	public FirstFeature[] sbSearch(BoundingBox box, TestParam param, Connection conn)
	{
		return null;
	}

	public String insert(FirstFeature feature, TestParam param)
	{
		param.result = "insert";
		return "insert";
	}

	@LayerAction(INSERT)
	public String[] dbInsert(FirstFeature[] features, Connection conn)
	{
		return null;
	}

	public void delete(FirstFeature feature)
	{
	}

	@LayerAction(DELETE)
	public void dbDelete(FirstFeature[] features, Connection conn)
	{
	}

	public void update(FirstFeature feature, TestParam param)
	{
		param.result = "update";
	}

	public void update(FirstFeature[] features, TestParam param)
	{
		param.result = "multiUpdate";
	}

	public void update(Filter filter, Map<String, Object> properties, TestParam param)
	{
		param.result = "filterUpdate";
	}

	//	@LayerAction(UPDATE)
	//	public void bla(String id, Map<String, Object> properties, TestParam param)
	//	{
	//		param.result = "singleIdUpdate";
	//	}
	//
	//	@LayerAction(UPDATE)
	//	public void bla(String[] ids, Map<String, Object> properties, TestParam param)
	//	{
	//		param.result = "multiIdUpdate";
	//	}
}
