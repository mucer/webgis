package de.fnt.webgis.context;

import java.lang.reflect.Field;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Stack;

import org.apache.log4j.Logger;

import de.fnt.webgis.annotation.AnnotationUtil;
import de.fnt.webgis.annotation.Context;
import de.fnt.webgis.di.DIUtil;
import de.fnt.webgis.di.DependencyException;
import de.fnt.webgis.di.MethodComparator;

/**
 * Intern class to manage the context objects in the {@linkplain ContextHolder}.
 * 
 * @see ClassContent
 * @see MemberContent
 * @see StaticContent
 */
abstract class ContextContent implements Comparable<ContextContent>
{
	private static final Logger logger = Logger.getLogger(ContextContent.class);

	public static final int PRIORITY_STATIC = 10;

	public static final int PRIORITY_PROTOTYPE = 20;

	/** The name of this context object */
	private String name;

	/** The scope for the context object */
	private Scope scope;

	/**
	 * Sets the scope for this content. The defaut scope ist {@link Scope#REQUEST}.
	 * 
	 * @param scope The scope. If null the default scope is used.
	 */
	protected ContextContent(String name, Scope scope, Context annotation)
	{
		this.name = name;
		this.scope = scope;

		if (annotation != null)
		{
			if (this.name == null)
			{
				this.name = annotation.value();
			}
			if (this.scope == null)
			{
				this.scope = annotation.scope();
			}
		}

		if (this.scope == null)
		{
			this.scope = Scope.REQUEST;
		}
	}

	/**
	 * Sets the scope for this content.
	 * 
	 * @param scope The new scope
	 * @see Scope
	 */
	protected void setScope(Scope scope)
	{
		this.scope = scope;
	}

	/**
	 * Gets the current scope of this content
	 * 
	 * @return The current scope
	 * @see Scope
	 */
	public Scope getScope()
	{
		return this.scope;
	}

	/**
	 * Compares two {@link ContextContent}s. In this step the 
	 * return value of the method {@link ContextContent#getPriority()} is
	 * used (lower value means higher priority).
	 * 
	 * @param o Compare the this context object with this context object
	 * @return The compare result.
	 * @see Comparable#compareTo(Object)
	 */
	public int compareTo(ContextContent o)
	{
		return getPriority() - o.getPriority();
	}

	/**
	 * Checks whether this context object can be used to satisfy the
	 * given class.
	 * 
	 * @param cls Check this class.
	 * @return Returns "true" if this context object can be used.
	 */
	public boolean matches(Class<?> cls)
	{
		// TODO handle arrays
		Class<?> valueType = getValueType();
		return (valueType != null && cls.isAssignableFrom(valueType));
	}

	/**
	 * Updates the name with the given name, if no name was set yet.
	 * 
	 * @param name The new name
	 */
	protected void updateName(String name)
	{
		if (this.name == null || this.name.length() == 0)
		{
			this.name = name;
		}
	}

	/**
	 * Returns the name of the context object. The name can be
	 * used for more detailed resolving if the type resolving is
	 * ambiguous.
	 * 
	 * @return The name of the context object.
	 */
	public String getName()
	{
		return this.name;
	}

	/**
	 * Returns the value of the context object. This value is used
	 * for dependency injection.
	 * 
	 * @return The value of the context object.
	 */
	public abstract Object getValue();

	/**
	 * Intern method to check whether this context object matches
	 * to a required parameter.
	 * 
	 * @return The type of the context object.
	 */
	protected abstract Class<?> getValueType();

	/**
	 * Returns the priority of the context object. Used the determine which
	 * context object will be used when the result is ambiguous. A lesser
	 * number means higher priority.
	 * 
	 * @return The priority of this object.
	 */
	protected abstract int getPriority();

	@Override
	public String toString()
	{
		return "[" + getClass().getSimpleName() + ": name '" + getName() + "', type '"
			+ getValueType().getName() + "', value '" + getValue() + "', scope '" + getScope() + "']";
	}

	@Override
	public boolean equals(Object obj)
	{
		if (obj == null || !(obj instanceof ContextContent))
		{
			return false;
		}

		ContextContent o = (ContextContent) obj;
		if (o.getClass() == getClass() && o.getName().equals(getName())
			&& o.getValueType().equals(getValueType()))
		{
			return true;
		}

		return false;
	}

	/**
	 * All contexts for all threads will use the same instance of this context.
	 */
	static class StaticContent extends ContextContent
	{
		/** Stores the value of the context object. */
		final Object value;

		/** The priority (dynamic created or not) */
		final int priority;

		/**
		 * Creates a new static context object. All contextes for all threads
		 * will use the same instance of this context. Make sure the value of this 
		 * context object is thread safe.
		 * 
		 * @param name The name of the context object.
		 * @param value The value of the context object.
		 */
		StaticContent(String name, Object value, Scope scope)
		{
			this(name, value, scope, ContextContent.PRIORITY_STATIC);
		}

		/**
		 * Creates a new static context object. All contextes for all threads
		 * will use the same instance of this context. Make sure the value of this 
		 * context object is thread safe.
		 * 
		 * @param name The name of the context object.
		 * @param value The value of the context object.
		 * @param priority Use this priority
		 */
		public StaticContent(String name, Object value, Scope scope, int priority)
		{
			super(name, scope, AnnotationUtil.getAnnotation(value.getClass(), Context.class));

			this.value = value;
			this.priority = priority;
		}

		/**
		 * Returns the static value.
		 * 
		 * @return The object which was given to the content.
		 */
		@Override
		public Object getValue()
		{
			return this.value;
		}

		/**
		 * Return the class of the static value.
		 * 
		 * @return The class of the object which was given to the content.
		 */
		@Override
		protected Class<?> getValueType()
		{
			return value != null ? value.getClass() : null;
		}

		@Override
		protected int getPriority()
		{
			return this.priority;
		}
	}

	/**
	 * Extended abstract class for dynamic content like class, methods and fields
	 */
	static abstract class DynamicContent extends ContextContent
	{
		/** If this flag is true, the value will only be requested once for each context */
		boolean enableCache = true;

		/** The scope for the generated value (if cached) */
		Scope instanceScope;

		/**
		 * Creates a new dynamic value.
		 * 
		 * @param name The name of the content
		 * @param scope The scope of this content
		 * @param instanceScope The scope of generated values (if cached)
		 * @param annotation The annotation of the member
		 */
		DynamicContent(String name, Scope scope, Scope instanceScope, Context annotation)
		{
			super(name, scope, annotation);

			this.instanceScope = instanceScope;

			// check annotation
			if (annotation != null)
			{
				this.enableCache = annotation.enableCache();
				if (instanceScope == null)
				{
					this.instanceScope = annotation.instanceScope();
				}
			}
		}

		/**
		 * Checks whether caching is enabled
		 * 
		 * @return Returns "true" when caching is enabled
		 */
		public final boolean getEnableCache()
		{
			return this.enableCache;
		}

		/**
		 * The priority of this content
		 * 
		 * @return The value of the constant {@link ContextContent#PRIORITY_PROTOTYPE}
		 */
		@Override
		protected int getPriority()
		{
			return PRIORITY_PROTOTYPE;
		}

		/**
		 * The scope for new generated values.
		 * 
		 * @return The scope
		 */
		public Scope getInstanceScope()
		{
			return this.instanceScope;
		}

		/**
		 * Generates a new dynamic value of the stored content.
		 * 
		 * @param context Use this context for the dependency injection
		 * @param dependency Already made dependencies
		 * @return The generated value
		 */
		protected abstract Object getDynamicValue(ContextHolder context, Stack<Class<?>> dependency);
	}

	/**
	 * Dynamic context objects normally are a result of a annotation scan. This
	 * objects are part of the static context. The first time a context tries
	 * to get a value from this object, the result will be cached.
	 * So a dynamic context object should be a factory.
	 */
	static class MemberContent extends DynamicContent
	{
		/** The instance which is used during the reflection call */
		private final Object instance;

		/** Call this method/field */
		private final Member member;

		/** The resolved return type */
		private final Class<?> returnType;

		/**
		 * New member content.
		 * 
		 * @param instance The instance which contains the member
		 * @param member The member (method or field)
		 * @param scope The scope for this content.
		 * @param instanceScope The scope of the create objects
		 */
		MemberContent(Object instance, Member member, Scope scope, Scope instanceScope)
		{
			super(null, scope, instanceScope, getAnnotation(member));

			this.instance = instance;
			this.member = member;
			updateName(DIUtil.normalize(member.getName()));

			// check whether the instance matches to the member
			if (!member.getDeclaringClass().isAssignableFrom(instance.getClass()))
			{
				throw new IllegalStateException("The instance " + instance
					+ " doesn't match to the member " + member);
			}

			// TODO handle arrays
			if (member instanceof Method)
			{
				this.returnType = ((Method) member).getReturnType();
			}
			else if (member instanceof Field)
			{
				this.returnType = ((Field) member).getType();
			}
			else
			{
				throw new IllegalStateException("Only Method and Field types are supported: " + member);
			}
		}

		/**
		 * Gets the {@link Context} annotation from the member.
		 * 
		 * @param member Search this member.
		 * @return The found annotation instance or null
		 */
		private static Context getAnnotation(Member member)
		{
			Context annotation = null;
			if (member instanceof Method)
			{
				annotation = AnnotationUtil.getAnnotation((Method) member, Context.class);
			}
			else if (member instanceof Field)
			{
				annotation = AnnotationUtil.getAnnotation((Field) member, Context.class);
			}
			return annotation;
		}

		/**
		 * Gets the value of a field or a method without parameters
		 * 
		 * @return The found value.
		 */
		@Override
		public Object getValue()
		{
			Object value = null;
			try
			{
				// check if method is a simple getter -> return value
				if (this.member instanceof Method)
				{
					Method method = (Method) this.member;
					if (method.getParameterTypes().length == 0)
					{
						value = method.invoke(this.instance, new Object[0]);
					}
				}
				else
				{
					Field field = (Field) this.member;
					value = field.get(this.instance);
				}
			}
			catch (Exception e)
			{
				throw new DependencyException(e);
			}

			return value;
		}

		/**
		 * The return type of the method or field
		 * 
		 * @return The return type
		 */
		@Override
		protected Class<?> getValueType()
		{
			return this.returnType;
		}

		/**
		 * @see ContextContent#compareTo(ContextContent)
		 */
		@Override
		public int compareTo(ContextContent o)
		{
			int result = super.compareTo(o);

			// compare two dynamic context objects
			if (result == 0 && o instanceof MemberContent)
			{
				MemberContent d = (MemberContent) o;
				if ((this.member instanceof Field) && (d.member instanceof Method))
				{
					result = 1;
				}
				else if ((this.member instanceof Method) && (d.member instanceof Field))
				{
					result = -1;
				}
				else if ((this.member instanceof Method) && (d.member instanceof Method))
				{
					result = new MethodComparator(true).compare((Method) this.member, (Method) d.member);
				}
			}
			return result;
		}

		@Override
		public String toString()
		{
			return "[MemberContext: name '" + this.getName() + "', cache '" + this.enableCache
				+ "', method '" + this.instance + " -> " + this.member + "']";
		}

		/**
		 * If the member is a method, this method will be invoked with {@link ContextHolder#invoke(Object, Method)}.
		 * 
		 * @param context Use this context
		 * @param dependency Already made dependencies
		 * @return The result of the di.
		 */
		@Override
		protected Object getDynamicValue(ContextHolder context, Stack<Class<?>> dependencies)
		{
			if (logger.isDebugEnabled())
			{
				logger.debug("Getting new instance from '" + this.member + "'");
			}

			if (this.member instanceof Method)
			{
				return context.invoke(this.instance, (Method) this.member, dependencies);
			}
			return null;
		}
	}

	/**
	 * Stores a class. If the {@link #getDynamicValue(ContextHolder, Stack)} method is called,
	 * a new instance of this class will be generated.
	 */
	static class ClassContent extends DynamicContent
	{
		/** The stored class */
		final Class<?> cls;

		/**
		 * The {@link Context} annotation will be extracted from the given class.
		 * 
		 * @see DynamicContent#DynamicContent(String, Scope, Scope, Context)
		 */
		ClassContent(Class<?> cls, Scope scope, Scope instanceScope)
		{
			super(null, scope, instanceScope, AnnotationUtil.getAnnotation(cls, Context.class));
			if ((cls.getModifiers() & Modifier.PUBLIC) == 0)
			{
				throw new DependencyException("Class '" + cls + "' is not public!");
			}
			if (cls.getConstructors().length == 0)
			{
				throw new DependencyException("Class '" + cls + "' has no public constructors!");
			}

			updateName(DIUtil.normalize(cls.getSimpleName()));
			this.cls = cls;
		}

		/**
		 * No value without an dependency injection possible
		 * 
		 * @return Always returns null
		 */
		@Override
		public Object getValue()
		{
			return null;
		}

		/**
		 * Returns the stored class
		 * 
		 * @return The stored class
		 */
		@Override
		protected Class<?> getValueType()
		{
			return this.cls;
		}

		/**
		 * Generates a new instance of the stored class
		 * 
		 * @param context Use this context for the dependency injection
		 * @param dependency Already made dependencies 
		 * 
		 * @see ContextHolder#newInstance(Class, boolean)
		 */
		@Override
		protected Object getDynamicValue(ContextHolder context, Stack<Class<?>> dependencies)
		{
			if (logger.isDebugEnabled())
			{
				logger.debug("Getting new instance of '" + this.cls + "' from context");
			}

			return context.newInstance(cls, true, dependencies);
		}

		@Override
		public boolean equals(Object o)
		{
			if (o instanceof ClassContent && ((ClassContent) o).cls == this.cls)
			{
				return true;
			}
			return super.equals(o);
		}
	}
}
