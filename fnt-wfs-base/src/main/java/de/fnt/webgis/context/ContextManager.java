package de.fnt.webgis.context;

import org.apache.log4j.Logger;

/**
 * This class manages the {@link ContextHolder} instances for different threads.
 * Also provides methods the populate the global context, which is identical
 * for all other contexts.
 */
public class ContextManager
{
	private final Logger logger = Logger.getLogger(ContextManager.class);

	/** The instance of the context manager */
	private static ContextManager instance = null;

	/** The global context */
	private final ContextHolder applicationContext;

	/** Holds all context instances */
	private final ThreadLocal<ContextHolder> contexts;

	private ContextManager()
	{
		applicationContext = new ContextHolder(Scope.APPLICATION);
		contexts = new ThreadLocal<ContextHolder>();
	}

	public static synchronized ContextManager getInstance()
	{
		if (instance == null)
		{
			instance = new ContextManager();
		}
		return instance;
	}

	/**
	 * Gets the session context. If no session context was created yet, a {@link Scope#REQUEST}
	 * context will be returned.
	 * 
	 * @see ContextManager#getSessionContext(boolean)
	 * @return
	 */
	public ContextHolder getSessionContext()
	{
		return getSessionContext(false);
	}

	/**
	 * Gets the session context. All elements will in this context will be stored
	 * GLOBAL -> SESSION -> REQUEST
	 * 
	 * @param forceCreate 
	 * @return
	 */
	public ContextHolder getSessionContext(boolean forceCreate)
	{
		// get normal context
		ContextHolder context = getContext();
		ContextHolder sessionContext = context.getParentContext(Scope.SESSION);

		if (sessionContext == null)
		{
			if (forceCreate)
			{
				sessionContext = new ContextHolder(Scope.SESSION, applicationContext);
				context.setParentContext(sessionContext);

				if (logger.isDebugEnabled())
				{
					logger.debug("Created new session context");
				}
			}
			else
			{
				sessionContext = context;
				logger.warn("No session context found. Using the current context!");
			}
		}

		return sessionContext;
	}

	/**
	 * Sets the session context. A existing session context cannot be overwritten.
	 * 
	 * @param sessionContext Use this session context.
	 */
	public void setSessionContext(ContextHolder sessionContext)
	{
		if (sessionContext.getScope() != Scope.SESSION)
		{
			throw new IllegalStateException("Only can add context holders with scope '" + Scope.SESSION
				+ "' as session context");
		}

		ContextHolder context = getContext();
		ContextHolder oldContext = context.getParentContext(Scope.SESSION);

		if (oldContext != null)
		{
			if (oldContext != sessionContext)
			{
				throw new IllegalStateException("Session context '" + oldContext + "' was already added!");
			}
		}
		else
		{
			context.setParentContext(sessionContext);
			sessionContext.setParentContext(applicationContext);
			if (logger.isDebugEnabled())
			{
				logger.debug("Embedded existing session context '" + sessionContext + "'");
			}
		}
	}

	/**
	 * Gets the current application context.
	 * 
	 * @return The application context.
	 */
	public ContextHolder getApplicationContext()
	{
		return applicationContext;
	}

	/**
	 * Gets the current context. If no current context exists, a new context will be created.
	 * All objects in the {@linkplain Scope#REQUEST} will be added to this context holder.
	 * 
	 * @return The current context of null.
	 */
	public ContextHolder getContext()
	{
		ContextHolder context = contexts.get();

		if (context == null)
		{
			context = new ContextHolder(null, applicationContext);
			contexts.set(context);
			logger.info("Created new context");
		}

		return context;
	}

	/**
	 * Destroys the current context. Should be called when the context is not longer needed.
	 */
	public void destroyContext()
	{
		contexts.remove();
	}

	/**
	 * Shortcut to get an instance from the current user context
	 * 
	 * @param cls
	 * @return
	 */
	public static <T> T get(Class<T> cls)
	{
		return getInstance().getContext().get(cls);
	}
}
