package de.fnt.webgis.context;

public enum Scope
{
	/** The element is hold in the global context */
	APPLICATION(3),
	/** The element will be stored with the session */
	SESSION(2),
	/** The element is only valid with the current context */
	REQUEST(1);

	public final int level;

	Scope(int level)
	{
		this.level = level;
	}
};
