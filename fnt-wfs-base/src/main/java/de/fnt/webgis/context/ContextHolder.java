package de.fnt.webgis.context;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import org.apache.log4j.Logger;

import de.fnt.webgis.annotation.AnnotationUtil;
import de.fnt.webgis.annotation.Context;
import de.fnt.webgis.context.ContextContent.DynamicContent;
import de.fnt.webgis.context.ContextContent.MemberContent;
import de.fnt.webgis.di.DIUtil;
import de.fnt.webgis.di.DependencyException;
import de.fnt.webgis.di.MethodComparator;

/**
 * The ContextHolder is the worker class. In manages the instances of the {@linkplain ContextContent} objects.
 * You can create sub context of this context with the method {@link ContextHolder#createSubContext()}.
 * It is also capable of creating new instances of given classes or automatically invokes the best method
 * of the given methods.
 */
public class ContextHolder
{
	private static final Logger logger = Logger.getLogger(ContextHolder.class);

	/** Holds all dynamically generated and static context objects */
	private final List<ContextContent> contents = new ArrayList<ContextContent>();

	/** For which scope is this context used */
	private final Scope scope;

	/** The parent context of this context */
	private ContextHolder parentContext;

	/**
	 * Use {@link ContextManager#createContext()} to create a new ContextHolder. Only
	 * one {@link ContextHolder} per Thread is allowed.
	 */
	protected ContextHolder(Scope scope)
	{
		this(scope, null);
	}

	/**
	 * Use {@link ContextHolder#createSubContext()}
	 * 
	 * @param parentContext
	 */
	protected ContextHolder(Scope scope, ContextHolder parentContext)
	{
		this.scope = scope == null ? Scope.REQUEST : scope;
		this.parentContext = parentContext;
	}

	/**
	 * Adds a object to the context instance of this thread.
	 * As name the class name will be used.
	 * 
	 * @param object Add this object.
	 */
	public boolean registerInstance(Object object)
	{
		String name = null;
		if (object != null)
		{
			name = DIUtil.normalize(object.getClass().getSimpleName());
		}

		boolean added = registerInstance(name, object);

		return added;
	}

	/**
	 * Adds this object to the context. This object an all its methods and 
	 * fields with the annotation {@link ContextContent} will be available for
	 * dependency injection.
	 * 
	 * @param name The name of the object in the context.
	 * @param object Add this object.
	 */
	public boolean registerInstance(String name, Object object)
	{
		return registerInstance(name, object, null);
	}

	/**
	 * Adds this object to the context. This object an all its methods and 
	 * fields with the annotation {@link ContextContent} will be available for
	 * dependency injection.
	 * 
	 * @param name The name of the object in the context.
	 * @param object Add this object.
	 */
	public boolean registerInstance(String name, Object object, Scope scope)
	{
		boolean added = false;
		if (object != null)
		{
			ContextContent content = new ContextContent.StaticContent(name, object, scope);
			removeContent(content);
			// TODO parse as factory?

			added = addContent(content);
		}
		return added;
	}

	/**
	 * Add the given class to the context.
	 * 
	 * @param cls Add this class
	 * @return Returns "true", if the class was successfully added
	 */
	public boolean registerClass(Class<?> cls)
	{
		return registerClass(cls, null, null);
	}

	/**
	 * Add the given class to the context.
	 * 
	 * @param cls Add this class
	 * @param scope Use this scope for the class content
	 * @param instanceScope Use this scope for new generted instances (if caching is enabled)
	 * @return Returns "true", if the class was successfully added
	 */
	public boolean registerClass(Class<?> cls, Scope scope, Scope instanceScope)
	{
		boolean added = false;
		if (cls != null)
		{
			ContextContent content = new ContextContent.ClassContent(cls, scope, instanceScope);
			removeContent(content);

			added = addContent(content);
		}
		return added;
	}

	/**
	 * Adds a object to the context instance of this thread
	 * 
	 * @param object
	 */
	public boolean unregisterInstance(Object object)
	{
		boolean removed = false;
		ContextContent instance = getInstance(object);
		if (instance != null)
		{
			removed = removeContent(instance);
		}
		else if (logger.isDebugEnabled())
		{
			logger.debug("Following object was not found in the current context: " + object);
		}

		return removed;
	}

	/**
	 * Helper method. Adds the given content to this context. If the
	 * scope of the content is higher, it will be passed to the parent
	 * context.
	 *  
	 * @param content Add this context.
	 * @return Returns "true" when the context was added.
	 * 
	 * @see Scope#level
	 */
	private boolean addContent(ContextContent content)
	{
		boolean added;
		synchronized (this.contents)
		{
			added = this.contents.add(content);
		}

		if (added && logger.isDebugEnabled())
		{
			logger.debug("Registered '" + content + "' to context '" + this + "'");
		}

		// register to target context, too
		Scope targetScope = content.getScope();
		if (targetScope != null && targetScope.level > this.scope.level)
		{
			ContextHolder targetContext = getParentContext(targetScope);
			if (targetContext != null)
			{
				added = targetContext.addContent(content) || added;
			}
		}

		return added;
	}

	/**
	 * Helper method. Removed the given content from the context.
	 * Also checks the parent contexts.
	 * 
	 * @param content Remove this context
	 * @return Returns "true" when the content was removed
	 */
	private boolean removeContent(ContextContent content)
	{
		boolean removed;
		synchronized (this.contents)
		{
			removed = this.contents.remove(content);
		}

		if (removed && logger.isDebugEnabled())
		{
			logger.debug("Removed content '" + content + "' from context '" + this + "'!");
		}

		// register to target context, too
		Scope targetScope = content.getScope();
		if (targetScope != this.scope && targetScope != null && targetScope != Scope.REQUEST)
		{
			ContextHolder targetContext = getParentContext(targetScope);
			if (targetContext != null)
			{
				removed = targetContext.removeContent(content) || removed;
			}
		}

		return removed;
	}

	/**
	 * Sets a new parent context for this context.
	 * 
	 * @param parentContext The new parent context
	 */
	protected void setParentContext(ContextHolder parentContext)
	{
		this.parentContext = parentContext;
	}

	/**
	 * Gets the current parent context of this context.
	 * 
	 * @return The parent context
	 */
	protected ContextHolder getParentContext()
	{
		return this.parentContext;
	}

	/**
	 * Searches recursive all parent contexts until no further parent
	 * context is found or a parent context matches the required scope.
	 * 
	 * @param scope Search for this scope
	 * @return The found context or null
	 */
	protected ContextHolder getParentContext(Scope scope)
	{
		ContextHolder context = this.parentContext;
		while (context != null && context.scope != scope)
		{
			context = context.parentContext;
		}

		if (context == null && logger.isInfoEnabled())
		{
			logger.info("No parent context with scope '" + scope + "' for context '" + this + "' found!");
		}

		return context;
	}

	/**
	 * The scope of this context.
	 * 
	 * @return The scope
	 */
	public Scope getScope()
	{
		return this.scope;
	}

	/**
	 * Gets the {@link DynamicContent} for the given object.
	 * 
	 * @param object Search for this object
	 * @return The instance or null
	 */
	protected ContextContent getInstance(Object object)
	{
		for (ContextContent instance : this.contents)
		{
			if (instance.getValue().equals(object))
			{
				return instance;
			}
		}
		if (logger.isDebugEnabled())
		{
			logger.debug("Object was not found in current context: " + object);
		}
		return null;
	}

	/**
	 * Searches an object in the context. Caching is enabled by default
	 * 
	 * @param cls The object must be assignable by this class.
	 * @return The best matching object or null.
	 */
	public <T> T get(Class<T> cls)
	{
		return get(cls, false);
	}

	/**
	 * Searches an object in the context.
	 * 
	 * @param cls The object must be assignable by this class
	 * @param disableCaching If a dynamic content was used, the result will be cached
	 * @return The best matching object or null
	 */
	public <T> T get(Class<T> cls, boolean disableCaching)
	{
		return get(cls, disableCaching, new Stack<Class<?>>());
	}

	/**
	 * Helper method. Searches an object in the context and checks for dependencies
	 * 
	 * @param cls The object must be assignable by this class.
	 * @param disableCaching If a dynamic content was used, the result will be cached
	 * @param dependencies Check these dependencies
	 * @return The best matching object or null
	 */
	@SuppressWarnings("unchecked")
	protected <T> T get(Class<T> cls, boolean disableCaching, Stack<Class<?>> dependencies)
	{
		// add own class to dependency stack
		dependencies.add(cls);

		List<ContextContent> objects = searchContents(cls, true);
		T value = null;
		for (int i = 0; value == null && i < objects.size(); i++)
		{
			ContextContent cobj = objects.get(i);
			value = (T) cobj.getValue();

			if (cobj instanceof DynamicContent)
			{
				DynamicContent proto = (DynamicContent) cobj;

				if (value == null)
				{
					value = (T) proto.getDynamicValue(this, dependencies);
				}

				// cache dynamic instances
				if (value != null && !disableCaching && proto.getEnableCache())
				{
					registerInstance(proto.getName(), value, proto.getInstanceScope());
				}
			}
		}

		// remove from stack
		dependencies.pop();

		if (logger.isDebugEnabled())
		{
			logger.debug("Found object for class '" + cls + "': " + value);
		}

		return value;
	}

	/**
	 * Checks whether this class is contained in this context.
	 * 
	 * @param cls Check this class.
	 * @return Returns true if this class is in the context.
	 */
	public boolean contains(Class<?> cls)
	{
		return searchContents(cls, true).size() > 0;
	}

	/**
	 * Helper method. Searches all contents which are assignable from the given
	 * class.
	 * 
	 * @param cls Search this class
	 * @param deep If "true", the parent contexts will be checked, too
	 * @return All found contexts.
	 */
	private List<ContextContent> searchContents(Class<?> cls, boolean deep)
	{
		List<ContextContent> objects = new ArrayList<ContextContent>();

		for (ContextContent object : this.contents)
		{
			if (object.matches(cls))
			{
				objects.add(object);
			}
		}

		// sort list
		Collections.sort(objects);

		// add parent objects
		if (deep && this.parentContext != null)
		{
			objects.addAll(parentContext.searchContents(cls, deep));
		}

		return objects;
	}

	/**
	 * Creates a new context. This context will be the parent context
	 * of the new context.
	 * 
	 * @return The new sub context.
	 */
	public ContextHolder createSubContext()
	{
		ContextHolder subContext = new ContextHolder(null, this);
		return subContext;
	}

	/**
	 * Searches for {@link Context} annotations in the given object.
	 * Adds all found members as {@link DynamicContent} to the list.
	 * e.g.
	 * <pre>
	 * class MyFactory {
	 *   &#64;Context
	 *   Object myMethod() {
	 *   	return new Object();
	 *   }
	 * }
	 * </pre>
	 * 
	 * @param object Parse this factory.
	 */
	public void registerFactory(Object factoryInstance)
	{
		if (logger.isDebugEnabled())
		{
			logger.debug("Searching dynamic context objects: " + factoryInstance);
		}

		Method[] methods = AnnotationUtil.getMethods(factoryInstance.getClass(), Context.class);
		for (Method method : methods)
		{
			new MemberContent(factoryInstance, method, null, null);
		}
		// TODO same for fields
	}

	/**
	 * Tries to fill the value array with parameters. Do not generate
	 * instances of classes with are present in the dependencies stack (loop).
	 * 
	 * @param types Get instances of this types.
	 * @param values Fill this array
	 * @param dependencies Classes in the pipe.
	 * @return Returns true if all values could be filled.
	 */
	protected boolean get(Class<?>[] types, Object[] values, Stack<Class<?>> dependencies)
	{
		boolean foundAll = true;
		for (int i = 0; i < types.length; i++)
		{
			Object value = get(types[i], false, dependencies);
			if (value == null)
			{
				foundAll = false;
			}
			values[i] = value;
		}
		return foundAll;
	}

	/**
	 * Searches in the given package for classes with the {@link Context} annotation.
	 * 
	 * @param packageName Search this package
	 * 
	 * @see AnnotationUtil#getClasses(String, Class)
	 */
	public void registerPackage(String packageName)
	{
		registerPackage(packageName, null);
	}

	/**
	 * Searches in the given package for classes with the {@link Context} annotation.
	 * 
	 * @param packageName Search this package
	 * @param scope Use this scope
	 * 
	 * @see AnnotationUtil#getClasses(String, Class)
	 */
	public void registerPackage(String packageName, Scope scope)
	{
		List<Class<?>> classes = AnnotationUtil.getClasses(packageName, Context.class);
		for (Class<?> cls : classes)
		{
			registerClass(cls, scope, scope);
		}
	}

	/**
	 * Executes the given method. Automatically resolves the
	 * parameter values using the stored contents.
	 * 
	 * @param instance Use this instance to execute the method
	 * @param method Execute this method
	 * @return The result of the method
	 * 
	 * @see Method#invoke(Object, Object...)
	 */
	public Object invoke(Object instance, Method method)
	{
		return invoke(instance, method, new Stack<Class<?>>());
	}

	/**
	 * Executes the given method. Automatically resolves the
	 * parameter values using the stored contents.
	 * 
	 * @param instance Use this instance to execute the method
	 * @param method Execute this method
	 * @param dependencies Check these dependencies when resolving the parameter values
	 * @return The result of the method
	 * 
	 * @see Method#invoke(Object, Object...)
	 */
	protected Object invoke(Object instance, Method method, Stack<Class<?>> dependencies)
	{
		// TODO also offer a method which doesn't need a instance object.  Instead this object can be fetched from the context.
		Class<?>[] parameterTypes = method.getParameterTypes();

		Object returnVal = null;

		if (checkDependency(dependencies, parameterTypes))
		{
			Object[] parameters = new Object[parameterTypes.length];
			get(parameterTypes, parameters, dependencies);
			try
			{
				returnVal = method.invoke(instance, parameters);
			}
			catch (Exception e)
			{
				throw new DependencyException(e);
			}
		}
		return returnVal;
	}

	/**
	 * @see #invoke(Collection, Collection, Stack)
	 */
	public Object invoke(Collection<Object> instances, Collection<Method> methods)
	{
		return invoke(instances, methods, new Stack<Class<?>>());
	}

	/**
	 * Invokes the first method which can be fully satisfied with
	 * the stored parameters.
	 * 
	 * @param instances Use one of these instances to execute the method.
	 * @param methods Execute one of these methods
	 * @param dependencies Check these dependencies
	 * @return The result of the invoked method
	 * 
	 * @see MethodComparator
	 * @see #invoke(Object, Method, Stack)
	 */
	protected Object invoke(Collection<Object> instances, Collection<Method> methods,
		Stack<Class<?>> dependencies)
	{
		Object returnVal = null;

		if (methods != null && !methods.isEmpty())
		{
			// sort by parameter count
			Collections.sort(methods instanceof List ? (List<Method>) methods : new ArrayList<Method>(
				methods), new MethodComparator(this));

			Object[] parametersToUse = null;
			Method methodToUse = null;
			Object instanceToUse = null;

			// check all methods
			for (Method method : methods)
			{
				Class<?>[] parameterTypes = method.getParameterTypes();

				if (checkDependency(dependencies, parameterTypes))
				{
					Object[] parameters = new Object[parameterTypes.length];
					if (get(parameterTypes, parameters, dependencies))
					{
						methodToUse = method;
						parametersToUse = parameters;
						instanceToUse = getMatchingInstance(instances, method.getDeclaringClass());
						if (instanceToUse != null)
						{
							break;
						}
						logger
							.warn("No suitable instance of class '" + method.getDeclaringClass() + "' found!");
					}
					// if no method could be satisfy, use the first
					else if (instanceToUse == null)
					{
						methodToUse = method;
						parametersToUse = parameters;
						instanceToUse = getMatchingInstance(instances, method.getDeclaringClass());
					}
				}
			}

			if (instanceToUse != null)
			{
				try
				{
					returnVal = methodToUse.invoke(instanceToUse, parametersToUse);
					if (logger.isDebugEnabled())
					{
						logger.debug("Invoked method '" + methodToUse + "' with parameters '"
							+ Arrays.toString(parametersToUse) + "' and got result '" + returnVal + "'");
					}
				}
				catch (InvocationTargetException e)
				{
					throw new DependencyException(e.getTargetException());
				}
				catch (Exception e)
				{
					throw new DependencyException(e);
				}
			}
		}
		else
		{
			logger.warn("No methods given!");
			returnVal = null;
		}

		return returnVal;
	}

	/**
	 * Picks out the instance which is most suitable for the given class.
	 * 
	 * @param instances Use this instances.
	 * @param cls Search this class.
	 * @return The best instance or null.
	 */
	private Object getMatchingInstance(Collection<Object> instances, Class<?> cls)
	{
		Object instanceToUse = null;
		for (Object instance : instances)
		{
			if (cls.isInstance(instance)
				&& (instanceToUse == null || instanceToUse.getClass().isAssignableFrom(instance.getClass())))
			{
				instanceToUse = instance;
			}
		}
		if (logger.isDebugEnabled())
		{
			logger.debug("Returning instance '" + instanceToUse + "' for class '" + cls + "'");
		}
		return instanceToUse;
	}

	/**
	 * Checks whether one of the given glasses is already in the dependency list. In
	 * this case the injection is not possible.
	 * 
	 * @param dependency Existing dependencies
	 * @param parameterTypes Check this classes
	 * @return Returns true if no dependency was found.
	 */
	protected boolean checkDependency(Stack<Class<?>> dependencies, Class<?>[] classes)
	{
		for (Class<?> cls : classes)
		{
			if (dependencies.contains(cls))
			{
				return false;
			}
		}
		return true;
	}

	/**
	 * @see ContextHolder#newInstance(Class, boolean, Stack)
	 */
	public Object newInstance(Class<?> cls, boolean callSetters)
	{
		return newInstance(cls, callSetters, new Stack<Class<?>>());
	}

	/**
	 * Generates a new instance of the given class. Uses the best matching constructor.
	 * 
	 * @param cls Create a new instance of this class.
	 * @param callSetters If "true", all setters will be called using the contents of this context.
	 * @param dependencies Check these dependencies.
	 * @return The new istance
	 */
	protected Object newInstance(Class<?> cls, boolean callSetters, Stack<Class<?>> dependencies)
	{
		Constructor<?>[] ctors = cls.getConstructors();

		// sort by parameter count
		Arrays.sort(ctors, new Comparator<Constructor<?>>()
		{
			public int compare(Constructor<?> c1, Constructor<?> c2)
			{
				return c1.getParameterTypes().length - c2.getParameterTypes().length;
			}
		});

		Constructor<?> constructorToUse = null;
		Object[] parametersToUse = null;
		// try to invoke
		for (Constructor<?> ctor : ctors)
		{
			if (!checkDependency(dependencies, ctor.getParameterTypes()))
			{
				continue;
			}

			Class<?>[] types = ctor.getParameterTypes();
			Object[] values = new Object[types.length];
			if (types.length == 0 || get(types, values, dependencies))
			{
				constructorToUse = ctor;
				parametersToUse = values;
				break;
			}
			else if (constructorToUse == null)
			{
				constructorToUse = ctor;
				parametersToUse = values;
			}
		}

		if (constructorToUse == null)
		{
			throw new DependencyException("No suitable constructor could be found for class '"
				+ cls.getName() + "'");
		}

		try
		{
			if (logger.isDebugEnabled())
			{
				logger.debug("Creating new instance. Using constructor '" + constructorToUse + "'");
			}
			Object instance = constructorToUse.newInstance(parametersToUse);
			if (callSetters)
			{
				Map<String, Method> setters = DIUtil.getSetters(instance.getClass());
				for (Method setter : setters.values())
				{
					Class<?> parameterType = setter.getParameterTypes()[0];
					boolean found = false;
					for (Object parameter : parametersToUse)
					{
						if (parameter.getClass().isAssignableFrom(parameterType))
						{
							if (logger.isDebugEnabled())
							{
								logger.debug("Value for setter '" + setter + "' already set via constructor: "
									+ parameter);
							}
							found = true;
							break;
						}
					}

					if (found)
					{
						continue;
					}

					Object value = get(parameterType);
					if (value != null)
					{
						setter.invoke(instance, new Object[] { value });
						if (logger.isDebugEnabled())
						{
							logger.debug("Called setter '" + setter + "' with value '" + value + "'");
						}
					}
				}
			}

			if (logger.isDebugEnabled())
			{
				logger.debug("Created instance '" + instance + "'");
			}
			return instance;
		}
		catch (Exception e)
		{
			throw new DependencyException(e);
		}
	}

	@Override
	public String toString()
	{
		return "[ContextHolder: scope '" + getScope() + "']";
	}
}
