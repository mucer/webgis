package de.fnt.webgis.di.feature;

import org.apache.log4j.Logger;
import org.geotools.feature.simple.SimpleFeatureImpl;
import org.opengis.feature.type.AttributeDescriptor;
import org.opengis.feature.type.Name;
import org.opengis.filter.identity.FeatureId;

import de.fnt.webgis.context.ContextHolder;
import de.fnt.webgis.context.ContextManager;

/**
 * "dynamic" version of the {@link SimpleFeatureImpl}. Is able to
 * create a new instance of the class, which is stored in the linked
 * {@link DynamicFeatureType} using the attributes.
 */
public class DynamicFeatureImpl extends SimpleFeatureImpl implements DynamicFeature
{
	private static final Logger logger = Logger.getLogger(DynamicFeatureImpl.class);

	public DynamicFeatureImpl(Object[] values, DynamicFeatureType type, FeatureId id, boolean validate)
	{
		super(values, type, id, validate);
	}

	/**
	 * @see DynamicFeatureImpl#newFeatureClassInstance(ContextHolder)
	 */
	public Object newFeatureClassInstance()
	{
		return newFeatureClassInstance(null);
	}

	/**
	 * Creates a new instance of the class, which is stored in the linked {@link DynamicFeatureType}
	 * 
	 * @param context Also use the contents of this context, when calling a constructor. Can be null.
	 * @return The new instance.
	 * 
	 * @see ContextHolder#newInstance(Class, boolean)
	 */
	public Object newFeatureClassInstance(ContextHolder context)
	{
		if (logger.isDebugEnabled())
		{
			logger.debug("Creating feature class instance of " + this);
		}

		if (context == null)
		{
			context = ContextManager.getInstance().getContext();
		}
		
		DynamicFeatureType type = (DynamicFeatureType) getFeatureType();

		ContextHolder subContext = context.createSubContext();
		// register attributes
		for (AttributeDescriptor ad : type.getAttributeDescriptors())
		{
			Name name = ad.getName();
			Object value = getAttribute(name);
			if (value == null)
			{
				continue;
			}

			// another dynamic feature found? -> create first
			if (value instanceof DynamicFeature)
			{
				DynamicFeature subFeature = (DynamicFeature) value;

				if (logger.isDebugEnabled())
				{
					logger.debug("Found sub dynamic feature: " + subFeature);
				}

				value = subFeature.newFeatureClassInstance();
			}

			subContext.registerInstance(name.getLocalPart(), value);
		}

		Class<?> featureClass = type.getFeatureClass();

		Object instance = subContext.newInstance(featureClass, true);

		if (logger.isDebugEnabled())
		{
			logger.debug("Created instance '" + instance + "'");
		}
		return instance;
	}

	@Override
	public String toString()
	{
		StringBuffer sb = new StringBuffer(getClass().getSimpleName());
		sb.append(" ");
		sb.append(getType().getName().getLocalPart());
		sb.append("=");
		sb.append(getValue());
		return sb.toString();
	}
}
