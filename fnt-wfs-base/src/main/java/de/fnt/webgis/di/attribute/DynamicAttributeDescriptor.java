package de.fnt.webgis.di.attribute;

import java.lang.reflect.AccessibleObject;

import org.opengis.feature.type.AttributeDescriptor;

/**
 * Extended attribute descriptor, which allows to return a method which is linked
 * to this attribute
 */
public interface DynamicAttributeDescriptor extends AttributeDescriptor
{
	/**
	 * If a new instance of the attribute is created, this method will be used
	 * to fill the value.
	 * 
	 * @return The method which is linked to this attribute.
	 */
	public AccessibleObject getMember();
}
