package de.fnt.webgis.di.layer;

import static de.fnt.webgis.di.layer.DynamicFeatureLayerAction.SearchType.BOUNDING_BOX;
import static de.fnt.webgis.di.layer.DynamicFeatureLayerAction.SearchType.GEOMETRY;
import static de.fnt.webgis.di.layer.DynamicFeatureLayerAction.SearchType.MULTI_ID;
import static de.fnt.webgis.di.layer.DynamicFeatureLayerAction.SearchType.SINGLE_ID;
import static de.fnt.webgis.di.layer.DynamicFeatureLayerAction.SearchType.UNKNOWN;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.geotools.filter.FidFilter;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.filter.Filter;
import org.opengis.filter.spatial.BBOX;
import org.opengis.geometry.BoundingBox;

import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;

import de.fnt.webgis.annotation.LayerAction;
import de.fnt.webgis.annotation.LayerAction.ActionType;
import de.fnt.webgis.context.ContextHolder;
import de.fnt.webgis.context.ContextManager;
import de.fnt.webgis.di.DIUtil;
import de.fnt.webgis.di.DependencyException;
import de.fnt.webgis.di.attribute.DynamicAttributeDescriptor;
import de.fnt.webgis.di.feature.DynamicFeature;
import de.fnt.webgis.di.feature.DynamicFeatureBuilder;
import de.fnt.webgis.di.feature.DynamicFeatureType;
import de.fnt.webgis.di.layer.DynamicFeatureLayerAction.Search;
import de.fnt.webgis.di.layer.DynamicFeatureLayerAction.SearchType;
import de.fnt.webgis.di.layer.DynamicFeatureLayerAction.Update;
import de.fnt.webgis.di.layer.DynamicFeatureLayerAction.UpdateType;
import de.fnt.webgis.layer.FeatureLayer;

/**
 * Represents a namespace in the webgis. Contains {@link DynamicFeatureType}s
 */
public class DynamicFeatureLayer extends FeatureLayer
{
	/** Holds an instance of the class, which is wrapped by this layer */
	final List<Object> layerInstances = new ArrayList<Object>();

	private final List<DynamicFeatureLayerAction> actions = new ArrayList<DynamicFeatureLayerAction>();

	protected DynamicFeatureLayer(String namespaceURI)
	{
		super(namespaceURI);
	}

	protected boolean addFeatureType(DynamicFeatureType featureType)
	{
		return super.addFeatureType(featureType);
	}

	public Object[] searchObjects(DynamicFeatureType type, Filter filter)
	{

		// get methods
		Class<?> featureClass = type.getFeatureClass();
		List<DynamicFeatureLayerAction> actions = getLayerActions(ActionType.SEARCH, featureClass, true);
		if (actions.size() == 0)
		{
			logger.warn("No suitable search methods for type '" + type.getName()
				+ "' were found. Using all search methods!");
			actions = getLayerActions(ActionType.SEARCH, featureClass, false);
		}

		if (actions.size() == 0)
		{
			throw new DependencyException("No search methods for type '" + type + "' found!");
		}

		// build context
		ContextHolder context = ContextManager.getInstance().getContext().createSubContext();
		String[] ids = null;
		BoundingBox boundingBox = null;
		Geometry geometry = null;
		if (filter != null)
		{
			context.registerInstance("filter", filter);
			if (filter instanceof BBOX)
			{
				BBOX bbox = (BBOX) filter;
				boundingBox = bbox.getBounds();
				context.registerInstance("bounds", boundingBox);

				// also add geometry object
				if (boundingBox instanceof Envelope)
				{
					GeometryFactory f = new GeometryFactory();
					Geometry g = f.toGeometry((Envelope) boundingBox);
					context.registerInstance("geometry", g);
				}
			}
			else if (filter instanceof FidFilter)
			{
				FidFilter fid = (FidFilter) filter;
				Set<Object> set = fid.getIDs();
				ids = new String[set.size()];
				int i = 0;
				for (Object id : set)
				{
					ids[i++] = id.toString();
				}
				context.registerInstance("ids", ids);
			}
		}

		List<Method> methods = new ArrayList<Method>();
		SearchType[] searchTypes = SearchType.valuesSorted();
		SearchType st = null;

		for (int i = 0; i < searchTypes.length; i++)
		{
			st = searchTypes[i];
			for (DynamicFeatureLayerAction action : actions)
			{
				Search s = (Search) action;
				// no current search type --> add all without restrictions
				// else --> searchType must match and maybe some special restrictions
				if (st == UNKNOWN //
					|| (s.getSearchType() == st //
					&& ((st == BOUNDING_BOX && boundingBox != null) // 
						|| (st == GEOMETRY && geometry != null) //
						|| (st == MULTI_ID && ids != null)//
					|| (st == SINGLE_ID && ids != null))))
				{
					methods.add(s.getMethod());
				}
			}

			if (methods.size() > 0)
			{
				break;
			}
		}

		Object[] objects;
		if (st == SINGLE_ID)
		{
			List<Object> list = new ArrayList<Object>();
			for (int i = 0; i < ids.length; i++)
			{
				context.registerInstance("id", ids[i]);
				Object returnVal = context.invoke(this.layerInstances, methods);
				if (returnVal != null)
				{
					list.add(returnVal);
				}
			}

			objects = new Object[list.size()];
			list.toArray(objects);
		}
		else
		{
			Object returnVal = context.invoke(this.layerInstances, methods);

			// convert return value to object array
			if (returnVal == null)
			{
				objects = new Object[0];
			}
			else if (returnVal.getClass().isArray())
			{
				objects = (Object[]) returnVal;
			}
			else
			{
				objects = new Object[] { returnVal };
			}
		}

		return objects;
	}

	@Override
	public List<SimpleFeature> searchFeatures(String typeName, Filter filter)
	{
		DynamicFeatureType type = getFeatureType(typeName);

		Object[] objects = searchObjects(type, filter);

		// map objects to features
		List<SimpleFeature> features = new ArrayList<SimpleFeature>();
		DynamicFeatureBuilder builder = new DynamicFeatureBuilder(type);
		// TODO use command unique id 
		for (Object obj : objects)
		{
			// already a simple feature -> nothing to do
			if (SimpleFeature.class.isAssignableFrom(obj.getClass()))
			{
				features.add((SimpleFeature) obj);
			}
			// map with AnnotationFeatureType
			else if (type instanceof DynamicFeatureType)
			{
				features.add(builder.parseObject(obj));
			}
			else
			{
				throw new IllegalStateException("Could not convert '" + obj.getClass().getCanonicalName()
					+ " to SimpleFeature!");
			}
		}

		return features;
	}

	@Override
	public DynamicFeatureType getFeatureType(String typeName)
	{
		return (DynamicFeatureType) super.getFeatureType(typeName);
	}

	public List<DynamicFeatureLayerAction> getLayerActions(LayerAction.ActionType type,
		Class<?> featureClass)
	{
		return getLayerActions(type, featureClass, true);
	}

	public List<DynamicFeatureLayerAction> getLayerActions(LayerAction.ActionType type,
		Class<?> featureClass, boolean extend)
	{
		List<DynamicFeatureLayerAction> actions = new ArrayList<DynamicFeatureLayerAction>();

		for (DynamicFeatureLayerAction action : this.actions)
		{
			if ((type == null || action.getActionType() == type)
				&& (featureClass == null || (extend ? featureClass.isAssignableFrom(action
					.getFeatureClass()) : action.getFeatureClass().isAssignableFrom(featureClass))))
			{
				actions.add(action);
			}
		}

		return actions;
	}

	/**
	 * 
	 * @param actionType The type of the action.
	 * @param featureClass The action must support this feature type.
	 * @param extend If this parameter is "true", all feature types with extend the given feature type will be accepted. Otherwise
	 *               the feature types which were extended by the given class will be accepted.
	 * @return
	 */
	protected List<Method> getMethods(LayerAction.ActionType type, Class<?> featureClass,
		boolean extend)
	{
		List<DynamicFeatureLayerAction> actions = getLayerActions(type, featureClass, extend);
		List<Method> methods = new ArrayList<Method>();
		for (DynamicFeatureLayerAction action : actions)
		{
			methods.add(action.getMethod());
		}

		return methods;
	}

	@Override
	public String[] insertFeatures(SimpleFeature... features)
	{
		//		// compare featre types
		//		SimpleFeatureType given = feature.getType();
		//		SimpleFeatureType found = entry.getValue();
		//		if (given != found)
		//		{
		//			if (given.getAttributeCount() != found.getAttributeCount())
		//			{
		//				throw new IllegalStateException("Attribute count between " + given + " and " + found
		//					+ " does not match");
		//			}
		//			for (int i = 0; i < given.getAttributeCount(); i++)
		//			{
		//				if (!given.getAttributeDescriptors().get(i).getType().getBinding().isAssignableFrom(
		//					found.getAttributeDescriptors().get(i).getType().getBinding()))
		//				{
		//					throw new IllegalStateException("The attribute descriptor "
		//						+ given.getAttributeDescriptors().get(i) + " doesn't match to "
		//						+ found.getAttributeDescriptors().get(i));
		//				}
		//			}
		//		}

		String[] ids = new String[features.length];

		ContextHolder context = ContextManager.getInstance().getContext();
		for (int i = 0; i < features.length; i++)
		{
			SimpleFeature feature = features[i];
			if (!(feature instanceof DynamicFeature))
			{
				DynamicFeatureType type = getFeatureType(feature.getName().getLocalPart());
				if (type == null)
				{
					throw new IllegalArgumentException("No feature type for feature '" + feature + "' found!");
				}

				DynamicFeatureBuilder builder = new DynamicFeatureBuilder(type);
				feature = builder.parseFeature(feature);
			}

			Object object = ((DynamicFeature) feature).newFeatureClassInstance();

			ContextHolder subContext = context.createSubContext();
			subContext.registerInstance("feature", object);

			List<Method> methods = getMethods(ActionType.INSERT, object.getClass(), false);

			if (methods.size() == 0)
			{
				throw new DependencyException("No method found for inserting '" + object + "'!");
			}

			Object returnVal = subContext.invoke(this.layerInstances, methods);

			ids[i] = returnVal != null ? returnVal.toString() : null;
		}

		return ids;
	}

	@Override
	public int updateFeatures(String typeName, Filter filter, Map<String, Object> attributes)
	{
		int updated = 0;
		DynamicFeatureType featureType = getFeatureType(typeName);
		Class<?> featureClass = featureType.getFeatureClass();
		List<DynamicFeatureLayerAction> actions = getLayerActions(ActionType.UPDATE, featureClass,
			false);

		List<Method> methods = new ArrayList<Method>();

		boolean useMultiFeatureMethod = false;
		boolean useMultiFeature = false;
		boolean useSingleFeature = false;
		boolean hasAttributeMap;

		do
		{
			hasAttributeMap = false;

			// check for filter method
			for (DynamicFeatureLayerAction action : actions)
			{
				if (action.getFeatureClass() == null && !useMultiFeatureMethod)
				{
					continue;
				}

				Update u = (Update) action;
				if ((useSingleFeature && u.getUpdateType() == UpdateType.SINGLE_FEATURE)
					|| (useMultiFeature && u.getUpdateType() == UpdateType.MULTI_FEATURE)
					|| u.getUpdateType() == UpdateType.FILTER)
				{
					if ((!hasAttributeMap && u.hasAttributeMap() || (hasAttributeMap && !u.hasAttributeMap())))
					{
						hasAttributeMap = true;
						if (!methods.isEmpty())
						{
							logger.warn("Update methods with and without a attribute map found!");
						}
					}
					methods.add(u.getMethod());
				}
			}

			if (methods.size() > 0)
			{
				break;
			}
			else if (!useMultiFeature)
			{
				useMultiFeature = true;
			}
			else if (!useSingleFeature)
			{
				useSingleFeature = true;
			}
			else if (!useMultiFeatureMethod)
			{
				useMultiFeature = false;
				useSingleFeature = false;
				useMultiFeatureMethod = true;
			}
			else
			{
				throw new DependencyException("No action to update '" + getNamespaceURI() + ":" + typeName
					+ "' found!");
			}
		} while (true);

		// check for "the_geom" attribute
		if (attributes.containsKey("the_geom")
			&& featureType.getGeometryDescriptor() instanceof DynamicAttributeDescriptor)
		{
			DynamicAttributeDescriptor descriptor = (DynamicAttributeDescriptor) featureType
				.getGeometryDescriptor();
			String name = DIUtil.normalize(descriptor.getMember());
			attributes.put(name, attributes.get("the_geom"));
		}

		ContextHolder context = ContextManager.getInstance().getContext().createSubContext();
		context.registerInstance("attributes", attributes);

		if (useMultiFeature || useSingleFeature)
		{
			Object[] objects = searchObjects(featureType, filter);

			if (objects.length == 0)
			{
				throw new DependencyException("No search result for type '" + getNamespaceURI() + ":"
					+ typeName + "' and filter '" + filter + "' found!");
			}

			if (useSingleFeature)
			{
				for (Object object : objects)
				{
					// the update method has no attribute map -> apply the map to the object
					if (!hasAttributeMap)
					{
						DIUtil.apply(object, attributes);
					}
					context.registerInstance("feature", object);
					updated += checkUpdateReturnVal(context.invoke(this.layerInstances, methods), 1);
				}
			}
			else
			{
				// the update method has no attribute map -> apply the map to the object
				if (!hasAttributeMap)
				{
					for (Object object : objects)
					{
						DIUtil.apply(object, attributes);
					}
				}
				context.registerInstance("features", objects);
				updated += checkUpdateReturnVal(context.invoke(this.layerInstances, methods),
					objects.length);
			}
		}
		else
		{
			context.registerInstance("filter", filter);
			updated += checkUpdateReturnVal(context.invoke(this.layerInstances, methods), 1);
		}

		return updated;
	}

	private int checkUpdateReturnVal(Object returnVal, int defaultSize)
	{
		int size = defaultSize;
		if (returnVal != null)
		{

		}
		return size;
	}

	@Override
	public int deleteFeatures(String typeName, Filter filter)
	{
		return 0;
	}

	protected void addActions(List<DynamicFeatureLayerAction> actions)
	{
		this.actions.addAll(actions);
	}
}
