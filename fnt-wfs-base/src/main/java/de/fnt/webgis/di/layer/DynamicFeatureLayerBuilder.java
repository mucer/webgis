package de.fnt.webgis.di.layer;

import java.lang.annotation.AnnotationFormatError;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.opengis.feature.simple.SimpleFeatureType;

import de.fnt.webgis.annotation.AnnotationUtil;
import de.fnt.webgis.annotation.Feature;
import de.fnt.webgis.annotation.Ignore;
import de.fnt.webgis.annotation.Layer;
import de.fnt.webgis.annotation.LayerAction;
import de.fnt.webgis.context.ContextHolder;
import de.fnt.webgis.context.ContextManager;
import de.fnt.webgis.di.feature.DynamicFeatureType;
import de.fnt.webgis.di.feature.DynamicFeatureTypeBuilder;
import de.fnt.webgis.di.feature.DynamicFeatureTypeBuilder.Link;

/**
 * Parses a class as layer, which was tagged with a {@link Layer} annotation.
 */
public class DynamicFeatureLayerBuilder
{
	protected final static Logger logger = Logger.getLogger(DynamicFeatureLayerBuilder.class);

	private String namespaceURI = null;

	/** Contains all parsed feature types */
	private final List<Class<?>> featureTypes = new ArrayList<Class<?>>();

	/** Holds the feature types with their class (key = feature type name) */
	private final List<DynamicFeatureTypeBuilder> featureTypeBuilders = new ArrayList<DynamicFeatureTypeBuilder>();

	/** Holds the feature types and the methods which return it */
	private final List<DynamicFeatureLayerAction> actions = new ArrayList<DynamicFeatureLayerAction>();

	/** Contains layers which have already been instantiated */
	private final List<Object> layerInstances = new ArrayList<Object>();
	
	public DynamicFeatureLayerBuilder()
	{
	}

	/**
	 * Checks whether this layer class (or a super class of this class) was added to the builder.
	 * 
	 * @param cls Check this class.
	 * @return Returns true if this class was added.
	 */
	public boolean containsLayer(Class<?> cls)
	{
		for (DynamicFeatureLayerAction action : actions)
		{
			if (cls.isAssignableFrom(action.getMethod().getDeclaringClass()))
			{
				return true;
			}
		}
		return false;
	}

	/**
	 * Parses the given class as a layer.
	 * 
	 * @param cls Parse this class as layer.
	 * @return The number of found layer actions.
	 */
	public int addLayer(Class<?> cls)
	{
		if (cls == null || containsLayer(cls))
		{
			logger.warn("Class '" + cls + "' was already added to the layer builder!");
			return 0;
		}

		if (logger.isInfoEnabled())
		{
			logger.info("Parsing class '" + cls.getName() + "' as layer");
		}

		// check layer annotation
		Layer layerAnn = AnnotationUtil.getAnnotation(cls, Layer.class);
		if (layerAnn == null)
		{
			throw new AnnotationFormatError(cls.getCanonicalName() + " has no Layer annotation");
		}

		// get namespace of the new layer
		String namespaceURI = layerAnn.value();
		if (this.namespaceURI == null)
		{
			this.namespaceURI = namespaceURI;
			if (logger.isDebugEnabled())
			{
				logger.debug("Using namespace '" + namespaceURI + "'");
			}
		}
		// not the first layer class -> namespace must match
		else if (!this.namespaceURI.equals(namespaceURI))
		{
			throw new RuntimeException("The namespace '" + namespaceURI
				+ " doesn't match to existing namespace '" + this.namespaceURI + "'");
		}

		// build layer actions
		DynamicFeatureLayerActionBuilder builder = new DynamicFeatureLayerActionBuilder();
		Method[] methods = cls.getMethods();
		List<Method> after = new ArrayList<Method>();
		int addedActions = 0;
		for (Method method : methods)
		{
			try
			{
				if (AnnotationUtil.isAnnotationPresent(method, Ignore.class))
				{
					if (logger.isDebugEnabled())
					{
						logger.debug("Ignore annotation for method '" + method + "' found.");
					}
					continue;
				}

				// check for LayerAction annotation
				LayerAction anno = AnnotationUtil.getAnnotation(method, LayerAction.class);
				// if annotation is present and a feature type name is given -> add to post processing
				// (make sure the class for this feature was found)
				if (anno != null && anno.typeName().length() > 0)
				{
					after.add(method);
					continue;
				}

				DynamicFeatureLayerAction action = builder.parseMethod(method);
				if (action != null && !this.actions.contains(action))
				{
					this.actions.add(action);
					addedActions++;
					if (logger.isDebugEnabled())
					{
						logger.debug("Added new action '" + action + "' to '" + this + "'");
					}
					addFeatureType(action.getFeatureClass());
				}
			}
			catch (Exception e)
			{
				logger.error("Couldn't add method '" + method + "'", e);
			}
		}

		for (Method method : after)
		{
			try
			{
				LayerAction anno = AnnotationUtil.getAnnotation(method, LayerAction.class);
				String typeName = anno.typeName();
				DynamicFeatureTypeBuilder featureTypeBuilder = getFeatureTypeBuilder(typeName);
				if (featureTypeBuilder == null)
				{
					throw new Exception("No feature type class for feature type name '" + typeName
						+ "' found!");
				}
				Class<?> featureType = featureTypeBuilder.getFeatureClass();

				DynamicFeatureLayerAction action = builder.parseMethod(featureType, method);
				if (action != null && !this.actions.contains(action))
				{
					this.actions.add(action);
					addedActions++;
					if (logger.isDebugEnabled())
					{
						logger.debug("Added new action '" + action + "' to '" + this + "'");
					}
				}
			}
			catch (Exception e)
			{
				logger.error("Couldn't add method '" + method + "'", e);
			}
		}

		if (logger.isDebugEnabled())
		{
			logger.debug("Finished parsing of '" + cls + "' added '" + addedActions + "' action(s).");
		}
		return addedActions;
	}

	/**
	 * Adds a already instantiated layer.
	 * 
	 * @param layerInstance Add this instance
	 * @return Returns the number of added actions
	 */
	public int addLayerInstance(Object newInstance)
	{
		Class<?> layer = newInstance.getClass();
		boolean found = false;
		boolean forceAdd = false;
		if (containsLayer(layer))
		{
			// check for super layer or same layer instance
			for (Object instance : this.layerInstances)
			{
				if (layer == newInstance.getClass())
				{
					if (instance == newInstance)
					{
						logger.debug("Layer '" + newInstance + "' was already added");
					}
					else
					{
						logger.warn("Another instanceof the layer '" + newInstance + "' was already added: "
							+ instance);
					}
					found = true;
					break;
				}
				else if (layer.isAssignableFrom(instance.getClass()))
				{
					logger.warn("A child instance of '" + newInstance + "' was already added: " + instance);
					found = true;
					break;
				}
				else if (instance.getClass().isAssignableFrom(layer))
				{
					logger.warn("Removing super instanceof the layer '" + newInstance + "': " + instance);
					forceAdd = true;
					break;
				}
			}
		}

		int addedActions = 0;
		if (!found || forceAdd)
		{
			addedActions = addLayer(layer);
			if (addedActions > 0 || forceAdd)
			{
				this.layerInstances.add(newInstance);
			}
		}
		return addedActions;
	}

	public void addFeatureType(Class<?> cls)
	{
		if (cls.isArray())
		{
			cls = cls.getComponentType();
		}

		// was class already parsed?
		if (!featureTypes.contains(cls))
		{
			featureTypes.add(cls);

			Feature annotation = AnnotationUtil.getAnnotation(cls, Feature.class);
			if (annotation == null)
			{
				throw new IllegalArgumentException("Annotation '" + Feature.class.getName()
					+ "' not found for class '" + cls.getName() + "'");
			}

			String typeName = annotation.value();

			if (typeName.length() == 0)
			{
				logger.info("Added placeholder '" + cls + "' to '" + this + "'");
			}
			else
			{
				DynamicFeatureTypeBuilder builder = getFeatureTypeBuilder(typeName);
				// first appearance of the class 
				if (builder != null)
				{
					if (!builder.getFeatureClass().isAssignableFrom(cls))
					{
						throw new RuntimeException("Two different classes (" + cls + ", "
							+ builder.getFeatureClass() + ") with feature type name '" + namespaceURI + ":"
							+ typeName + "' found");
					}

					if (logger.isInfoEnabled())
					{
						logger.info("Extended class for class '" + builder.getFeatureClass() + "' found: "
							+ cls);
						builder.setFeatureClass(cls);
					}
				}
				else
				{
					builder = new DynamicFeatureTypeBuilder(this.namespaceURI, cls);
					this.featureTypeBuilders.add(builder);
				}
			}
		}
		else if (logger.isDebugEnabled())
		{
			logger.debug("The feature type '" + cls + "' was already parsed");
		}
	}

	public DynamicFeatureLayer build(ContextHolder context)
	{
		return build(context, new HashMap<Class<?>, SimpleFeatureType>());
	}

	/**
	 * 
	 * @param featureTypes Maps a java class to a feature type (from other layers).
	 * @return
	 */
	public DynamicFeatureLayer build(ContextHolder context,
		Map<Class<?>, SimpleFeatureType> featureTypes)
	{
		if (logger.isInfoEnabled())
		{
			logger.info("Building layer for namespace '" + this.namespaceURI + "'");
		}

		// create feature layer
		DynamicFeatureLayer featureLayer = new DynamicFeatureLayer(this.namespaceURI);

		// collect all possible layer classes
		List<Class<?>> layerClasses = new ArrayList<Class<?>>();
		for (DynamicFeatureLayerAction action : actions)
		{
			Class<?> cls = action.getMethod().getDeclaringClass();
			if (!layerClasses.contains(cls))
			{
				layerClasses.add(cls);
			}
		}

		// create layer instances
		while (layerClasses.size() > 0)
		{
			Class<?> mainClass = layerClasses.remove(0);

			// check whether a class is assignable from another
			for (Iterator<Class<?>> it = layerClasses.iterator(); it.hasNext();)
			{
				Class<?> layerClass = it.next();
				if (mainClass.isAssignableFrom(layerClass))
				{
					it.remove();
					mainClass = layerClass;
				}
				else if (layerClass.isAssignableFrom(mainClass))
				{
					it.remove();
				}
			}

			// check for existing class
			Object layerInstance = null;
			for (Object existing : this.layerInstances)
			{
				if (existing.getClass() == mainClass)
				{
					layerInstance = existing;
					if (logger.isDebugEnabled())
					{
						logger.debug("Adding existing layer instance '" + existing + "'");
					}
					break;
				}
			}
			// create new instance
			if (layerInstance == null)
			{
				// Create temporary empty context to create instance
				if (context == null)
				{
					context = ContextManager.getInstance().getContext();
				}
				layerInstance = context.newInstance(mainClass, true);
				if (logger.isDebugEnabled())
				{
					logger.debug("Adding new layer instance of " + mainClass);
				}
			}
			featureLayer.layerInstances.add(layerInstance);
		}

		// sort feature type builders by their dependencies
		List<DynamicFeatureTypeBuilder> sorted = getSortedFeatureTypeBuilders(featureTypes.keySet());
		for (DynamicFeatureTypeBuilder builder : sorted)
		{
			// build feature type
			DynamicFeatureType featureType = (DynamicFeatureType) builder.buildFeatureType(featureTypes);
			featureLayer.addFeatureType(featureType);
			featureTypes.put(featureType.getFeatureClass(), featureType);
		}

		featureLayer.addActions(this.actions);

		return featureLayer;
	}

	/**
	 * Sort feature type builders by their dependency
	 * 
	 * @param existingFeatureClasses Sort by this feature classes
	 * @return The sorted builders
	 */
	private List<DynamicFeatureTypeBuilder> getSortedFeatureTypeBuilders(
		Collection<Class<?>> existingFeatureClasses)
	{
		List<DynamicFeatureTypeBuilder> sorted = new ArrayList<DynamicFeatureTypeBuilder>();
		Collection<DynamicFeatureTypeBuilder> unsorted = new ArrayList<DynamicFeatureTypeBuilder>(
			featureTypeBuilders);

		// copy array
		existingFeatureClasses = new ArrayList<Class<?>>(existingFeatureClasses);

		Link lastLink = null;
		a: while (unsorted.size() > 0)
		{
			b: for (Iterator<DynamicFeatureTypeBuilder> it = unsorted.iterator(); it.hasNext();)
			{
				DynamicFeatureTypeBuilder typeBuilder = it.next();
				c: for (Link link : typeBuilder.getLinks())
				{
					lastLink = link;
					for (Class<?> existingFeatureClass : existingFeatureClasses)
					{
						if (existingFeatureClass.isAssignableFrom(link.featureClass))
						{
							// check next link
							continue c;
						}
					}
					// a link was not found -> next builder
					continue b;
				}
				// all links found
				sorted.add(typeBuilder);
				existingFeatureClasses.add(typeBuilder.getFeatureClass());
				it.remove();

				// restart
				continue a;
			}

			// no builder matched
			throw new RuntimeException("Could not satisfy link '" + lastLink + "'");
		}

		return sorted;
	}

	public Collection<DynamicFeatureTypeBuilder> getFeatureTypeBuilders()
	{
		return Collections.unmodifiableCollection(this.featureTypeBuilders);
	}

	public DynamicFeatureTypeBuilder getFeatureTypeBuilder(String typeName)
	{
		DynamicFeatureTypeBuilder builder = null;
		for (DynamicFeatureTypeBuilder existing : this.featureTypeBuilders)
		{
			if (typeName.equals(existing.getName()))
			{
				builder = existing;
				break;
			}
		}
		if (logger.isDebugEnabled())
		{
			logger.debug("Returning feature type for type name '" + typeName + "': " + builder);
		}
		return builder;
	}

	public String getNamespaceURI()
	{
		return this.namespaceURI;
	}

	@Override
	public String toString()
	{
		return "[AnnotationFeatureLayerBuilder: " + this.namespaceURI + "]";
	}
}
