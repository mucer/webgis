package de.fnt.webgis.di.attribute;

import java.lang.reflect.AccessibleObject;

import org.geotools.feature.type.AttributeDescriptorImpl;
import org.opengis.feature.type.AttributeDescriptor;
import org.opengis.feature.type.AttributeType;
import org.opengis.feature.type.Name;

/**
 * Extends {@link AttributeDescriptorImpl} and stores the matching member which can 
 * provide the value for this attribute
 */
public class DynamicAttributeDescriptorImpl extends AttributeDescriptorImpl implements
	DynamicAttributeDescriptor
{
	/** The linked method */
	final AccessibleObject member;

	/**
	 * @param method Link this method to this attribute descriptor.
	 * @param meta 
	 * @see AttributeDescriptor
	 */
	public DynamicAttributeDescriptorImpl(AttributeType type, Name name, int min, int max,
		boolean isNillable, Object defaultValue, AccessibleObject member)
	{
		super(type, name, min, max, isNillable, defaultValue);
		this.member = member;
	}

	public AccessibleObject getMember()
	{
		return this.member;
	}
}
