package de.fnt.webgis.di.feature;

import java.lang.annotation.AnnotationFormatError;
import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.geotools.feature.NameImpl;
import org.geotools.feature.simple.SimpleFeatureTypeBuilder;
import org.geotools.feature.type.AttributeTypeImpl;
import org.geotools.feature.type.GeometryTypeImpl;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.feature.type.AttributeDescriptor;
import org.opengis.feature.type.AttributeType;
import org.opengis.feature.type.GeometryType;
import org.opengis.feature.type.Name;

import com.vividsolutions.jts.geom.Geometry;

import de.fnt.webgis.annotation.AnnotationUtil;
import de.fnt.webgis.annotation.Attribute;
import de.fnt.webgis.annotation.Feature;
import de.fnt.webgis.di.attribute.AttributeMetaData;
import de.fnt.webgis.di.attribute.DynamicAttributeDescriptor;
import de.fnt.webgis.di.attribute.DynamicAttributeDescriptorImpl;
import de.fnt.webgis.di.attribute.DynamicGeometryDescriptorImpl;

/**
 * Automatically builds a new feature. Collects the needed information from a class
 * which was tagged with a {@link Feature} annotation.
 */
public class DynamicFeatureTypeBuilder extends SimpleFeatureTypeBuilder
{
	protected static final Logger logger = Logger.getLogger(DynamicFeatureTypeBuilder.class);

	/** Fields and return types with this type are valid attributes */
	protected static List<Class<?>> SIMPLE_TYPES = Arrays.asList(new Class<?>[] { String.class,
		Integer.class, Double.class, Float.class, Boolean.class, UUID.class, Date.class });

	/** Map the simple types */
	protected static Map<Class<?>, Class<?>> MAPPED_TYPES = new HashMap<Class<?>, Class<?>>();
	static
	{
		MAPPED_TYPES.put(int.class, Integer.class);
		MAPPED_TYPES.put(double.class, Double.class);
		MAPPED_TYPES.put(float.class, Float.class);
		MAPPED_TYPES.put(boolean.class, Boolean.class);
	}

	/** The name of the geometry attribute */
	public static String GEOM_ATTRIB_NAME = "the_geom";

	/** The class to which this feature type is bound to */
	protected Class<?> featureClass;

	/** Links to other feature types (for complexe types) */
	protected final List<Link> links = new ArrayList<Link>();

	/**
	 * Stores the link to another feature class
	 */
	public class Link
	{
		/** The attribute meta data */
		public final AttributeMetaData meta;

		/** The class of the other feature */
		public final Class<?> featureClass;

		/** The field which represents the other feature */
		public final AccessibleObject member;

		private Link(AttributeMetaData meta, Class<?> featureClass, AccessibleObject member)
		{
			this.meta = meta;
			this.featureClass = featureClass;
			this.member = member;
		}

		@Override
		public String toString()
		{
			return "[" + DynamicFeatureTypeBuilder.this.toString() + "->" + featureClass.getName() + "]";
		}
	}

	/**
	 * New {@link DynamicFeatureType}
	 * 
	 * @param namespaceURI Use this namespace uri for the feature type
	 * @param cls Get the name and the attributes from this class
	 */
	public DynamicFeatureTypeBuilder(String namespaceURI, Class<?> cls)
	{
		super(new DynamicFeatureTypeFactory());
		setNamespaceURI(namespaceURI);
		this.featureClass = cls;
		parseClass(cls);
	}

	/**
	 * Parses the classes for attributes and the name.
	 * 
	 * @param cls Parse this class.
	 */
	protected void parseClass(Class<?> cls)
	{
		if (logger.isInfoEnabled())
		{
			logger.info("Parsing class '" + cls.getCanonicalName() + "' as feature type");
		}

		// check annotation
		Feature annotation = AnnotationUtil.getAnnotation(cls, Feature.class);
		if (annotation == null)
		{
			throw new AnnotationFormatError(cls.getCanonicalName() + " has no FeatureType annotation");
		}
		setName(annotation.value());

		Map<String, AttributeMetaData> fieldMetaData = getFieldMetaData(cls);

		// TODO parameter und felder prüfen
		// get methods
		Method[] methods = cls.getMethods();
		for (Method method : methods)
		{
			if (method.getDeclaringClass() == Object.class || method.getName().equals("toString")
				|| method.getName().equals("equals"))
			{
				continue;
			}

			// only getters without parameter are used
			if (method.getParameterTypes().length > 0)
			{
				// logger.debug(method.getName() + " has parameters");
				// TODO allow parameters (?)
				continue;
			}

			DynamicAttributeDescriptor attrib = parseMember(cls, method, fieldMetaData);
			if (attrib != null)
			{
				add(attrib);
			}
			// TODO handle default geometry
		}

		// check public fields
		Field[] fields = cls.getFields();
		for (Field field : fields)
		{
			DynamicAttributeDescriptor attrib = parseMember(cls, field, null);
			if (attrib != null)
			{
				if (getAttribute(attrib.getLocalName()) != null)
				{
					logger.warn("Attribute for name '" + attrib.getName() + "' was already added!");
				}
				else
				{
					add(attrib);
				}
			}
		}

		// check geometry method
		//		if (!hasGeomentryAttribute())
		//		{
		//			throw new IllegalStateException("A method which returns a Geometry object is required: "
		//				+ cls.getCanonicalName());
		//		}

	}

	protected DynamicAttributeDescriptor getAttribute(String attributeName)
	{
		for (AttributeDescriptor attrib : attributes())
		{
			if (attrib.getLocalName().equals(attributeName))
			{
				return (DynamicAttributeDescriptor) attrib;
			}
		}
		return null;
	}

	protected Map<String, AttributeMetaData> getFieldMetaData(Class<?> cls)
	{
		Map<String, AttributeMetaData> fieldMetaData = new HashMap<String, AttributeMetaData>();

		// Search for annotations on fields 
		// Also annotation from private fields will be found. If the attribute is
		// returned by a method, the annotation data of the field will be used.
		List<Field> fields = AnnotationUtil.getFields(cls, Attribute.class, true);
		for (Field field : fields)
		{
			AttributeMetaData meta = new AttributeMetaData(field);
			if (meta.hasAnnotation)
			{
				fieldMetaData.put(meta.name, meta);
			}
		}
		return fieldMetaData;
	}

	/**
	 * @param method Parse this method
	 * @param fieldMetaData The fields of the class with Attribute annotations.
	 * @return
	 */
	@SuppressWarnings("unchecked")
	protected DynamicAttributeDescriptor parseMember(Class<?> cls, AccessibleObject member,
		Map<String, AttributeMetaData> fieldMetaData)
	{
		DynamicAttributeDescriptor attrib = null;

		// check ignore annotation
		AttributeMetaData meta = new AttributeMetaData(member);
		if (fieldMetaData != null && fieldMetaData.containsKey(meta.name))
		{
			if (meta.hasAnnotation)
			{
				logger.warn("Found a method and a field for attribute name '" + meta.name + "' in class '"
					+ cls.getName() + "', removing field from attributes");
			}
			else
			{
				AttributeMetaData fieldMeta = fieldMetaData.get(meta.name);

				if (fieldMeta.type != meta.type)
				{
					throw new IllegalStateException(
						"Found a field and a method with the same attribute name '" + meta.name
							+ "' but with differend return types.");
				}

				if (logger.isDebugEnabled())
				{
					logger.debug("Using annotation from field for attribute '" + meta.name + "'");
				}
				meta = fieldMeta;
			}
		}

		Class<?> returnType = meta.type;
		if (!meta.ignore && meta.type != Void.class)
		{
			// check mapping
			if (MAPPED_TYPES.containsKey(returnType))
			{
				returnType = MAPPED_TYPES.get(returnType);
			}

			// geometry
			if (Geometry.class.isAssignableFrom(returnType))
			{
				Name name = new NameImpl(GEOM_ATTRIB_NAME);
				GeometryType gt = new GeometryTypeImpl(name, returnType, null /*crs*/, false, false,
					Collections.EMPTY_LIST, null, null);
				attrib = new DynamicGeometryDescriptorImpl(gt, name, 1, 1, meta.nillable, null, member,
					meta);
			}
			// simple type
			else if (SIMPLE_TYPES.contains(returnType))
			{
				Name name = new NameImpl(meta.name);
				AttributeType at = new AttributeTypeImpl(name, returnType, meta.isUnique, false,
					Collections.EMPTY_LIST, null, null);
				attrib = new DynamicAttributeDescriptorImpl(at, name, meta.min, meta.max, meta.nillable,
					null, member);
			}
			// link to other type
			else if (returnType.isAnnotationPresent(Feature.class))
			{
				this.links.add(new Link(meta, returnType, member));
			}
			else
			{
				logger.info("No supported return type '" + returnType + "' for member '" + member + "'");
			}
		}
		return attrib;
	}

	/**
	 * The linked class
	 * 
	 * @return The class
	 */
	public Class<?> getParentClass()
	{
		return featureClass;
	}

	/**
	 * Finally builds the feature type
	 * 
	 * @return The new feature type. 
	 */
	@Override
	public DynamicFeatureType buildFeatureType()
	{
		return (DynamicFeatureType) buildFeatureType(Collections
			.<Class<?>, SimpleFeatureType> emptyMap());
	}

	/**
	 * Build the feature type with extended mapping to other feature types
	 * 
	 * @param A list with the correspondending feature types for another class
	 * @return The new feature type
	 */
	public SimpleFeatureType buildFeatureType(Map<Class<?>, SimpleFeatureType> featureTypes)
	{
		if (logger.isInfoEnabled())
		{
			logger.info("Building feature type '" + getNamespaceURI() + ":" + getName() + "'");
		}

		// try to get a feature type for each link
		for (Link link : this.links)
		{
			SimpleFeatureType featureType = featureTypes.get(link.featureClass);
			if (featureType == null)
			{
				throw new RuntimeException(getName() + ": No feature type for class '" + link.featureClass
					+ "' found");
			}
			add(new DynamicAttributeDescriptorImpl(featureType, featureType.getName(), link.meta.min,
				link.meta.max, link.meta.nillable, null, link.member));
		}

		// avoid redundant adding
		links.clear();

		DynamicFeatureType featureType = (DynamicFeatureType) super.buildFeatureType();
		featureType.featureClass = this.featureClass;
		if (logger.isDebugEnabled())
		{
			logger.debug("Build feature type: " + featureType);
		}
		return featureType;
	}

	/**
	 * Overwritten so only a {@link DynamicAttributeDescriptor} can be added
	 * @see AttributeDescriptor#add
	 */
	public void add(DynamicAttributeDescriptor descriptor)
	{
		super.add(descriptor);
	}

	/**
	 * Overwritten so only a {@link DynamicAttributeDescriptor} can be added
	 * @see AttributeDescriptor#add
	 */
	public void add(int index, DynamicAttributeDescriptor descriptor)
	{
		super.add(index, descriptor);
	}

	/**
	 * Returns the links of all required links. This is necessary to define to order of the
	 * implementation.
	 * 
	 * @return List of links.
	 */
	public List<Link> getLinks()
	{
		return Collections.unmodifiableList(this.links);
	}

	/**
	 * Sets a new feature class for this type. The new feature class must extend
	 * the old feature class.
	 * 
	 * @param featureClass The new feature class.
	 */
	public void setFeatureClass(Class<?> featureClass)
	{
		if (featureClass == null && !this.featureClass.isAssignableFrom(featureClass))
		{
			throw new IllegalArgumentException("The new feature class '" + featureClass
				+ "' must extend the old feature class '" + this.featureClass + "'");
		}
		this.featureClass = featureClass;
	}

	/**
	 * Returns the class, to which this builder is bound
	 * 
	 * @return The bound class.
	 */
	public Class<?> getFeatureClass()
	{
		return this.featureClass;
	}

	@Override
	public String toString()
	{
		return "[AnnotationFeatureTypeBuilder: " + getNamespaceURI() + ":" + getName() + "]";
	}
}
