package de.fnt.webgis.di;

public class DependencyException extends RuntimeException
{
	private static final long serialVersionUID = 1L;

	public DependencyException(Throwable e)
	{
		super(e);
	}

	public DependencyException(String message)
	{
		super(message);
	}

	public DependencyException(String message, Exception e)
	{
		super(message, e);
	}
}
