package de.fnt.webgis.di.feature;

import java.util.List;

import org.geotools.feature.type.FeatureTypeFactoryImpl;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.feature.type.AttributeDescriptor;
import org.opengis.feature.type.AttributeType;
import org.opengis.feature.type.GeometryDescriptor;
import org.opengis.feature.type.Name;
import org.opengis.filter.Filter;
import org.opengis.util.InternationalString;

/**
 * Overwrites the {@link FeatureTypeFactoryImpl} so instead of a 
 * {@link SimpleFeatureType} a {@link DynamicFeatureType} is created
 */
public class DynamicFeatureTypeFactory extends FeatureTypeFactoryImpl
{
	@Override
	public SimpleFeatureType createSimpleFeatureType(Name name, List<AttributeDescriptor> schema,
		GeometryDescriptor defaultGeometry, boolean isAbstract, List<Filter> restrictions,
		AttributeType superType, InternationalString description)
	{
		return new DynamicFeatureType(name, schema, defaultGeometry, isAbstract, restrictions,
			superType, description);
	}
}
