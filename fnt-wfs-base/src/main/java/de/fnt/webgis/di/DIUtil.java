package de.fnt.webgis.di;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

public class DIUtil
{
	protected static final Logger logger = Logger.getLogger(DIUtil.class);

	/**
	 * Gets all public accessible methods which start with "set", have exact one parameter and return a value. The name of the method
	 * will be normalized ({@link #normalize(String)}) and added the the return map.
	 * 
	 * @param cls Search this class.
	 * @return A map with all found setters (key = normalized method name)
	 * 
	 * @see DIUtil#normalize(String)
	 */
	public static Map<String, Method> getSetters(Class<?> cls)
	{
		List<Method> result = new ArrayList<Method>();
		Method[] methods = cls.getMethods();
		for (Method method : methods)
		{
			if (method.getName().startsWith("set") && method.getParameterTypes().length == 1
				&& method.getReturnType() != Void.class)
			{
				result.add(method);
			}
		}

		Map<String, Method> map = new HashMap<String, Method>();
		for (Method method : result)
		{
			String name = DIUtil.normalize(method.getName()).toLowerCase();
			if (map.containsKey(name))
			{
				logger.warn("Duplicated entry for name '" + name + "' found!");
			}
			else
			{
				map.put(name, method);
			}
		}
		return map;
	}

	/**
	 * Gets all public accessible fields. The name of the field
	 * will be normalized ({@link #normalize(String)}) and added the the return map.
	 * 
	 * @param cls Search this class.
	 * @return A map with all found fields (key = normalized method name)
	 * 
	 * @see DIUtil#normalize(String)
	 */
	public static Map<String, Field> getFields(Class<?> cls)
	{
		Field[] fields = cls.getFields();
		Map<String, Field> map = new HashMap<String, Field>();

		for (Field field : fields)
		{
			String name = DIUtil.normalize(field.getName()).toLowerCase();
			if (map.containsKey(name))
			{
				logger.warn("Duplicated entry for name '" + name + "' found!");
			}
			else
			{
				map.put(name, field);
			}
		}

		return map;
	}

	/**
	 * Normalizes the name of the given member.
	 * 
	 * @param member The member
	 * @return The normalized name
	 * 
	 * @see DIUtil#normalize(String)
	 */
	public static String normalize(AccessibleObject member)
	{
		String name;
		if (member instanceof Field)
		{
			name = normalize(((Field) member).getName());
		}
		else if (member instanceof Method)
		{
			name = normalize(((Method) member).getName());
		}
		else
		{
			logger.warn("Could not normalize member '" + member + "'");
			name = null;
		}
		return name;
	}

	/**
	 * Removes the prefix "get", "set", or "is" and converts the first letter to lower case.
	 * 
	 * @param name Normalize this name.
	 * @return The normalized name.
	 */
	public static String normalize(String name)
	{
		if (name != null)
		{
			if (name.length() > 3 && (name.startsWith("get") || name.startsWith("set"))
				&& Character.isUpperCase(name.charAt(3)))
			{
				name = name.substring(3);
			}
			else if (name.length() > 2 && name.startsWith("is") && Character.isUpperCase(name.charAt(2)))
			{
				name = name.substring(2);
			}

			name = (name.substring(0, 1).toLowerCase()) + name.substring(1);
		}
		return name;
	}

	/**
	 * Gets all fields and setters from the given object and sets the given attributes.
	 * 
	 * @param object Update this object.
	 * @param attributes Use this attributes.
	 * 
	 * @see DIUtil#getSetters(Class)
	 * @see DIUtil#getFields(Class)
	 */
	public static void apply(Object object, Map<String, Object> attributes)
	{
		Map<String, Method> setters = getSetters(object.getClass());
		Map<String, Field> fields = getFields(object.getClass());

		for (Entry<String, Object> attrib : attributes.entrySet())
		{
			String name = attrib.getKey().toLowerCase();
			Object value = attrib.getValue();

			try
			{
				if (setters.containsKey(name))
				{
					Method method = setters.get(name);
					method.invoke(object, value);
				}
				else if (fields.containsKey(name))
				{
					Field field = fields.get(name);
					field.set(object, value);
				}
				else
				{
					logger.warn("No setter of field for attribute '" + name + "' in class '"
						+ object.getClass().getName() + "' found!");
				}
			}
			catch (Exception e)
			{
				logger.error("Could not set value '" + value + "' to attribute '" + name + "' for object '"
					+ object + "'");
			}
		}
	}
}
