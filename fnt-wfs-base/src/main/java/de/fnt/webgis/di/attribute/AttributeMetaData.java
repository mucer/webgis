package de.fnt.webgis.di.attribute;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

import org.apache.log4j.Logger;

import de.fnt.webgis.annotation.Attribute;
import de.fnt.webgis.annotation.Ignore;
import de.fnt.webgis.di.DIUtil;

/**
 * Reads the meta data of an method/field which represents
 * a feature type attribute
 */
public class AttributeMetaData
{
	private static final Logger logger = Logger.getLogger(AttributeMetaData.class);

	/** The name of the attribute */
	public final String name;

	/** This attribute can be null */
	public final boolean nillable;

	/** Use this attribute in the web feature service */
	public final boolean ignore;

	/** Minimal occurrence of this attribute */
	public final int min;

	/** Maximal occurrence of this attribute */
	public final int max;

	/** Can this attribute be used as primary id */
	public final boolean isUnique;

	public final boolean hasAnnotation;

	public final Class<?> type;

	public AttributeMetaData(AccessibleObject object)
	{
		boolean nillable = false;
		String name = null;
		int min = 1;
		int max = 1;
		boolean isUnique = false;
		boolean ignore = false;
		Class<?> type;

		// get type
		if (object instanceof Method)
		{
			type = ((Method) object).getReturnType();
		}
		else if (object instanceof Field)
		{
			type = ((Field) object).getType();
		}
		else
		{
			throw new IllegalArgumentException("Only methods and fields are supported: " + object);
		}
		if (type.isArray())
		{
			this.type = type.getComponentType();
		}
		else
		{
			this.type = type;
		}

		hasAnnotation = object.isAnnotationPresent(Attribute.class);
		// check Attribute annotation
		if (hasAnnotation)
		{
			Attribute annotation = object.getAnnotation(Attribute.class);
			name = annotation.name();
			nillable = annotation.nillable();
			min = annotation.min();
			max = annotation.max();
			isUnique = annotation.isUnique();
		}

		// check Ignore annotation
		if (object.isAnnotationPresent(Ignore.class))
		{
			ignore = true;
		}

		// build name if not given
		if (name == null || name.length() == 0)
		{
			if (object instanceof Method)
			{
				name = DIUtil.normalize(((Method) object).getName());
			}
			else if (object instanceof Field)
			{
				name = DIUtil.normalize(((Field) object).getName());
			}
		}

		if (nillable)
		{
			min = 0;
		}

		// set minimum and maximum
		if (min > max)
		{
			logger.warn("Minimum (" + min + ") is creater than maximum (" + max + ")");
			this.min = max;
			this.max = min;
		}
		else
		{
			this.min = min;
			this.max = max;
		}
		this.ignore = ignore;
		this.isUnique = isUnique;
		this.nillable = nillable;
		this.name = name;
	}

}
