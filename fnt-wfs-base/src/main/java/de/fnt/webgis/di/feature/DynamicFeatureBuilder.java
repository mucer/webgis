package de.fnt.webgis.di.feature;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;

import org.apache.log4j.Logger;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.type.AttributeDescriptor;
import org.opengis.feature.type.ComplexType;

import de.fnt.webgis.di.attribute.DynamicAttributeDescriptor;

/**
 * The class will build a new {@link DynamicFeature}
 */
public class DynamicFeatureBuilder extends SimpleFeatureBuilder
{
	private final static Logger logger = Logger.getLogger(DynamicFeatureBuilder.class);

	public DynamicFeatureBuilder(DynamicFeatureType featureType)
	{
		super(featureType, new DynamicFeatureFactory());
	}

	/**
	 * Cast to AnnotationFeatureType
	 */
	@Override
	public DynamicFeatureType getFeatureType()
	{
		return (DynamicFeatureType) super.getFeatureType();
	}

	/**
	 * Builds a new {@link DynamicFeature}.
	 * 
	 * @param obj Make sure that this obj is an instance of {@link DynamicFeatureType#getFeatureClass}
	 * @return The new {@link DynamicFeature}
	 */
	public DynamicFeature parseObject(Object obj)
	{
		if (obj == null)
		{
			logger.warn("Given object was null");
			return null;
		}

		DynamicFeatureType type = getFeatureType();
		if (obj.getClass() != type.featureClass)
		{
			throw new RuntimeException(type.getName() + ": Only objects of class '" + type.featureClass
				+ "' are suppored");
		}

		// handle descriptors
		List<AttributeDescriptor> descriptors = type.getAttributeDescriptors();
		Object[] values = new Object[descriptors.size()];

		String id = null;
		for (int i = 0; i < descriptors.size(); i++)
		{
			AttributeDescriptor descriptor = descriptors.get(i);

			// check for sub features
			Object value;
			if (descriptor instanceof DynamicAttributeDescriptor)
			{
				try
				{
					AccessibleObject member = ((DynamicAttributeDescriptor) descriptor).getMember();
					if (member instanceof Method)
					{
						value = ((Method) member).invoke(obj, new Object[] {});
						if (value != null && descriptor.getType() instanceof DynamicFeatureType)
						{
							DynamicFeatureBuilder builder = new DynamicFeatureBuilder(
								(DynamicFeatureType) descriptor.getType());
							value = builder.parseObject(value);
						}
					}
					else if (member instanceof Field)
					{
						value = ((Field) member).get(obj);
					}
					else
					{
						value = null;
					}
				}
				catch (Exception e)
				{
					value = descriptor.getDefaultValue();
					logger.warn(type.getName() + ": Could not set dynamic value for attribute '"
						+ descriptor.getLocalName() + "' -> using default value", e);
				}
			}
			else
			{
				value = descriptor.getDefaultValue();
				logger.warn(type.getName() + ": Using default value of descriptor " + descriptor);
			}

			values[i] = value;

			if (descriptor.getType().isIdentified() && !(descriptor.getType() instanceof ComplexType))
			{
				id = value.toString();
				if (logger.isDebugEnabled())
				{
					logger.debug(type.getName() + ": Using attribute '" + descriptor.getLocalName()
						+ "' as id: " + id);
				}
			}
		}

		return (DynamicFeature) buildFeature(id, values);
	}

	public DynamicFeature parseFeature(SimpleFeature in)
	{
		DynamicFeatureType type = getFeatureType();
		DynamicFeature feature;

		// must be the same instance!
		if (in.getFeatureType() == type)
		{
			logger.debug("Feature " + in + " must not be converted. Feature type does already match.");
			feature = (DynamicFeature) in;
		}
		else
		{
			Object[] values = new Object[type.getAttributeCount()];
			String id = null;
			for (int i = 0; i < values.length; i++)
			{
				AttributeDescriptor ad = type.getDescriptor(i);
				Object value = in.getAttribute(ad.getName());

				//				if (value instanceof Property)
				//				{
				//					value = ((Property) value).getValue();
				//				}

				if (ad.getType().isIdentified() && value != null)
				{
					id = value.toString();
				}

				values[i] = value;
			}

			feature = (DynamicFeature) buildFeature(id, values);
		}
		return feature;
	}
}
