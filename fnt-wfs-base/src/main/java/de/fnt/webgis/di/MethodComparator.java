package de.fnt.webgis.di;

import java.lang.reflect.Method;
import java.util.Comparator;

import de.fnt.webgis.context.ContextHolder;
import de.fnt.webgis.context.ContextManager;

/**
 * Compares two methods
 */
public class MethodComparator implements Comparator<Method>
{
	protected ContextHolder context;

	/**
	 * 
	 * @param checkContext If this parameter is true, the number
	 *                     of resolvable parameters is checked.
	 */
	public MethodComparator(boolean checkContext)
	{
		this(checkContext ? ContextManager.getInstance().getContext() : null);
	}

	public MethodComparator(ContextHolder context)
	{
		this.context = context;
	}

	public int compare(Method method1, Method method2)
	{
		int length1 = method1.getParameterTypes().length;
		int length2 = method2.getParameterTypes().length;

		if (this.context != null)
		{
			int resolved1 = countResolvableParameters(method1);
			int resolved2 = countResolvableParameters(method2);

			// same number of parameters resolved
			if (resolved1 == resolved2)
			{
				// return method with lesser parameters
				return length1 - length2;
			}
		}

		return length2 - length1;
	}

	private int countResolvableParameters(Method method)
	{
		int num = 0;
		for (Class<?> type : method.getParameterTypes())
		{
			if (this.context.contains(type))
			{
				num++;
			}
		}
		return num;
	}
}
