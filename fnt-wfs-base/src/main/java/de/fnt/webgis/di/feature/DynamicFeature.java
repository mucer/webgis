package de.fnt.webgis.di.feature;

import org.opengis.feature.simple.SimpleFeature;

/**
 * Dynamic extension for the {@link SimpleFeature} class
 */
public interface DynamicFeature extends SimpleFeature
{
	public Object newFeatureClassInstance();
}
