package de.fnt.webgis.di.layer;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Map;

import org.opengis.filter.Filter;

import de.fnt.webgis.annotation.LayerAction;

/**
 * This class is used to store wfs operation for a {@link DynamicFeatureLayer}.
 */
public abstract class DynamicFeatureLayerAction
{

	enum SearchType implements Comparable<SearchType>
	{
		/** Search method requires a filter object */
		FILTER(1),

		/** Search method requires a bounding box */
		BOUNDING_BOX(2),

		/** Search method requires a gemotry object */
		GEOMETRY(3),

		/** Search method requires a string array */
		MULTI_ID(4),

		/** Search method requires a string */
		SINGLE_ID(5),

		/** Search method has other parameters */
		UNKNOWN(6);

		public final int prio;

		private SearchType(int prio)
		{
			this.prio = prio;
		}

		public static SearchType[] valuesSorted()
		{
			SearchType[] types = values();
			Arrays.sort(types, new Comparator<SearchType>()
			{
				public int compare(SearchType o1, SearchType o2)
				{
					return o1.prio - o2.prio;
				}
			});
			return types;
		}
	}

	enum UpdateType
	{
		/** The update method expects a {@link Filter} and a {@link Map}<String, Object> */
		FILTER(1),

		/** The update method expects an array of features with already updated attributes */
		MULTI_FEATURE(2),

		/** The update method expects a single feature with already updated attributes */
		SINGLE_FEATURE(3);

		public final int prio;

		private UpdateType(int prio)
		{
			this.prio = prio;
		}
	}

	private final LayerAction.ActionType actionType;

	private final Class<?> featureClass;

	private final Method method;

	protected DynamicFeatureLayerAction(LayerAction.ActionType actionType, Method method,
		Class<?> featureClass)
	{
		this.actionType = actionType;
		this.method = method;
		this.featureClass = featureClass;
	}

	public LayerAction.ActionType getActionType()
	{
		return actionType;
	}

	public Method getMethod()
	{
		return this.method;
	}

	public Class<?> getFeatureClass()
	{
		return this.featureClass;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
		{
			return true;
		}

		if (obj == null || !(obj instanceof DynamicFeatureLayerAction))
		{
			return false;
		}

		return this.method.equals(((DynamicFeatureLayerAction) obj).method);
	}

	@Override
	public String toString()
	{
		return "[" + this.getClass().getSimpleName() + ": featureType '"
			+ this.featureClass.getSimpleName() + "', method '" + method + "']";
	}

	public static class Update extends DynamicFeatureLayerAction
	{
		private final UpdateType updateType;

		private final boolean hasMap;

		public Update(Method method, Class<?> featureClass, UpdateType updateType,
			boolean hasAttributeMap)
		{
			super(LayerAction.ActionType.UPDATE, method, featureClass);
			this.updateType = updateType;
			this.hasMap = hasAttributeMap;
		}

		public UpdateType getUpdateType()
		{
			return this.updateType;
		}

		public boolean hasAttributeMap()
		{
			return this.hasMap;
		}
	}

	public static class Search extends DynamicFeatureLayerAction
	{
		private final SearchType searchType;

		public Search(Method method, Class<?> featureClass, SearchType searchType)
		{
			super(LayerAction.ActionType.SEARCH, method, featureClass);
			this.searchType = searchType;
		}

		public SearchType getSearchType()
		{
			return this.searchType;
		}
	}

	public static class Delete extends DynamicFeatureLayerAction
	{
		public Delete(Method method, Class<?> featureClass)
		{
			super(LayerAction.ActionType.DELETE, method, featureClass);
		}
	}

	public static class Insert extends DynamicFeatureLayerAction
	{
		public Insert(Method method, Class<?> featureClass)
		{
			super(LayerAction.ActionType.INSERT, method, featureClass);
		}
	}
}
