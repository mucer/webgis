package de.fnt.webgis.di.attribute;

import java.lang.reflect.AccessibleObject;

import org.geotools.feature.type.GeometryDescriptorImpl;
import org.opengis.feature.type.GeometryType;
import org.opengis.feature.type.Name;

/**
 * Extends the {@link GeometryDescriptorImpl}. Links a method which will provide the value for this attribute.
 */
public class DynamicGeometryDescriptorImpl extends GeometryDescriptorImpl implements
	DynamicAttributeDescriptor
{
	/** This method provides the value for this attribute */
	final AccessibleObject member;

	/** Attribute meta data */
	final AttributeMetaData meta;

	/**
	 * @param method This method provides the value for this attribute
	 * @param meta 
	 * @see GeometryDescriptorImpl#GeometryDescriptorImpl(GeometryType, Name, int, int, boolean, Object)
	 */
	public DynamicGeometryDescriptorImpl(GeometryType type, Name name, int min, int max,
		boolean isNillable, Object defaultValue, AccessibleObject member, AttributeMetaData meta)
	{
		super(type, name, min, max, isNillable, defaultValue);
		this.member = member;
		this.meta = meta;
	}

	public AccessibleObject getMember()
	{
		return this.member;
	}

	public AttributeMetaData getMetaData()
	{
		return meta;
	}

}
