package de.fnt.webgis.di.layer;

import java.lang.reflect.Method;
import java.util.Map;

import org.apache.log4j.Logger;
import org.opengis.filter.Filter;
import org.opengis.geometry.BoundingBox;

import com.vividsolutions.jts.geom.Geometry;

import de.fnt.webgis.annotation.AnnotationUtil;
import de.fnt.webgis.annotation.Feature;
import de.fnt.webgis.annotation.LayerAction;
import de.fnt.webgis.di.DependencyException;
import de.fnt.webgis.di.layer.DynamicFeatureLayerAction.SearchType;
import de.fnt.webgis.di.layer.DynamicFeatureLayerAction.UpdateType;

/**
 * Parses a given method as a wfs operation
 */
public class DynamicFeatureLayerActionBuilder
{
	private final Logger logger = Logger.getLogger(DynamicFeatureLayerAction.class);

	public DynamicFeatureLayerAction parseMethod(Method method)
	{
		return parseMethod(null, method);
	}

	/**
	 * Parses a method as layer action.
	 * 
	 * @param featureClass Use this feature type. If null, the feature type will be get by the parameter types or return types.
	 * @param method Parse this method.
	 * @return
	 */
	public DynamicFeatureLayerAction parseMethod(Class<?> featureClass, Method method)
	{
		DynamicFeatureLayerAction action = null;
		// ignore object class
		if (method.getDeclaringClass() != Object.class)
		{
			String name = method.getName();

			// check annotation
			LayerAction anno = AnnotationUtil.getAnnotation(method, LayerAction.class);
			if (anno != null)
			{
				switch (anno.value()) {
				case SEARCH:
					action = parseSearchMethod(featureClass, method);
					break;
				case INSERT:
					action = parseInsertMethod(method);
					break;
				case UPDATE:
					action = parseUpdateMethod(featureClass, method);
					break;
				case DELETE:
					action = parseDeleteMethod(method);
					break;
				default:
					throw new IllegalArgumentException("Invalid action '" + anno.value()
						+ "' given. See LayerAction constants!");
				}
			}
			// check name
			else if (name.startsWith("get") || name.startsWith("search"))
			{
				action = parseSearchMethod(featureClass, method);
			}
			else if (name.startsWith("insert") || name.startsWith("add"))
			{
				action = parseInsertMethod(method);
			}
			else if (name.startsWith("update"))
			{
				action = parseUpdateMethod(featureClass, method);
			}
			else if (name.startsWith("delete"))
			{
				action = parseDeleteMethod(method);
			}
		}

		return action;
	}

	protected DynamicFeatureLayerAction.Search parseSearchMethod(Class<?> targetFeatureClass,
		Method method)
	{
		Class<?> returnType = method.getReturnType();
		if (returnType.isArray())
		{
			returnType = returnType.getComponentType();
		}

		if (!AnnotationUtil.isAnnotationPresent(returnType, Feature.class))
		{
			throw new IllegalArgumentException("A search method must return a feature: " + method);
		}

		// get search type
		SearchType searchType = null;
		Class<?>[] parameterTypes = method.getParameterTypes();
		for (Class<?> parameterType : parameterTypes)
		{
			if (Filter.class.isAssignableFrom(parameterType))
			{
				searchType = SearchType.FILTER;
				break;
			}
			else if (BoundingBox.class.isAssignableFrom(parameterType))
			{
				searchType = SearchType.BOUNDING_BOX;
				break;
			}
			else if (Geometry.class.isAssignableFrom(parameterType))
			{
				searchType = SearchType.GEOMETRY;
				break;
			}
			else if (parameterType.isArray()
				&& String.class.isAssignableFrom(parameterType.getComponentType()))
			{
				searchType = SearchType.MULTI_ID;
				break;
			}
			else if (String.class.isAssignableFrom(parameterType))
			{
				searchType = SearchType.SINGLE_ID;
				break;
			}
		}

		if (searchType == null)
		{
			logger.warn("No suitable search type for method '" + method + "' found!");
			searchType = SearchType.UNKNOWN;
		}

		return new DynamicFeatureLayerAction.Search(method, returnType, searchType);
	}

	protected DynamicFeatureLayerAction.Insert parseInsertMethod(Method method)
	{
		Class<?> featureCls = checkFeatureClass(null, method);
		if (featureCls == null)
		{
			throw new IllegalStateException("No parameter with annotation '" + Feature.class.getName()
				+ "' found for method '" + method + "'");
		}

		Class<?> returnType = method.getReturnType();

		if (featureCls.isArray())
		{
			if (!returnType.isArray() || returnType.getComponentType() != String.class)
			{
				logger.warn("Method should return a string array with feature ids: " + method);
			}
		}
		else
		{
			if (returnType.isArray() || returnType != String.class)
			{
				logger.warn("Method should return a string with the feature id: " + method);
			}
		}

		return new DynamicFeatureLayerAction.Insert(method, featureCls);
	}

	protected DynamicFeatureLayerAction.Update parseUpdateMethod(Class<?> targetFeatureClass,
		Method method)
	{
		boolean hasFilter = false;
		boolean hasMap = false;
		Class<?>[] parameterTypes = method.getParameterTypes();
		for (Class<?> parameterType : parameterTypes)
		{
			if (Filter.class.isAssignableFrom(parameterType))
			{
				hasFilter = true;
			}
			else if (Map.class.isAssignableFrom(parameterType))
			{
				hasMap = true;
			}
		}

		UpdateType updateType;
		Class<?> featureCls = checkFeatureClass(targetFeatureClass, method);
		if (featureCls != null)
		{
			updateType = (featureCls.isArray() ? UpdateType.MULTI_FEATURE : UpdateType.SINGLE_FEATURE);
		}
		else
		{
			if (logger.isDebugEnabled())
			{
				logger.debug("Found parameters: hasFilter '" + hasFilter + "', hasMap '" + hasMap + "'");
			}

			if (!hasMap)
			{
				throw new DependencyException("No " + Map.class.getName()
					+ " parameter found. No way to pass new properties!");
			}
			if (!hasFilter)
			{
				throw new DependencyException("Invalid parameters for update method '" + method
					+ "'. Expect feature or feature array or Map with Filter/String-Array/String");
			}

			updateType = UpdateType.FILTER;
		}

		return new DynamicFeatureLayerAction.Update(method, featureCls, updateType, hasMap);
	}

	protected DynamicFeatureLayerAction.Delete parseDeleteMethod(Method method)
	{
		Class<?> featureCls = checkFeatureClass(null, method);
		// TODO allow filter and ids parameters?
		if (featureCls == null)
		{
			throw new IllegalStateException("No parameter with annotation '" + Feature.class.getName()
				+ "' found for method '" + method + "'");
		}

		// add delete method
		return new DynamicFeatureLayerAction.Delete(method, featureCls);
	}

	/**
	 * Gets the feature to which this method belongs to
	 * 
	 * @param method
	 * @return
	 */
	private Class<?> checkFeatureClass(Class<?> targetFeatureClass, Method method)
	{
		// check for feature type parameter
		Class<?> parameterTypes[] = method.getParameterTypes();
		Class<?> featureCls = null;
		for (Class<?> parameterType : parameterTypes)
		{
			// parameter is a feature
			if (AnnotationUtil.isAnnotationPresent(parameterType.isArray() ? parameterType
				.getComponentType() : parameterType, Feature.class))
			{
				// more then one feature?
				if (featureCls != null)
				{
					throw new IllegalStateException("More then one feature parameter for method found: "
						+ method);
				}

				featureCls = parameterType;
			}
		}

		// no feature found
		if (targetFeatureClass != null && featureCls != null
			&& !featureCls.isAssignableFrom(targetFeatureClass))
		{
			throw new DependencyException("Could not add method '" + method + ": The class '"
				+ featureCls.getName() + "' is not assignable from '" + targetFeatureClass.getName() + "'");
		}

		return featureCls;
	}
}
