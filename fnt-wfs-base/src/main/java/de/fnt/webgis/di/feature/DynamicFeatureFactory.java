package de.fnt.webgis.di.feature;

import org.geotools.factory.CommonFactoryFinder;
import org.geotools.feature.AbstractFeatureFactoryImpl;
import org.geotools.feature.simple.SimpleFeatureImpl;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.filter.FilterFactory2;

/**
 * Overwrites some methods to return the "dynamic" version
 * of the build objects.
 */
public class DynamicFeatureFactory extends AbstractFeatureFactoryImpl
{
	FilterFactory2 ff = CommonFactoryFinder.getFilterFactory2(null);

	/**
	 * Returns a {@link DynamicFeatureImpl} instead of a {@link SimpleFeatureImpl}.
	 * 
	 * @return The create feature instance.
	 */
	@Override
	public SimpleFeature createSimpleFeature(Object[] array, SimpleFeatureType type, String id)
	{
		if (type.isAbstract())
		{
			throw new IllegalArgumentException("Cannot create an feature of an abstract FeatureType "
				+ type.getTypeName());
		}
		else if (!(type instanceof DynamicFeatureType))
		{
			throw new IllegalArgumentException(DynamicFeatureType.class.getName() + " needed");
		}
		return new DynamicFeatureImpl(array, (DynamicFeatureType) type, ff.featureId(id), false);
	}
}
