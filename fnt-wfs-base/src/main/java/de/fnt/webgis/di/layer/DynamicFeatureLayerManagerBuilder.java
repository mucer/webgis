package de.fnt.webgis.di.layer;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.opengis.feature.simple.SimpleFeatureType;

import de.fnt.webgis.annotation.AnnotationUtil;
import de.fnt.webgis.annotation.Layer;
import de.fnt.webgis.context.ContextHolder;
import de.fnt.webgis.layer.FeatureLayerManager;
import de.fnt.webgis.layer.FeatureLayerManagerBuilder;

public class DynamicFeatureLayerManagerBuilder implements FeatureLayerManagerBuilder
{
	private static final Logger logger = Logger.getLogger(DynamicFeatureLayerManagerBuilder.class);

	private final Map<String, DynamicFeatureLayerBuilder> builders = new HashMap<String, DynamicFeatureLayerBuilder>();

	public DynamicFeatureLayerManagerBuilder()
	{
	}

	/**
	 * Search all layers in the given package
	 * 
	 * @param packageName The absolute name of the package
	 */
	public void parsePackage(String packageName)
	{
		logger.info("Parsing package '" + packageName + "' for layers");

		List<Class<?>> classes = AnnotationUtil.getClasses(packageName, Layer.class);
		for (Class<?> cls : classes)
		{
			addLayer(cls);
		}
	}

	public Collection<DynamicFeatureLayerBuilder> getFeatureLayerBuilders()
	{
		return this.builders.values();
	}

	public FeatureLayerManager build()
	{
		return build(null);
	}

	public FeatureLayerManager build(ContextHolder context)
	{
		if (logger.isDebugEnabled())
		{
			logger.debug("Building layer manager '" + this + "'");
		}

		// collect all types

		FeatureLayerManager manager = new DynamicFeatureLayerManager();

		// TODO Sort by dependencies

		/*
				// Collect all feature types for self referencing
				Map<String, AnnotationFeatureTypeBuilder> featureTypeBuilders = new HashMap<Class<?>, AnnotationFeatureTypeBuilder>();
				for (AnnotationFeatureLayerBuilder layerBuilder : this.builders.values())
				{
					// add to list
					for (AnnotationFeatureTypeBuilder featureTypeBuilder : layerBuilder.getFeatureTypeBuilders())
					{
						if (featureTypeBuilders.containsKey(featureTypeName))
						{
							logger.warn("Dublicate feature type '" + featureTypeName + "' found (classes "
								+ featureTypeBuilders.get(featureTypeName).getParentClass().getCanonicalName()
								+ " (used) and " + featureTypeBuilder.getParentClass().getCanonicalName() + ")");
							continue;
						}

						featureTypeBuilders.put(featureTypeBuilder.getParentClass(), featureTypeBuilder);
					}
				}*/

		Map<Class<?>, SimpleFeatureType> featureTypes = new HashMap<Class<?>, SimpleFeatureType>();

		// TODO sort layers by feature type links
		// build layers
		for (DynamicFeatureLayerBuilder builder : this.builders.values())
		{
			DynamicFeatureLayer layer = builder.build(context, featureTypes);
			manager.addLayer(layer);
		}

		return manager;
	}

	/**
	 * Adds a new layer class. During the build process an instance of this class will be automatically generated.
	 * 
	 * @param layerClass Add this class as new layer.
	 */
	public void addLayer(Class<?> layerClass)
	{
		DynamicFeatureLayerBuilder builder = getLayerBuilder(layerClass);
		builder.addLayer(layerClass);
	}

	public void addLayerInstance(Object layerInstance)
	{
		if (layerInstance != null)
		{
			DynamicFeatureLayerBuilder builder = getLayerBuilder(layerInstance.getClass());
			builder.addLayerInstance(layerInstance);
		}
	}

	/**
	 * Gets the layer builder for this layer class. If no match builder is found a new
	 * instance will be created.
	 * 
	 * @param layerClass Get a builder for this layer class.
	 * @return The builder.
	 */
	private DynamicFeatureLayerBuilder getLayerBuilder(Class<?> layerClass)
	{
		Layer annotation = AnnotationUtil.getAnnotation(layerClass, Layer.class);
		if (annotation == null)
		{
			throw new IllegalArgumentException("The class '" + layerClass.getName() + "' doesn't have '"
				+ Layer.class.getName() + "' annotation");
		}

		String namespaceURI = annotation.value();

		if (logger.isDebugEnabled())
		{
			logger.debug("Adding layer class '" + layerClass.getCanonicalName() + "' to  '" + this + "'");
		}

		DynamicFeatureLayerBuilder builder = this.builders.get(namespaceURI);
		if (builder == null)
		{
			builder = new DynamicFeatureLayerBuilder();
			this.builders.put(namespaceURI, builder);
		}

		return builder;
	}

	/**
	 * The given class must have a Feature annotation. Also a already parsed
	 * layer must refer to an super class of this type. Otherwise it can not be
	 * assigned.
	 * 
	 * @param featureType Add this class as feature type
	 */
	public void addFeatureType(Class<?> featureType)
	{
		for (DynamicFeatureLayerBuilder builder : this.builders.values())
		{
			builder.addFeatureType(featureType);
		}
	}

}
