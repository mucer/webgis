package de.fnt.webgis.di.feature;

import java.util.List;

import org.apache.log4j.Logger;
import org.geotools.feature.simple.SimpleFeatureTypeImpl;
import org.opengis.feature.type.AttributeDescriptor;
import org.opengis.feature.type.AttributeType;
import org.opengis.feature.type.GeometryDescriptor;
import org.opengis.feature.type.Name;
import org.opengis.filter.Filter;
import org.opengis.util.InternationalString;

/**
 * "dynamic" version of the {@link SimpleFeatureTypeImpl}. Also
 * stores the linked feature class.
 */
public class DynamicFeatureType extends SimpleFeatureTypeImpl
{
	protected static final Logger logger = Logger.getLogger(DynamicFeatureType.class);

	/** The class to which this feature type is bound to, will be set by the builder */
	protected Class<?> featureClass = null;

	protected DynamicFeatureType(Name name, List<AttributeDescriptor> schema,
		GeometryDescriptor defaultGeometry, boolean isAbstract, List<Filter> restrictions,
		AttributeType superType, InternationalString description)
	{
		super(name, schema, defaultGeometry, isAbstract, restrictions, superType, description);
	}

	/**
	 * Will return the class, to which this feature type is bound to.
	 * 
	 * @return The bound class
	 */
	public Class<?> getFeatureClass()
	{
		return featureClass;
	}
}
