package de.fnt.webgis.layer;

import java.io.IOException;
import java.net.URL;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.log4j.Logger;
import org.eclipse.xsd.XSDSchema;
import org.geotools.GMLExtended.Version;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.feature.type.Name;
import org.opengis.filter.Filter;

import de.fnt.webgis.ows.wfs.xml.GMLEncoder;

public abstract class FeatureLayerManager
{
	protected static final Logger logger = Logger.getLogger(FeatureLayerManager.class);

	/** Holds all prefixes (key = prefix, value = namespace uri) */
	Map<String, String> prefixes = new HashMap<String, String>();

	/** Holds all layers (key = namespace uri) */
	Map<String, FeatureLayer> layers = new HashMap<String, FeatureLayer>();

	/** Make sure only one thread is writing */
	private final Lock writeLock = new ReentrantLock();

	/**
	 * Must be generated via builder
	 */
	protected FeatureLayerManager()
	{
	}

	/**
	 * Adds the given layer to this manager. 
	 * 
	 * @param layer Add this layer
	 */
	public void addLayer(FeatureLayer layer)
	{
		String namespaceURI = layer.getNamespaceURI();
		if (this.layers.containsKey(namespaceURI))
		{
			throw new RuntimeException("Only one layer per namespace is allowed: '" + layer + "', '"
				+ this.layers.get(namespaceURI) + "'");
		}

		// generate prefix
		int pos = namespaceURI.lastIndexOf("/");
		String prefix = null;
		if (pos > -1 && pos != namespaceURI.length() - 1)
		{
			prefix = namespaceURI.substring(pos + 1);
			if (this.prefixes.containsKey(prefix))
			{
				logger.info("Prefix '" + prefix + "' already used for '" + this.prefixes.get(prefix) + "'");
				prefix = null;
			}
		}

		// TODO other prefix generations?
		int num = 0;
		while (prefix == null)
		{
			if (!this.prefixes.containsKey("Q" + (++num)))
			{
				prefix = "Q" + num;
			}
		}

		this.layers.put(namespaceURI, layer);
		this.prefixes.put(prefix, namespaceURI);

		if (logger.isDebugEnabled())
		{
			logger.debug("Added layer '" + layer + "' with namespace '" + namespaceURI + "' and prefix '"
				+ prefix + "'");
		}
	}

	public Collection<FeatureLayer> getLayers()
	{
		return this.layers.values();
	}

	public FeatureLayer getLayer(String namespaceURI)
	{
		return this.layers.get(namespaceURI);
	}

	//
	//	public List<FeatureLayer> getLayersForTypeName(Name name)
	//	{
	//		List<FeatureLayer> layers = this.layers.get(name.getNamespaceURI());
	//		if (layers == null || layers.isEmpty())
	//		{
	//			throw new IllegalArgumentException("No layers for namespace '" + name.getNamespaceURI()
	//				+ "' found!");
	//		}
	//
	//		for (FeatureLayer layer : layers)
	//		{
	//			if (layer.containsFeatureType(name.getLocalPart()))
	//			{
	//				layers.add(layer);
	//			}
	//		}
	//		return layers;
	//	}

	//	public SimpleFeatureType getFeatureType(Name name)
	//	{
	//		Entry<FeatureLayer, SimpleFeatureType> entry = getFeatureTypeAndLayer(name);
	//		return entry != null ? entry.getValue() : null;
	//	}

	public SimpleFeatureType getFeatureType(Name name)
	{
		List<Entry<FeatureLayer, SimpleFeatureType>> entries = getFeatureTypesAndLayer(name);
		SimpleFeatureType type;
		if (entries.size() > 1)
		{
			throw new IllegalStateException("More then one feature type for name '" + name + "' found!");
		}
		else if (entries.size() == 1)
		{
			type = entries.get(0).getValue();
		}
		else
		{
			type = null;
		}
		return type;
	}

	public List<SimpleFeatureType> getFeatureTypes(String namespaceURI)
	{
		List<SimpleFeatureType> types = null;
		FeatureLayer layer = getLayer(namespaceURI);
		if (layer != null)
		{
			types = layer.getFeatureTypes();
		}
		return types;
	}

	public List<Entry<FeatureLayer, SimpleFeatureType>> getFeatureTypesAndLayer()
	{
		List<Entry<FeatureLayer, SimpleFeatureType>> entries = new ArrayList<Map.Entry<FeatureLayer, SimpleFeatureType>>();
		for (FeatureLayer layer : getLayers())
		{
			for (SimpleFeatureType featureType : layer.getFeatureTypes())
			{
				entries
					.add(new AbstractMap.SimpleEntry<FeatureLayer, SimpleFeatureType>(layer, featureType));
			}
		}
		return entries;
	}

	public List<Entry<FeatureLayer, SimpleFeatureType>> getFeatureTypesAndLayer(Name name)
	{
		logger.debug("Search feature type with name '" + name + "'");
		String namespaceURI = name.getNamespaceURI();
		List<FeatureLayer> layers = new ArrayList<FeatureLayer>();
		if (namespaceURI == null || namespaceURI.length() == 0 || "*".equals(namespaceURI))
		{
			layers.addAll(getLayers());
		}
		else
		{
			layers.add(this.layers.get(namespaceURI));
		}

		List<Entry<FeatureLayer, SimpleFeatureType>> entries = new ArrayList<Map.Entry<FeatureLayer, SimpleFeatureType>>();
		if (layers != null)
		{
			for (FeatureLayer layer : getLayers())
			{
				SimpleFeatureType featureType = layer.getFeatureType(name.getLocalPart());
				if (featureType != null)
				{
					entries.add(new AbstractMap.SimpleEntry<FeatureLayer, SimpleFeatureType>(layer,
						featureType));
				}
			}
		}
		if (logger.isDebugEnabled())
		{
			logger.debug("Found layers: " + entries);
		}
		return entries;
	}

	public Map<String, String> getPrefixes()
	{
		return Collections.unmodifiableMap(this.prefixes);
	}

	public String getPrefix(String namespaceURI)
	{
		String prefix = null;
		for (Map.Entry<String, String> entry : this.prefixes.entrySet())
		{
			if (entry.getValue().equals(namespaceURI))
			{
				prefix = entry.getKey();
				break;
			}
		}
		return prefix;
	}

	/**
	 * 
	 * @param name
	 * @param filter
	 * @return
	 */
	public List<SimpleFeature> searchFeatures(Name name, Filter filter)
	{
		logger.debug("Searching features for type name '" + name + "' with filter '" + filter + "'");

		List<SimpleFeature> features = new ArrayList<SimpleFeature>();
		FeatureLayer layer = getLayer(name.getNamespaceURI());
		if (layer != null)
		{
			features.addAll(searchFeatures(layer, name.getLocalPart(), filter));
		}

		logger.debug("end: Features found: " + features.size());
		return features;
	}

	protected List<SimpleFeature> searchFeatures(FeatureLayer layer, String typeName, Filter filter)
	{
		return layer.searchFeatures(typeName, filter);
	}

	/**
	 * 
	 * @param feature
	 * @return
	 */
	public String insertFeature(SimpleFeature feature)
	{
		if (logger.isDebugEnabled())
		{
			logger.debug("Inserting new feature: " + feature);
		}

		FeatureLayer layer = getLayer(feature.getName().getNamespaceURI());

		String result;
		try
		{
			writeLock.lock();
			result = insertFeature(layer, feature);
		}
		finally
		{
			writeLock.unlock();
		}
		return result;
	}

	protected String insertFeature(FeatureLayer layer, SimpleFeature feature)
	{
		String[] ary = layer.insertFeatures(feature);
		String result = null;
		if (ary != null && ary.length > 0)
		{
			result = ary[0];
		}

		if (logger.isDebugEnabled())
		{
			logger.debug("Inserted new feature '" + feature + "' to layer '" + layer + "' with result '"
				+ result + "'");
		}
		return result;
	}

	public int updateFeatures(Name name, Filter filter, Map<String, Object> attributes)
	{
		int updateCount;
		FeatureLayer layer = getLayer(name.getNamespaceURI());
		try
		{
			writeLock.lock();
			updateCount = updateFeatures(layer, name.getLocalPart(), filter, attributes);
		}
		finally
		{
			writeLock.unlock();
		}

		return updateCount;
	}

	protected int updateFeatures(FeatureLayer layer, String typeName, Filter filter,
		Map<String, Object> attributes)
	{
		return layer.updateFeatures(typeName, filter, attributes);
	}

	public int deleteFeatures(Name name, Filter filter)
	{
		int deleteCount;

		FeatureLayer layer = getLayer(name.getNamespaceURI());
		try
		{
			writeLock.lock();
			deleteCount = deleteFeatures(layer, name.getLocalPart(), filter);
		}
		finally
		{
			writeLock.unlock();
		}

		return deleteCount;
	}

	protected int deleteFeatures(FeatureLayer layer, String typeName, Filter filter)
	{
		return layer.deleteFeatures(typeName, filter);
	}

	public XSDSchema[] describeFeatureTypes(Version version, URL baseURL) throws IOException
	{
		return describeFeatureTypes(version, baseURL, (Name[]) null);
	}

	/**
	 * 
	 * @param version Use this version for encoding
	 * @param baseURL The base url where the wfs schemas can be found
	 * @param typeNames Encode this types
	 * @return The encodes schemas (One schema for each namespace)
	 */
	public XSDSchema[] describeFeatureTypes(Version version, URL baseURL, Name... names)
		throws IOException
	{
		GMLEncoder encode = new GMLEncoder(version);
		encode.setBaseURL(baseURL);

		Map<String, List<SimpleFeatureType>> types = new HashMap<String, List<SimpleFeatureType>>();
		for (Entry<FeatureLayer, SimpleFeatureType> entry : getFeatureTypesAndLayer())
		{
			SimpleFeatureType type = entry.getValue();
			String namespaceURI = entry.getKey().getNamespaceURI();

			if (names != null && Arrays.binarySearch(names, type.getName()) == -1)
			{
				continue;
			}

			List<SimpleFeatureType> list = types.get(namespaceURI);
			if (list == null)
			{
				list = new ArrayList<SimpleFeatureType>();
				types.put(namespaceURI, list);
			}
			list.add(type);
		}

		// namespaces which were defined in the layer (key = prefix, value = namespaceURI)
		Map<String, String> prefixes = getPrefixes();
		for (Map.Entry<String, String> entry : prefixes.entrySet())
		{
			encode.addNamespace(entry.getKey(), entry.getValue());
		}

		XSDSchema[] schemas = new XSDSchema[types.size()];
		int i = 0;
		for (List<SimpleFeatureType> list : types.values())
		{
			schemas[i++] = encode.xsd(list.toArray(new SimpleFeatureType[list.size()]));
		}
		return schemas;
	}
}
