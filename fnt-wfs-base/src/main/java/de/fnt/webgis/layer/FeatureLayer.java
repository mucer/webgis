package de.fnt.webgis.layer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.filter.Filter;

/** 
 * Holds a Layer
 */
public abstract class FeatureLayer
{
	protected static final Logger logger = Logger.getLogger(FeatureLayer.class);

	protected String namespace;

	protected List<SimpleFeatureType> featureTypes = new ArrayList<SimpleFeatureType>();

	protected FeatureLayer(String namespace)
	{
		this.namespace = namespace;
	}

	public String getNamespaceURI()
	{
		return namespace;
	}

	public boolean containsFeatureType(String featureTypeName)
	{
		boolean found = false;
		for (SimpleFeatureType type : this.featureTypes)
		{
			if (type.getTypeName().equals(featureTypeName))
			{
				found = true;
				break;
			}
		}

		if (logger.isDebugEnabled())
		{
			logger.debug("'" + this + "' contains '" + featureTypeName + "': " + found);
		}

		return found;
	}

	public boolean containsFeatureType(SimpleFeatureType featureType)
	{
		return containsFeatureType(featureType.getTypeName());
	}

	/**
	 * Only add new features during build process. Otherwise some check routines in
	 * the layer manager won't work.
	 * 
	 * @param featureType
	 * @return
	 */
	protected boolean addFeatureType(SimpleFeatureType featureType)
	{
		boolean added = false;
		if (!containsFeatureType(featureType))
		{
			this.featureTypes.add(featureType);
			added = true;
		}

		if (logger.isDebugEnabled())
		{
			logger.debug("Added '" + featureType + "' to '" + this + "': result -> " + added);
		}

		return added;
	}

	public List<SimpleFeatureType> getFeatureTypes()
	{
		return Collections.unmodifiableList(this.featureTypes);
	}

	@Override
	public String toString()
	{
		StringBuffer buf = new StringBuffer("[Layer ").append(this.namespace);
		for (int i = 0; i < this.featureTypes.size(); i++)
		{
			if (i > 0)
			{
				buf.append(", ");
			}
			else
			{
				buf.append(": ");
			}
			buf.append(featureTypes.get(i).getTypeName());
		}
		buf.append("]");

		return buf.toString();
	}

	/**
	 * Searches a feature type for the given type name.
	 * 
	 * @param typeName Search this type name.
	 * @return The found feature type or null.
	 */
	public SimpleFeatureType getFeatureType(String typeName)
	{
		SimpleFeatureType returnType = null;
		for (SimpleFeatureType featureType : this.featureTypes)
		{
			if (featureType.getTypeName().equals(typeName))
			{
				returnType = featureType;
			}
		}

		if (logger.isDebugEnabled())
		{
			logger.debug("Getting feature type for name '" + typeName + "': " + returnType);
		}

		return returnType;
	}

	public abstract List<SimpleFeature> searchFeatures(String typeName, Filter filter);

	public abstract String[] insertFeatures(SimpleFeature... features);

	/**
	 * 
	 * @param type Only update features with this type.
	 * @param filter Use this filter to restrict the affected objects.
	 * @param properties The new values.
	 * @return The number of updated objects.
	 */
	public abstract int updateFeatures(String typeName, Filter filter, Map<String, Object> properties);

	/**
	 * 
	 * @param type Only delete features with this type.
	 * @param filter Use this filter to restrict the affected objects.
	 * @return The number of deleted objects.
	 */
	public abstract int deleteFeatures(String typeName, Filter filter);
}
