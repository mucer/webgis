package de.fnt.webgis.layer;

public interface FeatureLayerManagerBuilder
{
	public FeatureLayerManager build();
}
