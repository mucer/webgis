package de.fnt.webgis.ows.wfs;

import javax.xml.namespace.QName;

import org.geotools.xml.AbstractSimpleBinding;
import org.geotools.xml.InstanceComponent;
import org.opengis.feature.simple.SimpleFeature;

import de.fnt.webgis.di.feature.DynamicFeatureBuilder;
import de.fnt.webgis.di.feature.DynamicFeatureType;

class DynamicFeatureTypeBinding extends AbstractSimpleBinding
{
	final DynamicFeatureType type;

	public DynamicFeatureTypeBinding(DynamicFeatureType type)
	{
		this.type = type;
	}

	public QName getTarget()
	{
		QName name = new QName(type.getName().getNamespaceURI(), type.getName().getLocalPart());
		return name;
	}

	public Class<?> getType()
	{
		return type.getClass();
	}

	@Override
	public Object parse(InstanceComponent instance, Object value) throws Exception
	{
		if (value instanceof SimpleFeature)
		{
			SimpleFeature feature = (SimpleFeature) value;
			if (feature.getType() != this.type)
			{
				DynamicFeatureBuilder builder = new DynamicFeatureBuilder(this.type);
				value = builder.parseFeature(feature);
			}
		}
		return value;
	}
}
