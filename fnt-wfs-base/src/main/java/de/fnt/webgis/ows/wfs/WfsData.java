package de.fnt.webgis.ows.wfs;

import org.geotools.GMLExtended.Version;

import de.fnt.webgis.ows.wfs.WfsConstants.Request;

public class WfsData
{
	Version version = null;

	String service = WfsConstants.SERVICE;

	Request requestType = null;

	Object requestData = null;

	public Request getRequestType()
	{
		return requestType;
	}

	public Object getRequestData()
	{
		return requestData;
	}

	public Version getVersion()
	{
		return version;
	}

	public String getService()
	{
		return service;
	}
}
