package de.fnt.webgis.ows.wfs;

public interface WfsConstants
{
	public static final String SERVICE = "wfs";

	public enum Request
	{
		GetCapabilities, DescribeFeatureType, GetFeature, GetGmlObject, LockFeature, Transaction
	};

	public enum HttpMethod
	{
		OPTIONS, GET, POST
	};

	public static final String ENCODING_XML = "text/xml";

	public static final String ENCODING_KVP = "application/x-www-form-urlencoded";
	/*
		/ ** This value is kept for backward compatibility. Must be generated that validates against GML2 schema * /
		public static final String OUTPUT_FORMAT_XMLSCHEMA = "XMLSCHEMA";

		/ ** This value is kept for backward compatibility. Must be generated that validates against GML2 schema * /
		public static final String OUTPUT_FORMAT_GML2 = "GML2";

		public static final String OUTPUT_FORMAT_SUBTYPE_GML2 = "text/xml; subtype=gml/2.1.2";

		public static final String OUTPUT_FORMAT_SUBTYPE_GML3 = "text/xml; subtype=gml/3.1.1";*/

}
