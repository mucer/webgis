package de.fnt.webgis.ows.wfs.xml;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.eclipse.xsd.util.XSDResourceImpl;
import org.geotools.GMLExtended;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import de.fnt.webgis.ows.wfs.WfsException;

public class ExceptionEncoder
{
	private final WfsException exception;

	private Charset encoding = Charset.forName("UTF-8");

	public ExceptionEncoder(WfsException exception)
	{
		this.exception = exception;
	}

	public WfsException getException()
	{
		return exception;
	}

	public GMLExtended.Version getVersion()
	{
		return this.exception.getVersion();
	}

	public void setEncoding(Charset encoding)
	{
		this.encoding = encoding;
	}

	public Charset getEncoding()
	{
		return this.encoding;
	}

	public void encode(OutputStream out) throws IOException
	{
		/*Ows10Factory factory = new Ows10FactoryImpl();
		ExceptionReportType erElem = factory.createExceptionReportType();
		erElem.setVersion(getVersion().getVersionStr());

		ExceptionType eElem = factory.createExceptionType();
		eElem.setExceptionCode(this.exception.getCode().toString());
		eElem.setLocator(this.exception.getLocator());
		eElem.getExceptionText().add(this.exception.getMessage());

		Encoder encoder = new Encoder(getVersion().getConfiguration());
		encoder.setEncoding(getEncoding());
		encoder.setIndenting(getIndenting());
		encoder.encode(erElem, new QName("http://www.opengis.net/ogc", "ExceptionReport"), out);*/
		Document doc;
		try
		{
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			doc = db.newDocument();
		}
		catch (ParserConfigurationException e)
		{
			throw new IOException(e);
		}

		Element rootElem = doc.createElementNS("http://www.opengis.net/ows", "ExceptionReport");

		// TODO local schema
		rootElem.setAttributeNS("http://www.w3.org/2001/XMLSchema-instance", "xsi:schemaLocation",
			"http://www.opengis.net/ows/1.1 http://schemas.opengis.net/ows/1.1.0/owsAll.xsd");
		rootElem.setAttribute("version", getVersion().getVersionStr());

		Element exceptionElem = doc.createElement("Exception");
		// TODO attribute == exceptionCode in version 2.0.0?
		exceptionElem.setAttribute("code", exception.getCode().toString());
		exceptionElem.setAttribute("locator", exception.getLocator());

		Element exceptionTextElem = doc.createElement("ExceptionText");
		exceptionTextElem.setTextContent(exception.getMessage());

		exceptionElem.appendChild(exceptionTextElem);
		rootElem.appendChild(exceptionElem);

		XSDResourceImpl.serialize(out, rootElem, encoding.name());
	}

}
