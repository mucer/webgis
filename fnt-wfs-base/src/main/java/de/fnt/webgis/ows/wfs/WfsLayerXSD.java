package de.fnt.webgis.ows.wfs;

import java.io.IOException;
import java.net.URL;
import java.util.List;

import org.eclipse.xsd.XSDSchema;
import org.geotools.GMLExtended;
import org.geotools.xml.XSD;
import org.opengis.feature.simple.SimpleFeatureType;

import de.fnt.webgis.layer.FeatureLayer;
import de.fnt.webgis.ows.wfs.xml.GMLEncoder;

public class WfsLayerXSD extends XSD
{
	XSDSchema schema;

	protected GMLExtended.Version version;

	protected URL baseURL;

	protected FeatureLayer layer;

	public WfsLayerXSD(GMLExtended.Version version, URL baseURL, FeatureLayer layer)
	{
		this.version = version;
		this.baseURL = baseURL;
		this.layer = layer;
	}

	@Override
	public String getNamespaceURI()
	{
		return this.layer.getNamespaceURI();
	}

	@Override
	public String getSchemaLocation()
	{
		// TODO
		return getClass().getResource(".").toExternalForm();
	}

	@Override
	protected XSDSchema buildSchema() throws IOException
	{
		if (this.schema == null)
		{
			GMLEncoder encode = new GMLEncoder(this.version);
			encode.setBaseURL(this.baseURL);

			List<SimpleFeatureType> types = layer.getFeatureTypes();
			this.schema = encode.xsd(types.toArray(new SimpleFeatureType[types.size()]));
		}
		return this.schema;
	}
}
