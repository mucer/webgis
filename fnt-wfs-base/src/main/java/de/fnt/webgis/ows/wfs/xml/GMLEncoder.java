package de.fnt.webgis.ows.wfs.xml;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.namespace.QName;

import net.opengis.wfs.FeatureCollectionType;

import org.geotools.GMLExtended;
import org.geotools.wfs.WFS;
import org.geotools.xml.Encoder;
import org.opengis.feature.simple.SimpleFeatureType;

public class GMLEncoder extends GMLExtended
{
	public GMLEncoder(Version version)
	{
		super(version);
	}

	protected void updateNamespaces()
	{
	}

	protected Map<String, List<SimpleFeatureType>> sortByNamespaceURI(SimpleFeatureType[] types)
	{
		Map<String, List<SimpleFeatureType>> map = new HashMap<String, List<SimpleFeatureType>>();
		for (SimpleFeatureType type : types)
		{
			List<SimpleFeatureType> list = map.get(type.getName().getNamespaceURI());
			if (list == null)
			{
				list = new ArrayList<SimpleFeatureType>();
				map.put(type.getName().getNamespaceURI(), list);
			}
			list.add(type);
		}
		return map;
	}

	/**
	 * 
	 */
	public void encode(OutputStream out, FeatureCollectionType collection) throws IOException
	{
		Encoder e = new Encoder(version.getConfiguration());

		e.setIndenting(getIndenting());

		QName name;
		if (version == Version.WFS2_0)
		{
			name = org.geotools.wfs.v2_0.WFS.FeatureCollection;
		}
		else if (version == Version.WFS1_1)
		{
			name = WFS.FeatureCollection;
		}
		else
		{
			name = WFS.FeatureCollection;
		}

		// add namespaces
		updateNamespaces();
		for (Map.Entry<String, String> entry : this.namespaces.entrySet())
		{
			e.getNamespaces().declarePrefix(entry.getKey(), entry.getValue());
		}

		e.encode(collection, name, out);
	}
}
