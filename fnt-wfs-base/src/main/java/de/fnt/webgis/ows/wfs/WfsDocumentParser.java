package de.fnt.webgis.ows.wfs;

import static de.fnt.webgis.ows.wfs.WfsConstants.SERVICE;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.xml.namespace.QName;

import net.opengis.wfs.GetFeatureType;
import net.opengis.wfs.QueryType;
import net.opengis.wfs.TransactionType;
import net.opengis.wfs.WfsFactory;

import org.apache.log4j.Logger;
import org.geotools.GMLExtended;
import org.geotools.GMLExtended.Version;
import org.geotools.xml.Configuration;
import org.geotools.xml.Parser;

import de.fnt.webgis.layer.FeatureLayerManager;
import de.fnt.webgis.ows.wfs.WfsConstants.Request;
import de.fnt.webgis.ows.wfs.WfsException.Code;

public class WfsDocumentParser
{
	protected static final Logger logger = Logger.getLogger(WfsDocumentParser.class);

	public static final String PARAMETER_REQUEST = "request";

	public static final String PARAMETER_SERVICE = "service";

	public static final String PARAMETER_VERSION = "version";

	FeatureLayerManager layerManager = null;

	URL baseURL;

	public WfsDocumentParser(URL baseURL)
	{
		this.baseURL = baseURL;
	}

	/**
	 * Use this manager to resolve features
	 * 
	 * @param layerManager
	 */
	public void setFeatureLayerManager(FeatureLayerManager layerManager)
	{
		this.layerManager = layerManager;
	}

	public WfsData parse(InputStream in) throws IOException
	{
		logger.debug("Start parsing new InputStream");
		WfsData data = new WfsData();

		// get version of request
		byte[] bytes;
		{
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			byte[] buf = new byte[4086];
			int len;
			while ((len = in.read(buf)) > 0)
			{
				out.write(buf, 0, len);
			}
			out.flush();
			bytes = out.toByteArray();
		}

		data.version = readVersion(bytes);

		if (logger.isDebugEnabled())
		{
			logger.debug("Read version from input stream: " + data.version);
		}

		Configuration config = new WfsLayerManagerConfiguration(data.version, this.baseURL,
			this.layerManager);
		Parser p = new Parser(config);

		try
		{
			data.requestData = p.parse(new ByteArrayInputStream(bytes));
		}
		catch (Exception e)
		{
			throw new RuntimeException("Could not parse parameter xml: " + e.getMessage(), e);
		}

		if (data.requestData instanceof GetFeatureType)
		{
			GetFeatureType type = (GetFeatureType) data.requestData;
			data.requestType = Request.GetFeature;
			data.service = type.getService();
		}
		else if (data.requestData instanceof TransactionType)
		{
			TransactionType type = (TransactionType) data.requestData;
			data.requestType = Request.Transaction;
			data.service = type.getService();
		}
		else
		{
			throw new IllegalArgumentException("Unsupported request type: "
				+ data.requestData.getClass().getCanonicalName());
		}

		return data;
	}

	private Version readVersion(byte[] bytes) throws IOException
	{
		StringBuffer key = null;
		StringBuffer value = null;

		for (int i = 0; i < bytes.length; i++)
		{
			char c = (char) bytes[i];
			if (value != null)
			{
				if (c == '"')
				{
					if (value.length() == 0)
					{
						continue;
					}
					else
					{
						break;
					}
				}
				value.append(c);
			}
			else if (c == ' ')
			{
				key = new StringBuffer();
			}
			else if (c == '=' && key != null && "version".equalsIgnoreCase(key.toString()))
			{
				value = new StringBuffer();
			}
			else if (key != null)
			{
				key.append(c);
			}
		}

		if (value == null)
		{
			throw new WfsException(null, Code.InvalidParameterValue, "readVersion",
				"No version number given!");
		}

		Version version;
		try
		{
			version = Version.valueOf(value.toString());
		}
		catch (Exception e)
		{
			throw new WfsException(null, Code.InvalidParameterValue, "readVersion",
				"Unsupported version number '" + value + "' given!");
		}
		return version;
	}

	/**
	 * Spec 14.4 Common parameters
	 *  VERSION -> required (default "1.1.0")
	 *  SERVICE -> required (default "WFS")
	 *  REQUEST -> required, Name of WFS request
	 *  NAMESPACE -> optional, The format must be "xmlns(prefix=escaped_url)"
	 */
	public WfsData parse(Map<String, Object> parameters)
	{
		String versionStr = null;
		String requestStr = null;

		WfsData data = new WfsData();
		Map<String, String> values = normalize(parameters);

		for (Map.Entry<String, String> entry : values.entrySet())
		{
			String name = entry.getKey();
			String value = entry.getValue();

			if (PARAMETER_VERSION.equalsIgnoreCase(name))
			{
				versionStr = value;
			}
			else if (PARAMETER_SERVICE.equalsIgnoreCase(name))
			{
				data.service = value;
			}
			else if (PARAMETER_REQUEST.equalsIgnoreCase(name))
			{
				requestStr = value;
			}
		}

		// try to get version first -> correct exception report
		checkRequiredParameter(data.version, PARAMETER_VERSION, versionStr);
		try
		{
			data.version = Version.valueOf(versionStr);
		}
		catch (Exception e)
		{
			throw new WfsException(null, WfsException.Code.InvalidParameterValue, "Unsupported version '"
				+ versionStr + "'");
		}

		// only "wfs" is supported
		if (!WfsConstants.SERVICE.equals(data.service))
		{
			logger.warn("Got service '" + data.service + "' but expected '" + SERVICE + "'");
			//			throw new WfsException(null, WfsException.Code.InvalidParameterValue, "Service '" + service
			//				+ "' not supported!");
			data.service = WfsConstants.SERVICE;
		}

		// compare to capabilities
		checkRequiredParameter(data.version, PARAMETER_REQUEST, requestStr);
		try
		{
			data.requestType = Request.valueOf(requestStr);
		}
		catch (Exception e)
		{
			throw new WfsException(null, WfsException.Code.InvalidParameterValue, "Unsupported request '"
				+ requestStr + "'");
		}

		switch (data.requestType) {
		case GetFeature:
			parseGetFeature(data, values);
			break;
		default:
		}

		return data;
	}

	/**
	 * Spec 14.7.2 DescriptFeatureType operation
	 *  REQUEST = DescribeFeatureType
	 *  TYPENAME -> optional, A comma separated list of feature types to describe.
	 *  OUTPUTFORMAT -> optional (default "ext/xml; subtype=gml/3.1.1")
	 *  e.g.: http://www.someserver.com/wfs.cgi?
	 * 		SERVICE=WFS&
	 * 		VERSION=1.1.0&
	 * 		REQUEST=DescribeFeatureType&
	 * 		TYPENAME=TreesA_1M,BuiltUpA_1M
	 */
	//	private void parseDescribeFeatureType()
	//	{
	//	}

	private Map<String, String> normalize(Map<String, Object> parameters)
	{
		Map<String, String> values = new HashMap<String, String>();
		for (Map.Entry<String, Object> entry : parameters.entrySet())
		{

			String name = entry.getKey().toLowerCase();
			Object obj = entry.getValue();
			String value = null;
			if (obj instanceof Object[])
			{
				if (((Object[]) obj).length > 0)
				{
					value = ((Object[]) obj)[0].toString();
				}
			}
			else if (obj != null)
			{
				value = obj.toString();
			}

			values.put(name.toLowerCase(), value);
		}
		return values;
	}

	/**
	 * Typename is required
	 * @param data 
	 * 
	 * @param parameters
	 */
	@SuppressWarnings("unchecked")
	protected void parseGetFeature(WfsData data, Map<String, String> parameters)
	{
		String typeName = parameters.get("typename");
		checkRequiredParameter(data.version, "typeName", typeName);

		WfsFactory factory = WfsFactory.eINSTANCE;
		GetFeatureType requestData = factory.createGetFeatureType();

		String[] types = typeName.split(",");
		for (String type : types)
		{
			QueryType query = factory.createQueryType();
			query.setTypeName(Arrays.asList(new QName[] { new QName(type) }));
			requestData.getQuery().add(query);
		}

		data.requestData = requestData;
	}

	/**
	 * Checks whether the parameter is given. If not a WfsException will be thrown
	 * 
	 * @param name
	 * @param value
	 */
	protected void checkRequiredParameter(GMLExtended.Version version, String name, String value)
	{
		if (value == null || value.length() == 0)
		{
			throw new WfsException(version, WfsException.Code.MissingParameterValue, "Parameter '" + name
				+ "' is required!");
		}
	}

	public URL getBaseURL()
	{
		return this.baseURL;
	}
}
