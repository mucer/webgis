package de.fnt.webgis.ows.wfs;

import java.net.URL;
import java.util.Map;

import javax.xml.namespace.QName;

import org.geotools.GMLExtended;
import org.geotools.xml.Configuration;
import org.opengis.feature.simple.SimpleFeatureType;

import de.fnt.webgis.di.feature.DynamicFeatureType;
import de.fnt.webgis.layer.FeatureLayer;

public class WfsLayerConfiguration extends Configuration
{
	protected FeatureLayer layer;

	public WfsLayerConfiguration(GMLExtended.Version version, URL baseUrl, FeatureLayer layer)
	{
		super(new WfsLayerXSD(version, baseUrl, layer));
		this.layer = layer;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	protected void registerBindings(Map/*<QName, ? extends Binding>*/bindings)
	{
		for (SimpleFeatureType type : this.layer.getFeatureTypes())
		{
			if (type instanceof DynamicFeatureType)
			{
				QName name = new QName(type.getName().getNamespaceURI(), type.getName().getLocalPart());
				DynamicFeatureTypeBinding binding = new DynamicFeatureTypeBinding(
					(DynamicFeatureType) type);
				bindings.put(name, binding);
			}
		}
		super.registerBindings(bindings);
	}
}
