package de.fnt.webgis.ows.wfs;

import java.io.IOException;
import java.net.URL;

import org.geotools.GMLExtended;
import org.geotools.xml.XMLConfiguration;
import org.picocontainer.MutablePicoContainer;

import de.fnt.webgis.layer.FeatureLayer;
import de.fnt.webgis.layer.FeatureLayerManager;

public class WfsLayerManagerConfiguration extends XMLConfiguration
{
	final FeatureLayerManager manager;

	protected WfsLayerManagerConfiguration(GMLExtended.Version version, URL baseUrl,
		FeatureLayerManager manager) throws IOException
	{
		super();
		this.manager = manager;

		for (FeatureLayer layer : manager.getLayers())
		{
			addDependency(new WfsLayerConfiguration(version, baseUrl, layer));
		}
		
		addDependency(version.getConfiguration());
	}

	@Override
	protected void configureContext(MutablePicoContainer container)
	{
		super.configureContext(container);
		container.registerComponentInstance(this.manager);
	}
}
