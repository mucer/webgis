package de.fnt.webgis.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import de.fnt.webgis.context.ContextHolder;
import de.fnt.webgis.context.Scope;

/**
 * If a class, method or field has this annotation, it will be added to the context.
 * See {@link ContextHolder#registerFactory(Object)} and {@link ContextHolder#registerPackage(String)}
 * for more details.
 * Methods can have parameters which will also be resolved per dependency injection.
 */
@Target({ ElementType.FIELD, ElementType.METHOD, ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface Context
{
	/** Use this name for the context object. If not given it will be determined automatically */
	String value() default "";

	/** If this parameter is false, the value will not be cached */
	boolean enableCache() default true;

	/** The scope of the context object. Only useful with catching or static objects */
	Scope scope() default Scope.REQUEST;

	/** Special scope for dynamic content. The generated content will be added to this scope	 */
	Scope instanceScope() default Scope.REQUEST;
}
