package de.fnt.webgis.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marks a field or method as an attribute of a feature.
 * Also allows to define several settings of the attribute.
 */
@Target({ ElementType.FIELD, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
public @interface Attribute
{
	/** The name of the attribute, as defaut the name of the method is used */
	String name() default "";

	/** Whether the attribute is nillable (default false) */
	boolean nillable() default false;

	/** The number of elements with must occure at least */
	int min() default 1;

	/** The maximum number of elements with can occure */
	int max() default 1;

	/** Use this attribute as unique id for the feature */
	boolean isUnique() default false;
}
