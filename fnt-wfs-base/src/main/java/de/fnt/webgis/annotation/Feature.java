package de.fnt.webgis.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * A class with this annotation will be added as feature type to a feature layer if the layer references this class.
 */
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface Feature
{
	/** The name of this feature type */
	String value();
}
