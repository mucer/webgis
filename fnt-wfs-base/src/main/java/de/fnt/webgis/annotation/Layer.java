package de.fnt.webgis.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * A class with this annotation will be handeled as webgis layer.
 */
@Target({ java.lang.annotation.ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface Layer
{
	/** The namespace uri of this layer */
	String value();
}
