package de.fnt.webgis.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.opengis.geometry.BoundingBox;

/**
 * Marks a method in a {@link Layer} as an action method. Methods with annotations
 * will be used with higher priority. Otherwise the compination of method name and/or
 * the return type and parameter types:
 * <ul>
 * 		<li>INSERT: method should return a string with the generated unique id. (name = add..., insert...)</li>
 * 		<li>DELETE: (name = delete...)</li>
 * 		<li>UPDATE: (name = update...)</li>
 * 		<li>SEARCH: always create a method which accepts a {@link BoundingBox} parameter. (name = search..., get...)</li>
 * </ul>
 */
@Target({ java.lang.annotation.ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
public @interface LayerAction
{
	/** The possible action types */
	enum ActionType
	{
		INSERT, DELETE, UPDATE, SEARCH
	};

	/** The type of this layer action, see constants */
	ActionType value();

	/** The name of the feature type which is handled by this method. 
	    If empty, the feature type will be guessed by the parameter types or return types */
	String typeName() default "";
}
