package de.fnt.webgis.annotation;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.apache.log4j.Logger;

/**
 * This class provides some helper method to deal with annotations
 */
public class AnnotationUtil
{
	protected static final Logger logger = Logger.getLogger(AnnotationUtil.class);

	/**
	 * Scans all classes accessible from the context class loader which belong to the given package. Also
	 * check sub packages.
	 * 
	 * @param packageName The name of the package (e.g. "my.package")
	 * @return List of found classes
	 */
	public static List<Class<?>> getClasses(String packageName)
	{
		return getClasses(packageName, null, true);
	}

	/**
	 * Scans all classes accessible from the context class loader which belong to the given package. Also
	 * check sub packages.
	 * 
	 * @param packageName The name of the package (e.g. "my.package")
	 * @param annotation Only return classes with this annotation.
	 * @return List of found classes
	 */
	public static List<Class<?>> getClasses(String packageName, Class<? extends Annotation> annotation)
	{
		return getClasses(packageName, annotation, true);
	}

	/**
	 * Scans all classes accessible from the context class loader which belong to the given package.
	 * 
	 * @param packageName The name of the package (e.g. "my.package")
	 * @param deep If this parameter is "true", sub packages will be returned, too.
	 * @return List of found classes
	 */
	public static List<Class<?>> getClasses(String packageName, boolean deep)
	{
		return getClasses(packageName, null, deep);
	}

	/**
	 * Scans all classes accessible from the context class loader which belong to the given package.
	 *
	 * @param packageName The name of the package (e.g. "my.package")
	 * @param annotation Only return classes with this annotation.
	 * @param deep If this parameter is "true", sub packages will be returned, too.
	 * @return List of found classes
	 */
	public static List<Class<?>> getClasses(String packageName,
		Class<? extends Annotation> annotation, boolean deep)
	{
		logger.debug("Getting classes for packageName '" + packageName + "'.");

		while (packageName.endsWith("."))
		{
			packageName = packageName.substring(0, packageName.length() - 1);
		}

		List<Class<?>> classes = new ArrayList<Class<?>>();
		List<URL> resources = getResources(packageName);

		if (resources.isEmpty())
		{
			logger.warn("No resources for package '" + packageName + "' and annotation '" + annotation
				+ "' found!");
		}

		for (URL resource : resources)
		{
			File directory;
			try
			{
				directory = new File(resource.toURI());
			}
			catch (Exception e)
			{
				logger.warn("Error getting directory for resource '" + resource);
				directory = null;
			}

			if (directory != null && directory.exists())
			{
				classes.addAll(parseDirectory(directory, packageName, annotation, deep));
			}
			else
			{
				try
				{
					int len = packageName.length();
					String jarPath = resource.getFile().replaceFirst("[.]jar[!].*", ".jar").replaceFirst(
						"file:", "");
					logger.debug("Loading jar file: " + jarPath);

					JarFile jarFile = new JarFile(jarPath);
					Enumeration<JarEntry> entries = jarFile.entries();
					while (entries.hasMoreElements())
					{
						JarEntry entry = entries.nextElement();
						String entryName = entry.getName();
						if (entryName.endsWith(".class"))
						{
							String className = entryName.replace('/', '.').replace('\\', '.').substring(0,
								entryName.length() - 6);
							if (!className.startsWith(packageName)
								|| (!deep && className.indexOf('.', len + 1) > -1))
							{
								continue;
							}

							logger.debug("Found class: " + className);
							try
							{
								Class<?> cls = Class.forName(className);
								if (annotation == null || isAnnotationPresent(cls, annotation))
								{
									classes.add(cls);
								}
							}
							catch (ClassNotFoundException e)
							{
								throw new RuntimeException("Couldn't load class " + className, e);
							}
						}
					}
				}
				catch (IOException e)
				{
					throw new RuntimeException(packageName + " (" + directory
						+ ") does not appear to be a valid package", e);
				}
			}
		}
		return classes;
	}

	/**
	 * Searches the given directory for class files.
	 *  
	 * @param directory Search this directory.
	 * @param packageName The name of the package (e.g. "my.package")
	 * @param annotation Only return classes with this annotation.
	 * @param deep If this parameter is "true", sub packages will be returned, too.
	 * @return List of found classes
	 */
	private static List<Class<?>> parseDirectory(File directory, String packageName,
		Class<? extends Annotation> annotation, boolean deep)
	{
		logger.debug("Searching classes in directory '" + directory + "' (package '" + packageName
			+ "')");

		List<Class<?>> classes = new ArrayList<Class<?>>();
		// Get the list of the files contained in the package
		File[] files = directory.listFiles();
		for (File file : files)
		{
			String name = file.getName();

			// we are only interested in .class files
			if (file.isDirectory())
			{
				if (deep && name.indexOf('.') == -1)
				{
					classes.addAll(parseDirectory(file, packageName + "." + name, annotation, deep));
				}
			}
			else if (file.getName().endsWith(".class"))
			{
				// removes the .class extension
				String className = packageName + "." + name.substring(0, name.length() - 6);
				logger.debug("Found class: " + className);
				try
				{
					Class<?> cls = Class.forName(className);
					if (annotation == null || isAnnotationPresent(cls, annotation))
					{
						classes.add(cls);
					}
				}
				catch (ClassNotFoundException e)
				{
					throw new RuntimeException("Couldn't load class " + className, e);
				}
			}
		}

		return classes;
	}

	/**
	 * Tries to get resources from the system class loader and the context class loader
	 * 
	 * @param packageName Get all resource for this package name.
	 * @return A list with found resources.
	 */
	private static List<URL> getResources(String packageName)
	{
		// Get a File object for the package
		String relPath = packageName.replace('.', '/');

		List<URL> resources = new ArrayList<URL>();
		try
		{
			Enumeration<URL> urls = ClassLoader.getSystemResources(relPath);
			while (urls != null && urls.hasMoreElements())
			{
				resources.add(urls.nextElement());
			}
		}
		catch (IOException e)
		{
			logger.error("Error adding resources for package '" + packageName
				+ "' via system classloader", e);
		}

		try
		{
			Enumeration<URL> urls = Thread.currentThread().getContextClassLoader().getResources(relPath);
			while (urls != null && urls.hasMoreElements())
			{
				URL url = urls.nextElement();
				if (!resources.contains(url))
				{
					resources.add(url);
				}
			}
		}
		catch (IOException e)
		{
			logger.error("Error adding resources for package '" + packageName
				+ "' via context classloader", e);
		}

		return resources;
	}

	/**
	 * Returns all methods where the return type is flagged by the given annotation
	 * 
	 * @param cls Search this class
	 * @param annotation Search for this annotation
	 * @return All found methods
	 */
	public static Method[] getMethodsByReturnType(Class<?> cls, Class<? extends Annotation> annotation)
	{
		List<Method> methods = new ArrayList<Method>();
		for (Method method : cls.getMethods())
		{
			if ((method.getModifiers() & Modifier.PUBLIC) == 0)
			{
				continue;
			}

			if (isAnnotationPresent(method, annotation))
			{
				methods.add(method);
			}
			else
			{
				Class<?> returnType = method.getReturnType();
				if (returnType.isArray())
					returnType = returnType.getComponentType();

				if (isAnnotationPresent(returnType, annotation))
				{
					methods.add(method);
				}
			}
		}
		return methods.toArray(new Method[methods.size()]);
	}

	/**
	 * Checks whether the getAnnotation method returns a value for the given 
	 * parameters.
	 * 
	 * @return Returns true if the result of the get annotation method is not null
	 * @see #getAnnotation(AnnotatedElement, Class)
	 */
	public static boolean isAnnotationPresent(Class<?> cls, Class<? extends Annotation> annoCls)
	{
		return getAnnotation(cls, annoCls) != null;
	}

	/**
	 * Checks whether the getAnnotation method returns a value for the given 
	 * parameters.
	 * 
	 * @return Returns true if the result of the get annotation method is not null
	 * @see #getAnnotation(AnnotatedElement, Class)
	 */
	public static boolean isAnnotationPresent(Method method, Class<? extends Annotation> annoCls)
	{
		return getAnnotation(method, annoCls) != null;
	}

	/**
	 * Gets all methods with the given annotation from the given class.
	 * 
	 * @param source Search this class
	 * @param annotation Search for this annotation
	 * @return The found methods
	 */
	public static Method[] getMethods(Class<?> source, Class<? extends Annotation> annotation)
	{
		Method[] all = source.getMethods();
		List<Method> found = new ArrayList<Method>();
		for (Method method : all)
		{
			if (method.isAnnotationPresent(annotation))
			{
				found.add(method);
			}
		}

		Method[] methods = new Method[found.size()];
		found.toArray(methods);

		if (logger.isDebugEnabled())
		{
			logger.debug("Found methods for annotation '" + annotation.getSimpleName() + "': " + methods);
		}

		return methods;
	}

	/**
	 * Gets all fields with the given annotation from the given class.
	 * 
	 * @param source Search this class
	 * @param anno Search for this annotation
	 * @param getPrivateFields If this parameter is "true", private fields will be checked, too.
	 * @return The found fields.
	 */
	public static List<Field> getFields(Class<?> source, Class<? extends Annotation> annotation,
		boolean getPrivateFields)
	{
		List<Field> found = new ArrayList<Field>();

		Field[] fields = getPrivateFields ? source.getDeclaredFields() : source.getFields();
		for (Field field : fields)
		{
			if (field.isAnnotationPresent(annotation))
			{
				found.add(field);
			}
		}

		if (getPrivateFields && source != Object.class && source.getSuperclass() != null)
		{
			found.addAll(getFields(source.getSuperclass(), annotation, getPrivateFields));
		}

		if (logger.isDebugEnabled())
		{
			logger.debug("Found fields with annotation '" + annotation.getSimpleName() + "': " + found);
		}
		return found;
	}

	/**
	 * Gets the given annotation from the given element and the super elements.
	 * 
	 * @param element Search this element (class, method or field)
	 * @param annoCls Search for this annotation.
	 * @return The annotation instance or null
	 */
	public static <T extends Annotation> T getAnnotation(AnnotatedElement element, Class<T> annoCls)
	{
		T anno = null;

		if (element != null && annoCls != null)
		{
			if (logger.isDebugEnabled())
			{
				logger.debug("Checking '" + element + "' for annotation '" + annoCls.getName() + "'");
			}

			if (element.isAnnotationPresent(annoCls))
			{
				anno = element.getAnnotation(annoCls);
			}
			else if (element instanceof Class && !((Class<?>) element).getName().startsWith("java.lang."))
			{
				Class<?> cls = (Class<?>) element;
				for (Class<?> i : cls.getInterfaces())
				{
					anno = getAnnotation(i, annoCls);
					if (anno != null)
					{
						break;
					}
				}

				if (anno == null && cls.getSuperclass() != Object.class)
				{
					anno = getAnnotation(cls.getSuperclass(), annoCls);
				}
			}

			if (anno != null && logger.isDebugEnabled())
			{
				logger.debug("* Found annotation '" + anno + "' in '" + element + "'");
			}
		}

		return anno;
	}
}
